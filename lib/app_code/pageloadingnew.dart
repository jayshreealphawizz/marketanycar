import 'dart:convert';

import 'package:Marketanycar/log/allapi.dart';
import 'package:flutter/material.dart';
import 'package:getwidget/getwidget.dart';
import 'package:http/http.dart' as http;
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:loadmore/loadmore.dart';
import 'package:shared_preferences/shared_preferences.dart';

class pageloadingnew extends StatefulWidget {
  @override
  _pageloadingnewState createState() => _pageloadingnewState();
}

class _pageloadingnewState extends State<pageloadingnew> {
  int get count => recent_upload_whole_list.length;
  String userAuthorization, userID;
  int pageno_value =0; int isFinish = 0;
  var recent_upload_whole_list ;
  List recent_upload_whole_list_newdata ;

  @override
  void initState() {
    load();
    // TODO: implement initState
    super.initState();
  }

  getAllPost_Api(category_id,pageNumber,pageSize) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userAuthorization = prefs.getString(AllApi.userAuthorization);
    userID = prefs.getString(AllApi.userId);
    if(userID == null){
      userID = '';
    }
    Map data = {
      'pageNumber':pageNumber.toString(),
      'pageSize': pageSize,
      'category_id':category_id,
      'user_id':'',
    };
    var jsondata = null;
    var response = await http.post(Uri.parse(AllApi.get_post_listApi),body: data);
    if(response.statusCode==200){
      jsondata = jsonDecode(response.body);
      int status = jsondata['status'];
      String message = jsondata['message'];
      var data_list = jsondata['data'];
      if(status==0){
        showToast(message);
        isFinish = -1;
      }
      else {
        setState(() {
          recent_upload_whole_list_newdata = data_list;
          if(recent_upload_whole_list!=null) {
            recent_upload_whole_list = new List.from(recent_upload_whole_list_newdata)..addAll(recent_upload_whole_list);
          }
          else{
            recent_upload_whole_list = recent_upload_whole_list_newdata;
          }
        });
      }
    }
    else {
      throw Exception('Failed to load post');
    }
  }

// ==============================
  void load() {
    print("load");
    setState(() {
      if(isFinish != -1) {
        getAllPost_Api('1', pageno_value, '5');
        pageno_value = pageno_value + 1;
      }
    });
  }

  Future<bool> _loadMore() async {
    print("onLoadMore");
      await Future.delayed(Duration(seconds: 0, milliseconds: 2000));
      load();
    return true;
  }

  Future<void> _refresh() async {
    print("_refresh");
    // await Future.delayed(Duration(seconds: 0, milliseconds: 2000));
    // recent_upload_whole_list.clear();
    // load();
  }
//   ============================
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text('hiiiiii'),
      ),
      body: Container(
        child: RefreshIndicator(
          child: LoadMore(
            isFinish: isFinish == -1,
            onLoadMore: _loadMore,
            child: ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  child: Text(recent_upload_whole_list[index]['name'], style: TextStyle(color: Colors.black38)),
                  height: 300.0,
                  alignment: Alignment.center,
                );
              },
              itemCount: count,
            ),
            whenEmptyLoad: false,
            delegate: DefaultLoadMoreDelegate(),
            textBuilder: DefaultLoadMoreTextBuilder.english,
          ),
          onRefresh: _refresh,
        ),
      ),
    );
  }
}
