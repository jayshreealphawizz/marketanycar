import 'dart:async';

import 'package:Marketanycar/emojitext/emoji_picker_widget.dart';
import 'package:Marketanycar/emojitext/input_widget.dart';
import 'package:Marketanycar/log/allapi.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:emoji_picker/emoji_picker.dart';

class ChatPage extends StatefulWidget {
  final docs, receiver_name;

  const ChatPage({Key key, this.docs, this.receiver_name}) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  String groupChatId;
  String userID;
  var ref;
  TextEditingController textEditingController = TextEditingController();
  bool isEmojiVisible = false;
  bool isKeyboardVisible = false;
  List<DocumentSnapshot> documents;

  ScrollController scrollController = ScrollController();
  @override
  void initState() {
    getGroupChatId();
    super.initState();
  }

  getGroupChatId() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    userID = sharedPreferences.getString(AllApi.userId);

    String anotherUserId = widget.docs;

    if (userID.compareTo(anotherUserId) > 0) {
      groupChatId = '$userID - $anotherUserId';
    } else {
      groupChatId = '$anotherUserId - $userID';
    }
    setState(() {});
  }

  _animateToIndex(i) =>
      scrollController.animateTo(scrollController.position.maxScrollExtent+1.0, duration: Duration(seconds: 2), curve: Curves.fastOutSlowIn);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
         widget.receiver_name,
          style: TextStyle(fontSize: 16, color: Color(0xFF8E2E7B)),
        ),
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () => _animateToIndex(documents.length),
      //   child: Icon(Icons.arrow_downward),
      // ),
      body: Stack(
        children: [
          StreamBuilder(
            stream:  FirebaseFirestore.instance.collection('messages').doc(groupChatId).collection(groupChatId).snapshots(),
            builder: (context, snapshot) {
              if (snapshot.hasData && snapshot.data != null) {
                documents = snapshot.data.docs;
                return documents.length==0?Container(height:MediaQuery.of(context).size.height,color : Colors.white): Column(
                  children: <Widget>[

                    Expanded(

                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 8),
                          child: ListView .builder(
                            physics: BouncingScrollPhysics(),
                            shrinkWrap: true,
                            controller: scrollController,
                            itemBuilder: (listContext, index) => buildItem(documents[index]),
                            itemCount: documents.length,
                          ),
                        )),
                    InputWidget(
                      onBlurred: toggleEmojiKeyboard,
                      controller: textEditingController,
                      isEmojiVisible: isEmojiVisible,
                      isKeyboardVisible: isKeyboardVisible,
                      onSentMessage: (message) =>
                          setState(() =>    sendMsg()),
                    ),
                    Offstage(
                      child: EmojiPickerWidget(onEmojiSelected: onEmojiSelected),
                      offstage: !isEmojiVisible,
                    ),
                  ],
                );
              } else {
                return Center(
                    child: SizedBox(
                      height: 36,
                      width: 36,
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                      ),
                    ));
              }
            },
          ),
          // Positioned(
          //   bottom: 10,
          //   child:
          //   Container(color: Colors.white, width: MediaQuery.of(context).size.width,
          //     child: Padding(
          //       padding: const EdgeInsets.only(left: 10,right: 10,top: 5,bottom: 3),
          //       child: Row(
          //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //         children: [
          //           IconButton(icon: Icon(Icons.emoji_emotions_outlined,size: 18,),
          //             onPressed:()async {
          //               buildSticker();
          //               // sendMsg();
          //             },
          //           ),
          //           Row(
          //             children: [
          //               SizedBox(width: 8,),
          //               Container(
          //                   width: 200,
          //                   height: 30,
          //                   child: TextFormField(
          //                     key: Key("comment"),
          //                     controller: textEditingController,
          //                     decoration: InputDecoration(
          //                       border: InputBorder.none,
          //                       hintStyle: TextStyle(fontSize: 12),
          //                       hintText: 'Add Message...',),
          //                   )),
          //             ],
          //           ),
          //           IconButton(icon: Icon(Icons.send,size: 18,),
          //             onPressed:()async {
          //               sendMsg();
          //             },
          //           )
          //           // Icon(Icons.add_circle_outline,size: 18,)
          //         ],
          //       ),
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }


  void onEmojiSelected(String emoji) => setState(() {
    textEditingController.text = textEditingController.text + emoji;
  });

  Future toggleEmojiKeyboard() async {
    if (isKeyboardVisible) {
      FocusScope.of(context).unfocus();
    }

    setState(() {
      isEmojiVisible = !isEmojiVisible;
    });
  }

  sendMsg() {
    String msg = textEditingController.text.trim();
    /// Upload images to firebase and returns a URL
    if (msg.isNotEmpty) {

     var ref = FirebaseFirestore.instance
          .collection('messages')
          .doc(groupChatId)
          .collection(groupChatId)
          .doc(DateTime.now().millisecondsSinceEpoch.toString());

      FirebaseFirestore.instance.runTransaction((transaction) async {
        await transaction.set(ref, {
          "senderId": userID,
          "anotherUserId": widget.docs,
          "timestamp": DateTime.now().millisecondsSinceEpoch.toString(),
          'content': msg,
          "type": 'text',
        });
      });


      textEditingController.text = '';

    } else {
    }
  }

  buildItem(doc) {
    return Padding(
      padding: EdgeInsets.only(
          top: 8.0,
          left: ((doc['senderId'] == userID) ? 120 : 10),
          right: ((doc['senderId'] == userID) ? 10 : 120)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.all(13.0),
            decoration: BoxDecoration(
                color: ((doc['senderId'] == userID)
                    ? Color(0xFF8E2E7B)
                    : Color(0xDDDBD9D9)),
                borderRadius: BorderRadius.circular(5.0)),
            // color: (doc['senderId']==userID ? Colors.amber: Colors.cyanAccent),
            child: (doc['type'] == 'text')
                ? Text('${doc['content']}', style: TextStyle(color:doc['senderId']==userID ? Colors.white: Colors.black))
                : Image.network(doc['content']),
          ),
          SizedBox(height: 5,),
          Text(readTimestamp(int.parse(doc['timestamp'])),style: TextStyle(fontSize: 10.0,color:Color(0xDDBFBEBE)))
        ],
      ),
    );
  }

  String readTimestamp(int timestamp) {
    var now = new DateTime.now();
    var format = new DateFormat('HH:mm a');
    var date = new DateTime.fromMicrosecondsSinceEpoch(timestamp * 1000);
    var diff = date.difference(now);
    var time = '';
    if (diff.inSeconds <= 0 || diff.inSeconds > 0 && diff.inMinutes == 0 || diff.inMinutes > 0 && diff.inHours == 0 || diff.inHours > 0 && diff.inDays == 0) {
      time = format.format(date);
    } else {
      if (diff.inDays == 1) {
        time = diff.inDays.toString() + 'DAY AGO';
      } else {
        time = diff.inDays.toString() + 'DAYS AGO';
      }
    }
    return time;
  }
}
