import 'package:flutter/material.dart';
class Damy extends StatefulWidget {
  const Damy({
    Key key,
  }) : super(key: key);
  @override
  _DamyState createState() => _DamyState();
}

class _DamyState extends State<Damy> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Snack bar'),
      ),

      /// We use [Builder] here to use a [context] that is a descendant of [Scaffold]
      /// or else [Scaffold.of] will return null
      body: Builder(
        builder: (context) => Center(
          child: RaisedButton(
            child: const Text('Show toast'),
            onPressed: () => _showToast(context),
          ),
        ),
      ),
    );
  }

  void _showToast(BuildContext context) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: const Text('Added to favorite'),
        action: SnackBarAction(
            label: 'UNDO', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }
}
