
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:Marketanycar/cardetails.dart';
import 'package:Marketanycar/sell_any_car.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter/material.dart';
import 'comman/_bottomAppBar.dart';
import 'comman/_drawer.dart';
// import 'package:Marketanycar/sell_any_car.dart';
import 'log/allapi.dart';
import 'log/login.dart';
import 'notifications.dart';

class Mypost_list extends StatefulWidget {
  @override
  _Mypost_listState createState() => _Mypost_listState();
}

class _Mypost_listState extends State<Mypost_list> {
  Map<int, String> postimge_strlist;
  List mypost_list;
  String userAuthorization, userID;
 // String user_image_url= "https://alphawizztest.tk/market_any_car/";

  getchatData(String pageNumber,pageSize) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userAuthorization = prefs.getString(AllApi.userAuthorization);
    userID = prefs.getString(AllApi.userId);

    if(userAuthorization==null){
      Navigator.of(context).pop();
      Navigator.push(context,
          MaterialPageRoute(builder:
              (context) => Loginpage()
          ));
    }
    else {
      Map data = {
        'pageNumber': pageNumber,
        'pageSize': pageSize,
      };
      Map<String, String> requestHeaders = {
        'Authorizations': userAuthorization
      };
      var jsondata = null;
      var response = await http.post(Uri.parse(AllApi.get_my_post_listApi), body: data,
          headers: requestHeaders);
      if (response.statusCode == 200) {
        jsondata = jsonDecode(response.body);
        int status = jsondata['status'];
        String message = jsondata['message'];
        var data_list = jsondata['data'];
        if (status == 0) {
          showToast(message);
        }
        else {
          setState(() {
            mypost_list = data_list;
          });
        }
      }
      else {
        throw Exception('Failed to load post');
      }
    }
  }
  //----------------
  post_delete_Data(String post_id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userAuthorization = prefs.getString(AllApi.userAuthorization);
    userID = prefs.getString(AllApi.userId);

    if(userAuthorization==null){
      Navigator.of(context).pop();
      Navigator.push(context,
          MaterialPageRoute(builder:
              (context) => Loginpage()
          ));
    }
    else {
      Map data = {
        'post_id': post_id,
      };
      Map<String, String> requestHeaders = {
        'Authorizations': userAuthorization
      };
      var jsondata = null;
      var response = await http.post(Uri.parse(AllApi.post_deleteApi), body: data,
          headers: requestHeaders);

      if (response.statusCode == 200) {
        jsondata = jsonDecode(response.body);
        int status = jsondata['status'];
        String message = jsondata['message'];
        var data_list = jsondata['data'];
        if (status == 0) {
          showToast(message);
        }
        else {
          setState(() {
            getchatData('1', '20');
            Mypost_list();
          });
        }
      }
      else {
        throw Exception('Failed to load post');
      }
    }
  }
  @override
  void initState() {
    setState(() {
      getchatData('1','20');
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
      drawer: MyDrawer(),
      appBar: AppBar(
        title: Text("Vehicle You're Selling",
          style: TextStyle(fontSize: 16,color: Color(0xFF8E2E7B),),),
        actions: [
          IconButton(
              icon:  Icon(Icons.notifications_none) ,
              onPressed:  () =>Navigator.push(context,
          MaterialPageRoute(builder:
              (context) => Notifications()
          )
      ),
          ),
          SizedBox(width: 10,)
        ],
      ),
      body: mypost_list==null?Container(child: Center(child: Text('No Records found')),): Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.only(left: 15,right: 15),
          child: ListView.builder(
            itemCount:mypost_list.length,
            itemBuilder: (BuildContext context,int index){
              return _Stack(index);
            }
          ),
        ),
      ),
      bottomNavigationBar: Bbar(),
    );
  }
  Widget _Stack(int index){
    String data_list_hvgh = mypost_list[index]['filename'];
    if(data_list_hvgh!='') {
      final split = data_list_hvgh.split(',');
      postimge_strlist = {
        for (int i = 0; i < split.length; i++)
          i: split[i]
      };
    }
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Card(
        child: Container(
          //height: 350,
          width: MediaQuery.of(context).size.width,
          child: GestureDetector(
            onTap: (){  Navigator.pop(this.context);
            Navigator.of(this.context).push(
              MaterialPageRoute(builder: (BuildContext context) => Cardetails(text_PostId:mypost_list[index]['id'])),);},

            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                postimge_strlist[0]==null?Image.network(''):
                Image.network(AllApi.user_image_url+postimge_strlist[0],
                  fit: BoxFit.cover,
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10,top: 10,),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Row(children: [
                              Icon(Icons.favorite_sharp,color: Colors.red,size: 15,),
                              Text(mypost_list[index]['like_count']+"\tLike",
                                style: TextStyle(fontSize: 10,fontWeight: FontWeight.bold),)
                            ],),
                            SizedBox(width: 5,),
                            Row(children: [
                              Icon(Icons.send,color: Colors.yellow,size: 15,),
                              Text( mypost_list[index]['like_count']+"\tComments",
                                style: TextStyle(fontSize: 10,fontWeight: FontWeight.bold),)
                            ],),
                          ],
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(mypost_list[index]['name'],
                            style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                          SizedBox(width: 0.1,)
                        ],
                      ),

                      SizedBox(height: 10,),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text("\$"+mypost_list[index]['price'],
                            style: TextStyle(fontSize: 16,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),),
                          SizedBox(width: 20,),
                          Text("\$"+mypost_list[index]['offer_price'],
                            style: TextStyle(fontSize: 11,
                                fontWeight: FontWeight.bold),),
                        ],
                      ),
                      SizedBox(height: 10,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          // Image.asset("assets/Image 3.png",
                          //   fit: BoxFit.cover,
                          //   height: 50,
                          //   width: 50,
                          // ),
                          Row(
                            // mainAxisAlignment: MainAxisAlignment.end,
                            // crossAxisAlignment: CrossAxisAlignment.e,
                            children: [
                              // GestureDetector(
                              //   onTap: () =>Navigator.push(context,
                              //       MaterialPageRoute(builder:
                              //           (context) => Sellanycar()
                              //       )
                              //   ),
                              //   child: Row(
                              //     children: [
                              //       Icon(Icons.edit,size: 18,),
                              //       Text("Edit",
                              //         style: TextStyle(fontSize: 11,
                              //             fontWeight: FontWeight.bold,),),
                              //     ],
                              //   ),
                              // ),
                             // SizedBox(width: 10,),
                              GestureDetector(
                                onTap:  () async {
                                  post_delete_Data(mypost_list[index]['id']);
                                  // post_add(String vehicle_name,brand,model,mileage,description)
                                },
                                child: Row(
                                  children: [
                                    Icon(Icons.delete,color: Colors.red,
                                    size: 18,),
                                    Text("Delete",
                                      style: TextStyle(fontSize: 11,
                                        fontWeight: FontWeight.bold,
                                      color: Colors.red),),
                                  ],
                                ),
                              ),
                              SizedBox(width: 10,)
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 10,)
              ],
            ),
          ),
        ),
      ),
    );
  }

}
