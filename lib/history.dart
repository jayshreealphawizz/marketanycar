import 'package:flutter/material.dart';
import 'comman/_bottomAppBar.dart';

class Historys extends StatefulWidget {
  @override
  _HistorysState createState() => _HistorysState();
}

class _HistorysState extends State<Historys> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Payment History"
          ,style: TextStyle(fontSize: 16,color: Color(0xFF8E2E7B)),),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Padding(
          padding: const EdgeInsets.only(left: 15,right: 15),
          child: ListView(
            children: [
              SizedBox(height: 20,),
             _Card(),
              SizedBox(height: 15,),
              _Card(),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Bbar(),
    );
  }
  Widget _Card(){
    return  Container(
      height: 230,
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.center,
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset("assets/car.jpg",
                fit: BoxFit.cover,
                height: 150,
                width: MediaQuery.of(context).size.width*0.35,
              ),
              Row(
                children: [
                  Image.asset("assets/car.jpg",
                    fit: BoxFit.cover,
                    height: 70,
                    width: MediaQuery.of(context).size.width*0.18,
                  ),
                  SizedBox(width: 10,),
                  Image.asset("assets/car.jpg",
                    fit: BoxFit.cover,
                    height: 70,
                    width: MediaQuery.of(context).size.width*0.14,
                  ),
                ],
              )
            ],
          ),
          SizedBox(width: 10,),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 10,),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("\$2050",
                        style: TextStyle(fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Colors.yellow
                        ),),
                    ],
                  ),
                ),
                Text("2016 Audi A1 2.0 TFSI S1",
                  style: TextStyle(fontSize: 14,
                      fontWeight: FontWeight.bold),),
                Text("Hatchback",
                  style: TextStyle(fontSize: 14,
                      fontWeight: FontWeight.bold),),
                Text("\$732 pm (PCP)",
                  style: TextStyle(fontSize: 12,),),
                Text("ID : 1231927931",
                  style: TextStyle(fontSize: 12,),),
                Row(
                  children: [
                    ImageIcon(
                      AssetImage("assets/icons/sasa.png",),
                      color: Colors.green,
                    ),
                   SizedBox(width: 5,),
                   // Icon(Icons.adb,color:Colors.green ,),
                    Text("Card Payment",
                      style: TextStyle(fontSize: 12,
                          color: Colors.green,
                          fontWeight: FontWeight.bold),),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

}
