
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

abstract class AllApi
{
 // static const String baseUrl = 'http://192.168.29.171/market_any_car/Api';
 // static const String user_image_url= "http://192.168.29.171/market_any_car/";

  static const String user_image_url= "https://alphawizztest.tk/market_any_car/";
  static const String baseUrl = 'https://alphawizztest.tk/market_any_car/Api';
  // https://alphawizz.in/market_any_car/Api/SocialLogin/Login

  static const String loginApi = '$baseUrl/Authentication/login';
  static const String socialloginApi = '$baseUrl/SocialLogin/login';
  static const String registerApi = '$baseUrl/Authentication/register';
  static const String forgot_passwordApi = '$baseUrl/Authentication/forgot_password';
  static const String profile_updateApi = '$baseUrl/Authentication/profile_update';
  static const String logoutApi = '$baseUrl/Authentication/logout';
  static const String getcontentApi = '$baseUrl/Authentication/getcontent';
  static const String faq_listApi = '$baseUrl/Authentication/faq_list';
  static const String enter_vehicle_no_verifyApi = '$baseUrl/Users/enter_vehicle_no_verify';
  static const String get_categoriesApi = '$baseUrl/Post/get_categories';
  static const String get_dashboard_dataApi = '$baseUrl/Post/get_dashboard_data';
  static const String get_body_typeApi = '$baseUrl/Post/get_body_type';
  static const String get_search_post_listApi = '$baseUrl/Post/get_search_post_list';
  static const String get_post_listApi = '$baseUrl/Post/get_post_list';
  static const String get_notification_listApi = '$baseUrl/Users/get_notification_list';
  static const String post_addApi = '$baseUrl/Post/post_add';
  static const String get_mileageApi = '$baseUrl/Post/get_mileage';
  static const String get_yearApi = '$baseUrl/Post/get_year';
  static const String get_min_priceApi = '$baseUrl/Post/get_min_price';
  static const String post_deleteApi = '$baseUrl/Post/post_delete';
  static const String post_favoriteApi = '$baseUrl/Post/post_favorite';
  static const String get_favorite_post_listApi = '$baseUrl/Post/get_favorite_post_list';
  static const String get_subcategoriesApi = '$baseUrl/Post/get_subcategories';
  static const String get_childsubcategoriesApi = '$baseUrl/Post/get_childsubcategories';
  static const String post_detailsApi = '$baseUrl/Post/post_details';
  static const String get_post_likeApi = '$baseUrl/Post/post_like';
  static const String get_chat_list = '$baseUrl/Users/get_chat_list';
  static const String get_my_post_listApi = '$baseUrl/Post/get_my_post_list';
  static const String post_commentApi = '$baseUrl/Post/post_comment';
  static const String get_comment_listApi = '$baseUrl/Post/get_comment_list';
  static const String get_like_listApi = '$baseUrl/Post/get_like_list';
  static const String changepasswordApi = '$baseUrl/Authentication/changepassword';
  static const String chat_add_user_Api = '$baseUrl/Chat/add_chat';


  static const String userId = 'Id';
  static const String useremail = 'email';
  static const String userfirst_name = 'first_name';
  static const String usermobile = 'mobile';
  static const String userAuthorization = 'Authorization';
  static const String useraddress = 'address';
  static const String userimage = 'image';

}
void showToast(String message) {
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIos: 8,
      backgroundColor: Colors.black54,
      textColor: Colors.white,
      fontSize: 14
  );
}

void setErrorLoadingText() {


  ErrorWidget.builder = (FlutterErrorDetails errorDetails) {
    Timer(
      Duration(seconds: 1),
          () => setErrorLoadingText,
    );
    return Scaffold(
        body: Center(
            child: Text("Loading...")));
  };
}



Future<bool> loginAction() async {
  await new Future.delayed(const Duration(seconds: 2));
  //await CircularProgressIndicator.adaptive();
  return true;
}
