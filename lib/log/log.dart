class Post {
  final String email;
  final String password;
  final String data;
  final String name;
  final String mobile;
  final String vehicle_no;
  //final String page_title;

  Post({this.email,
    this.password,
    this.data,
    this.name,
    this.mobile,
    this.vehicle_no,
   // this.page_title,
  });

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      email: json['email'],
      password: json['password'],
      data: json['data'],
      name: json['name'],
      mobile: json['mobile'],
      vehicle_no: json['vehicle_no'],
     // page_title: json['page_title'],
    );
  }
}