import 'dart:convert';

import 'package:Marketanycar/social/sign_in.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../account.dart';
import '../forgotp.dart';
import '../home.dart';
import 'allapi.dart';
import 'log.dart';
import 'login.dart';
class signup extends StatefulWidget {

  @override
  _signupState createState() => _signupState();
}

class _signupState extends State<signup> {
  final _formKey = GlobalKey<FormState>();
  Future<Post> post;

  TextEditingController emailController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();
  TextEditingController mobileController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  Creatid(String email, password, name, mobile) async {
    Map data = {
      "name":name,
      "mobile":mobile,
      "email": email,
      "password": password,
    };

    var jsondata = null;
    var response = await http.post(Uri.parse(AllApi.registerApi),body: data);
    if(response.statusCode==200){
      jsondata = jsonDecode(response.body);
      int status =  jsondata['status'];
      String message =  jsondata['message'];
      if(status==0){
        showToast(message);
      }
      else {
        setState(() {
          showToast(message);
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => Home()),);
          String id = jsondata['data']['id'];
          String email = jsondata['data']['email'];
          String name = jsondata['data']['first_name'];
          String mobile = jsondata['data']['mobile'];
          String address = jsondata['data']['address'];
          String Authorization = jsondata['data']['Authorization'];
          // String status =  jsondata['status'];

          addStringToSF(id, Authorization,email,name,mobile,address);

          signInWithGoogle().then((result) {
            if (result != null) {
            }
          });
        });
      }
    }
    else {
      throw Exception('Failed to load post');
    }
  }
  addStringToSF(String id ,String Authorization, String email, String name, String mobile, String address) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(AllApi.userId, id);
    prefs.setString(AllApi.userAuthorization , Authorization);
    prefs.setString(AllApi.userfirst_name , name);
    prefs.setString(AllApi.useremail, email);
    prefs.setString(AllApi.usermobile, mobile);
    prefs.setString(AllApi.useraddress, address);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: ListView(
          children: [
            Stack(
              children: [
                Center(
                  child: Image.asset("assets/Mask 180.png",
                    fit: BoxFit.fill,
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    height: MediaQuery
                        .of(context)
                        .size
                        .height,
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25, right: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height: MediaQuery.of(context).size.height * 0.05,),
                        Image.asset("assets/applogo.png",
                          height: 100, width: 200,),
                        Text("Create Account", style: TextStyle(
                          fontSize: 16, fontWeight: FontWeight.bold,
                        ),),
                        TextFormField(
                          controller: nameController,
                          key: Key("name"),
                          textInputAction: TextInputAction.next,
                          style: TextStyle(color: Color(0xFF8E2E7B)),
                          cursorColor: Color(0xFF8E2E7B),
                          decoration: InputDecoration(
                            fillColor: Colors.black12,
                            border: InputBorder.none,
                            filled: true,
                            hintText: "Name",
                            hintStyle: TextStyle( color: Colors.black38,fontSize: 12),
                          ),
                          validator: (val){
                            if(val.isEmpty){
                              return 'enter your name';
                            }
                          } ,
                        ),
                        TextFormField(
                          controller: emailController,
                          key: Key("email"),
                          textInputAction: TextInputAction.next,
                          style: TextStyle(color: Color(0xFF8E2E7B)),
                          cursorColor: Color(0xFF8E2E7B),
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.black12,
                            border: InputBorder.none,
                            hintText: "Email",
                            hintStyle: TextStyle(
                                color: Colors.black38, fontSize: 12),
                          ),
                          validator: (val) {
                            final RegExp nameExp =
                            new RegExp(
                                r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$');
                            if (val.isEmpty) {
                              return 'Please enter email address.';
                            }
                            else if (!nameExp.hasMatch(val)) {
                              return 'Please enter valid email address.';
                            }
                          },
                        ),

                        TextFormField(
                          maxLength: 10,
                          controller: mobileController,
                          key: Key("mobile"),
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.number,
                          style: TextStyle(color: Color(0xFF8E2E7B)),
                          cursorColor: Color(0xFF8E2E7B),
                          decoration: InputDecoration(
                            counterText: "",
                            fillColor: Colors.black12,
                            border: InputBorder.none,
                            filled: true,
                            hintText: "Mobile No.",
                            hintStyle: TextStyle( color: Colors.black38,fontSize: 12),
                          ),
                          validator: (val ){
                            RegExp nameExp = new RegExp(r'^([0-9]{10})$');
                            if (val != "") {
                              if (val.length != 10)
                                return " should be 10 digit";
                              else if (!nameExp.hasMatch(val))
                                return " should be in correct format";
                              return null;
                            } else {
                              return "Required";
                            }
                          },
                        ),
                        TextFormField(
                          controller: passwordController,
                          textInputAction: TextInputAction.done,
                          style: TextStyle(color: Color(0xFF8E2E7B)),
                          cursorColor: Color(0xFF8E2E7B),
                          decoration: InputDecoration(
                            fillColor: Colors.black12,
                            border: InputBorder.none,
                            filled: true,
                            hintText: "Password",
                            hintStyle: TextStyle(
                                color: Colors.black38, fontSize: 12),
                          ),
                          validator: (val) {
                            if (val.isEmpty) {
                              return 'Please enter password.';
                            }
                          },
                          //
                        ),

                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 40, right: 40, top: 15, bottom: 15),
                                child: Text("Create",
                                  style: TextStyle(color: Colors.white),),
                              ),
                              color: Color(0xFF8E2E7B),
                              onPressed: () async {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return Center(
                                        child: CircularProgressIndicator(),);
                                    });
                                await loginAction();
                                Navigator.pop(context);
                                if (_formKey.currentState.validate()) {
                                  Creatid(emailController.text,passwordController.text,nameController.text,mobileController.text);
                                }
                                else {
                                }
                              },
                            ),
                          ],),

                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Already Have An Account",style: TextStyle(
                                color:Colors.black54,fontSize: 14
                            ),),
                            GestureDetector(
                              onTap: () =>
                                  Navigator.push(context,
                                      MaterialPageRoute(builder:
                                          (context) => Loginpage()
                                      )
                                  ),
                              child: Text(" Log in",style: TextStyle(
                                  color: Color(0xFF8E2E7B),fontSize: 14,
                                  fontWeight: FontWeight.bold
                              ),),
                            ),
                          ],
                        ),
                        SizedBox(height: 25,),
                      ],
                    ),
                  ),

                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}