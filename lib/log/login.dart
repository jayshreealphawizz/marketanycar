import 'dart:convert';

import 'package:Marketanycar/social/sign_in.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:Marketanycar/log/signup.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_core/firebase_core.dart';
import '../account.dart';
import '../forgotp.dart';
import '../home.dart';
import 'allapi.dart';
import 'log.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

var facebookLogin = FacebookLogin();
var user_fbprofile;

class Loginpage extends StatefulWidget {
  @override
  _LoginpageState createState() => _LoginpageState();
}

class _LoginpageState extends State<Loginpage> {
  final _formKey = GlobalKey<FormState>();
  Future<Post> post;
  String email;
  String password;
  bool isLoggedIn = false;
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();


 void Sign(String email, password) async {
   Map data = {
     "email": email,
     "password": password,
   };

    var jsondata = null;
    var response = await http.post(Uri.parse(AllApi.loginApi),body: data);
    if (response.statusCode == 200) {
      jsondata = jsonDecode(response.body);
      int status =  jsondata['status'];
      String message =  jsondata['message'];
      if(status==0){
        showToast(message);
      }
      else{
        setState(() {
          showToast(message);
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => Home()),);
          String id = jsondata['data']['id'];
          String email = jsondata['data']['email'];
          String name = jsondata['data']['first_name'];
          String mobile = jsondata['data']['mobile'];
          String address = jsondata['data']['address'];
          String Authorization = jsondata['data']['Authorization'];
          addStringToSF(id,Authorization,name,email,mobile,address);
        });
      }
    }
    else {
      throw Exception('Failed to load post');
    }
  }


  void sociallogin_Sign(String name, email) async {
    Map data = {
      "name": name,
      "email": email,
      "social_login": '1'
    };

    var jsondata = null;
    var response = await http.post(Uri.parse(AllApi.socialloginApi),body: data);
    if (response.statusCode == 200) {
      jsondata = jsonDecode(response.body);

      int status =  jsondata['status'];
      String message =  jsondata['message'];
      if(status==0){
        showToast(message);
      }
      else{
        setState(() {
          showToast(message);
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => Home()),);
          String id = jsondata['data']['id'];
          String email = jsondata['data']['email'];
          String name = jsondata['data']['first_name'];
          String mobile = jsondata['data']['mobile'];
          String address = jsondata['data']['address'];
          String Authorization = jsondata['data']['Authorization'];
          addStringToSF(id,Authorization,name,email,mobile,address);
        });
      }
    }
    else {
      throw Exception('Failed to load post');
    }
  }

  addStringToSF(String id ,String Authorization, String name, String email, String mobile, String address) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(AllApi.userId, id);
    prefs.setString(AllApi.userAuthorization , Authorization);
    prefs.setString(AllApi.userfirst_name , name);
    prefs.setString(AllApi.useremail, email);
    prefs.setString(AllApi.usermobile, mobile);
    prefs.setString(AllApi.useraddress, address);
  }

  @override
  Widget build(BuildContext context) {

    WidgetsFlutterBinding.ensureInitialized();
    Firebase.initializeApp();

    return Scaffold(
      body: Form(
        key: _formKey,
        child: ListView(
          children: [
            Stack(
              children: [
                Center(
                  child: Image.asset("assets/Mask 180.png",
                    fit: BoxFit.fill,
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    height: MediaQuery
                        .of(context)
                        .size
                        .height,
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25, right: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height: MediaQuery
                            .of(context)
                            .size
                            .height * 0.15,),
                        Image.asset("assets/applogo.png",
                          height: 100, width: 200,),
                        Text("Welcome Back", style: TextStyle(
                          fontSize: 16, fontWeight: FontWeight.bold,
                        ),),

                        TextFormField(
                          controller: emailController,
                          key: Key("email"),
                          textInputAction: TextInputAction.next,
                          style: TextStyle(color: Color(0xFF8E2E7B)),
                          cursorColor: Color(0xFF8E2E7B),
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.black12,
                            border: InputBorder.none,
                            hintText: "Email",
                            hintStyle: TextStyle(
                                color: Colors.black38, fontSize: 12),
                          ),
                          validator: (val) {
                            final RegExp nameExp =
                            new RegExp(
                                r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$');
                            if (val.isEmpty) {
                              return 'Please enter email address.';
                            }
                            else if (!nameExp.hasMatch(val)) {
                              return 'Please enter valid email address.';
                            }
                          },
                        ),
                        TextFormField(
                          controller: passwordController,
                          textInputAction: TextInputAction.done,
                          style: TextStyle(color: Color(0xFF8E2E7B)),
                          cursorColor: Color(0xFF8E2E7B),
                          decoration: InputDecoration(
                            fillColor: Colors.black12,
                            border: InputBorder.none,
                            filled: true,
                            hintText: "Password",
                            hintStyle: TextStyle(
                                color: Colors.black38, fontSize: 12),
                          ),
                          validator: (val) {
                            if (val.isEmpty) {
                              return 'Please enter password.';
                            }
                          },
                          //
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          // crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            GestureDetector(
                              child: Text("Forgot Password?", style: TextStyle(
                                  fontSize: 16, color: Color(0xFF8E2E7B)
                              ),),
                              onTap: () =>
                                  Navigator.push(context,
                                      MaterialPageRoute(builder:
                                          (context) => Forgotp()
                                      )
                                  ),
                            )
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 40, right: 40, top: 15, bottom: 15),
                                child: Text("Login",
                                  style: TextStyle(color: Colors.white),),
                              ),
                              color: Color(0xFF8E2E7B),
                              onPressed: () async {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return Center(
                                        child: CircularProgressIndicator(),);
                                    });
                                await loginAction();
                                Navigator.pop(context);
                                if (_formKey.currentState.validate()) {
                                  Sign(emailController.text,
                                      passwordController.text);
                                }
                                else {
                                }
                              },
                            ),
                          ],),

                        Text("Or Connect Using", style: TextStyle(
                            color: Colors.black45, fontSize: 14
                        ),),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            GestureDetector(
                              onTap: () {
                               initiateFacebookLogin();
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Color(0xFF5C6BC0),
                                ),
                                width: 120, height: 45,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment
                                      .spaceEvenly,
                                  children: [
                                    Image.asset(
                                      "assets/icons/facebook.png", scale: 1.5,
                                      color: Colors.white,),
                                    Text("FaceBook", style: TextStyle(
                                        color: Colors.white
                                    ),)
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(width: 10,),
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Color(0xFFEF5350),
                              ),
                              width: 120, height: 45,
                              child: GestureDetector(
                                onTap: () {
                                  signInWithGoogle().then((result) {
                                    if (result != null) {
                                      print('social_email'+social_email);
                                      print('social_name'+social_name);
                                      sociallogin_Sign(social_name, social_email);}
                                  });
                                },
                                child: Row (
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment
                                      .spaceEvenly,
                                  children: [
                                    Image.asset(
                                      "assets/icons/google.png", scale: 1.5,),
                                    Text("Google", style: TextStyle(
                                        color: Colors.white
                                    ),)
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10,),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Don't Have Any Account", style: TextStyle(
                                color: Colors.black38, fontSize: 14
                            ),),
                            GestureDetector(
                              child: Text(" Signup", style: TextStyle(
                                  color: Color(0xFF8E2E7B), fontSize: 14,
                                fontWeight: FontWeight.bold
                              ),),
                              onTap: () =>
                                  Navigator.push(context,
                                      MaterialPageRoute(builder:
                                          (context) => signup()
                                      )
                                  ),
                            ),
                          ],
                        ),
                        SizedBox(height: MediaQuery
                            .of(context)
                            .size
                            .height * 0.1,),
                      ],
                    ),
                  ),

                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  void  initiateFacebookLogin() async {
    var facebookLoginResult = await facebookLogin.logIn(['email']);
    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.error:
        print("Error");
        onLoginStatusChanged(false);
        break;
      case FacebookLoginStatus.cancelledByUser:
        print("CancelledByUser");
        onLoginStatusChanged(false);
        break;
      case FacebookLoginStatus.loggedIn:
        print("LoggedIn");

        print('facebook token====');
        print(facebookLoginResult.accessToken.token);

        var graphResponse = await http.get(Uri.parse('https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${facebookLoginResult.accessToken.token}'));
        user_fbprofile = json.decode(graphResponse.body);

        String social_name = user_fbprofile['name'];
        String social_email = user_fbprofile['email'];

        sociallogin_Sign(social_name, social_email);

        onLoginStatusChanged(true);
        break;
    }
  }

  void onLoginStatusChanged(bool isLoggedIn) {
    setState(() {
      this.isLoggedIn = isLoggedIn;
    });
  }

}


Future<void> facebooksignOut() async {
  await facebookLogin.logOut();
  user_fbprofile = null;
  print("User Signed Out facebook");
}
