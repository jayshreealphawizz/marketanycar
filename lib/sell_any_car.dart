import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:Marketanycar/home.dart';
import 'package:Marketanycar/mypostlist.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:image_cropper/image_cropper.dart';
import 'favoritelist.dart';
import 'log/allapi.dart';
import 'package:path/path.dart';
import 'package:path/path.dart';
import 'package:async/async.dart';

import 'package:path_provider/path_provider.dart';
import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';
import 'package:http_parser/http_parser.dart';
import 'package:permission_handler/permission_handler.dart';

import 'log/login.dart';


class Sellanycar extends StatefulWidget {
 final String text_model,text_color, text_engineno,text_enginecapacity,text_make,text_weight,text_vehicleno, text_mileage;

  const Sellanycar({Key key, this.text_model, this.text_color, this.text_engineno, this.text_enginecapacity, this.text_make, this.text_weight, this.text_vehicleno, this.text_mileage}) : super(key: key);


  @override
  _SellanycarState createState() => _SellanycarState();
}

class _SellanycarState extends State<Sellanycar> {

  TextEditingController vehicle_nameController = new TextEditingController();
  TextEditingController colorController = new TextEditingController();
  TextEditingController brandController = new TextEditingController();
  TextEditingController modelController = new TextEditingController();
  TextEditingController mileageController = new TextEditingController();
  TextEditingController makeController = new TextEditingController();
  TextEditingController descriptionController = new TextEditingController();
  TextEditingController priceController = new TextEditingController();
  TextEditingController offer_priceController = new TextEditingController();
  TextEditingController engine_no_Controller = new TextEditingController();
  TextEditingController engine_capacity_Controller = new TextEditingController();
  TextEditingController weightController = new TextEditingController();
  TextEditingController vehicle_no_Controller = new TextEditingController();
  List<File> PostImagefiles = [];

  final PermissionHandler _permissionHandler = PermissionHandler();

  http.MultipartFile multipartFile;
  String userAuthorization, finalimagepathhhhh;
  List<String> postImage_strlist;

// ===========================================
  String _selectedCategory;
  List category_list;
  List<String> category_list_strlist;
  File _image;
  String Imagepath;
  String result = "";
  String FinalImage;
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String useremail, userfirst_name, usermobile;
  String useraddress, userimage;
  String imagePATH;
  List<int> final_image_data_multipart;

// ===========================================
  List<Asset> multiple_images = List<Asset>();

  Future<void> pickImages() async {
    List<Asset> resultList = List<Asset>();
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 6,
        enableCamera: true,
        selectedAssets: multiple_images,
        materialOptions: MaterialOptions(
          actionBarTitle: "Sellanycar",
        ),
      );
    } on Exception catch (e) {
    }
    setState(() {
      multiple_images = resultList;
    });
  }

  // ===========================================
  Future<List<String>> getCategoryList() async {
    final response = await http.get(Uri.parse(AllApi.get_categoriesApi));
    final jsonResponse = jsonDecode(response.body);
    int status = jsonResponse['status'];
    String message = jsonResponse['message'];
    var data = jsonResponse['data'];
    if (status == 0) {
      showToast(message);
    }
    else {
      category_list = data;
      category_list_strlist = [for(int i = 0; i < category_list.length; i++)
        category_list[i]['category_name'].toString()
      ];
    }
  }
// ===============================================


  post_add(String vehicle_name, brand, model, mileage, description, Image) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userAuthorization = prefs.getString(AllApi.userAuthorization);

    if (vehicle_nameController.text.isEmpty) {
      Navigator.pop(this.context);
      showToast('Please enter vehicle name');
    }
    else if (brandController.text.isEmpty) {
      Navigator.pop(this.context);
      showToast('Please enter brand');
    }
    else if (priceController.text.isEmpty) {
      Navigator.pop(this.context);
      showToast('Please enter price');
    }
    else if (descriptionController.text.isEmpty) {
      Navigator.pop(this.context);
      showToast('Please enter description');
    }
    else if (multiple_images.length == 0) {
      Navigator.pop(this.context);
      showToast('Please upload image');
    }
    else {
      postImage_strlist =
      [for(int i = 0; i < multiple_images.length; i++)multiple_images[i].name];
      String images_str_array = jsonEncode(postImage_strlist);

      Uri uri = Uri.parse(AllApi.post_addApi);
      // create multipart request
      http.MultipartRequest request = http.MultipartRequest("POST", uri);

      // ======= this is for multiple image===========
      List<http.MultipartFile> multipartImageList = new List<http.MultipartFile>();
      for (Asset asset in multiple_images) {
        ByteData byteData = await asset.getByteData();
        List<int> imageData = byteData.buffer.asUint8List();
        http.MultipartFile multipartFile = new http.MultipartFile.fromBytes(
          'image[]',
          imageData,
          filename: '${DateTime
              .now()
              .millisecondsSinceEpoch}.jpg',
          contentType: MediaType("image", "jpg"),
        );
        multipartImageList.add(multipartFile);
      }
      // =======this is for single image============
      // multipartFile =  http.MultipartFile.fromBytes('image[]', final_image_data_multipart, filename: '${DateTime.now().millisecondsSinceEpoch}.jpg', contentType: MediaType("image", "jpg"),);

      //add text fields
      request.fields["vehicle_name"] = vehicle_nameController.text;
      request.fields["brand"] = brandController.text;
      request.fields["model"] = modelController.text;
      request.fields["mileage"] = mileageController.text;
      request.fields["category_id"] = makeController.text;
      request.fields["description"] = descriptionController.text;
      request.fields["price"] = priceController.text;
      request.fields["offer_price"] = offer_priceController.text;
      request.fields["vehicle_no"] = vehicle_no_Controller.text;
      request.fields["engine_capacity"] = engine_capacity_Controller.text;
      request.fields["engine_number"] = engine_no_Controller.text;
      request.fields["weight"] = weightController.text;
      request.fields["colour"] = colorController.text;
      request.fields["latitude"] = '22.7196';
      request.fields["longitude"] = '75.8577';
      request.fields["location"] = 'Indore';
      request.headers["Authorizations"] = userAuthorization;
      //
      // // add file to multipart
      request.files.addAll(multipartImageList);
      var response = await request.send();
      //
      // // Decode response
      final respStr = await response.stream.bytesToString();
      Navigator.pop(this.context);
      Navigator.of(this.context).push(
        MaterialPageRoute(builder: (BuildContext context) => Mypost_list()),);
    }
  }
  
  @override
  void initState() {
    setState(() {
      getStoragepermission();

      mileageController.text = widget.text_mileage;
      makeController.text = widget.text_make;
      modelController.text = widget.text_model;
      colorController.text = widget.text_color;
      engine_no_Controller.text = widget.text_engineno;
      engine_capacity_Controller.text = widget.text_enginecapacity;
      weightController.text = widget.text_weight;
      vehicle_no_Controller.text = widget.text_vehicleno;
    });

    super.initState();
  }

  void getStoragepermission() {
    var result = _permissionHandler.requestPermissions(
        [PermissionGroup.storage]);
    if (result == PermissionStatus.granted) {
      // permission was granted
    }
    else if (result == PermissionStatus.denied) {
      // permission was denied
    }
    else if (result == PermissionStatus.disabled) {
      // permission was disabled
    }
    else {
      // permission was disabled
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Sell Any Car",
          style: TextStyle(fontSize: 16,color: Color(0xFF8E2E7B),),),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Padding(
          padding: const EdgeInsets.only(left: 15,right: 15),
          child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 50,left: 30,right: 30),
                child: Image.asset("assets/Group.png"),
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Vehicle Name",style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                  CircleAvatar(
                    radius: 8,
                    backgroundColor: Colors.green,
                    child: Icon(Icons.done,color: Colors.white,size: 12,),
                  ),
                ],
              ),
              SizedBox(height: 5,),
              Card(
                child:Padding(
                  padding: const EdgeInsets.only(top: 5,bottom: 5,left: 5),
                  child: TextField(
                    controller: vehicle_nameController,
                      style: TextStyle(color: Colors.black),
                      cursorColor: Colors.black,
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          hintText: "Enter Vehicle Name",
                          hintStyle: TextStyle( color: Colors.black54,
                          fontSize: 13),
                          border:InputBorder.none
                      )
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Make",style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                  CircleAvatar(
                    radius: 8,
                    backgroundColor: Colors.green,
                    child: Icon(Icons.done,color: Colors.white,size: 12,),
                  ),
                ],
              ),
              SizedBox(height: 5,),
              Card(
                child:Padding(
                  padding: const EdgeInsets.only(top: 5,bottom: 5,left: 5),
                  child: TextField(
                      readOnly: true,
                    autofocus: true,
                      controller: makeController,
                      style: TextStyle(color: Colors.black),
                      cursorColor: Colors.black,
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          hintText: "Enter make",
                          hintStyle: TextStyle( color: Colors.black54,
                              fontSize: 13),
                          border:InputBorder.none
                      )
                  ),
                ),
              ),
              // SizedBox(height: 15,),
              // Text("Category",
              //   style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
              // SizedBox(height: 5,),
              // Card(
              //   child: Padding(
              //     padding: const EdgeInsets.only(top: 5, bottom: 10, left: 20, right: 20),
              //     child: Column(
              //       children: [
              //         DropdownButton(
              //           isExpanded: true,
              //           underline: Container(),
              //           value: _selectedCategory,
              //           onChanged: (newValue) {
              //             setState(() {
              //               _selectedCategory = newValue;
              //             });
              //           },
              //           items:category_list_strlist.map((catTitle) => DropdownMenuItem(
              //               value: catTitle, child: Text("$catTitle")))
              //               .toList(),
              //         ),
              //       ],
              //     ),
              //   ),
              // ),
              SizedBox(height: 15,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Brand",style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                  CircleAvatar(
                    radius: 8,
                    backgroundColor: Colors.green,
                    child: Icon(Icons.done,color: Colors.white,size: 12,),
                  ),
                ],
              ),
              SizedBox(height: 5,),
              Card(
                child:Padding(
                  padding: const EdgeInsets.only(top: 5,bottom: 5,left: 5),
                  child: TextField(
                      controller: brandController,
                      style: TextStyle(color: Colors.black),
                      cursorColor: Colors.black,
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          hintText: "Enter Brand Name",
                          hintStyle: TextStyle( color: Colors.black54,
                              fontSize: 13),
                          border:InputBorder.none
                      )
                  ),
                ),
              ),
              SizedBox(height: 15,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Engine Number",style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                  CircleAvatar(
                    radius: 8,
                    backgroundColor: Colors.green,
                    child: Icon(Icons.done,color: Colors.white,size: 12,),
                  ),
                ],
              ),
              SizedBox(height: 5,),
              Card(
                child:Padding(
                  padding: const EdgeInsets.only(top: 5,bottom: 5,left: 5),
                  child: TextField(
                      readOnly: true,
                    autofocus: true,
                      controller: engine_no_Controller,
                      style: TextStyle(color: Colors.black),
                      cursorColor: Colors.black,
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          hintText: "Enter Engine No",
                          hintStyle: TextStyle( color: Colors.black54,
                              fontSize: 13),
                          border:InputBorder.none
                      )
                  ),
                ),
              ),
              SizedBox(height: 15,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Vehicle Number",style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                  CircleAvatar(
                    radius: 8,
                    backgroundColor: Colors.green,
                    child: Icon(Icons.done,color: Colors.white,size: 12,),
                  ),
                ],
              ),
              SizedBox(height: 5,),
              Card(
                child:Padding(
                  padding: const EdgeInsets.only(top: 5,bottom: 5,left: 5),
                  child: TextField(
                      readOnly: true,
                      autofocus: true,
                      controller: vehicle_no_Controller,
                      style: TextStyle(color: Colors.black),
                      cursorColor: Colors.black,
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          hintText: "Enter Engine No",
                          hintStyle: TextStyle( color: Colors.black54,
                              fontSize: 13),
                          border:InputBorder.none
                      )
                  ),
                ),
              ),
              SizedBox(height: 15,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Engine Capacity",style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                  CircleAvatar(
                    radius: 8,
                    backgroundColor: Colors.green,
                    child: Icon(Icons.done,color: Colors.white,size: 12,),
                  ),
                ],
              ),
              SizedBox(height: 5,),
              Card(
                child:Padding(
                  padding: const EdgeInsets.only(top: 5,bottom: 5,left: 5),
                  child: TextField(
                      readOnly: true,
                    autofocus: true,
                      controller: engine_capacity_Controller,
                      style: TextStyle(color: Colors.black),
                      cursorColor: Colors.black,
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          hintText: "Enter Engine capacity",
                          hintStyle: TextStyle( color: Colors.black54,
                              fontSize: 13),
                          border:InputBorder.none
                      )
                  ),
                ),
              ),
              SizedBox(height: 15,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Weight",style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                  CircleAvatar(
                    radius: 8,
                    backgroundColor: Colors.green,
                    child: Icon(Icons.done,color: Colors.white,size: 12,),
                  ),
                ],
              ),
              SizedBox(height: 5,),
              Card(
                child:Padding(
                  padding: const EdgeInsets.only(top: 5,bottom: 5,left: 5),
                  child: TextField(
                      readOnly: true,
                    autofocus: true,
                      controller: weightController,
                      style: TextStyle(color: Colors.black),
                      cursorColor: Colors.black,
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          hintText: "Enter weight",
                          hintStyle: TextStyle( color: Colors.black54,
                              fontSize: 13),
                          border:InputBorder.none
                      )
                  ),
                ),
              ),
              SizedBox(height: 15,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Color",style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                  CircleAvatar(
                    radius: 8,
                    backgroundColor: Colors.green,
                    child: Icon(Icons.done,color: Colors.white,size: 12,),
                  ),
                ],
              ),
              SizedBox(height: 5,),
              Card(
                child:Padding(
                  padding: const EdgeInsets.only(top: 5,bottom: 5,left: 5),
                  child: TextField(
                      readOnly: true,
                    autofocus: true,
                      controller: colorController,
                      style: TextStyle(color: Colors.black),
                      cursorColor: Colors.black,
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          hintText: "Enter Brand Name",
                          hintStyle: TextStyle( color: Colors.black54,
                              fontSize: 13),
                          border:InputBorder.none
                      )
                  ),
                ),
              ),
              SizedBox(height: 15,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Model",style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                  CircleAvatar(
                    radius: 8,
                    backgroundColor: Colors.green,
                    child: Icon(Icons.done,color: Colors.white,size: 12,),
                  ),
                ],
              ),
              SizedBox(height: 5,),
              Card(
                child:Padding(
                  padding: const EdgeInsets.only(top: 5,bottom: 5,left: 5),
                  child: TextField(
                      readOnly: true,
                    autofocus: true,
                    controller: modelController,
                      style: TextStyle(color: Colors.black),
                      cursorColor: Colors.black,
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          hintText: "Enter Model",
                          hintStyle: TextStyle( color: Colors.black54,
                              fontSize: 13),
                          border:InputBorder.none
                      )
                  ),
                ),
              ),
              SizedBox(height: 15,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Price",
                    style: TextStyle(fontSize: 14,
                        fontWeight: FontWeight.bold),),
                  CircleAvatar(
                    radius: 8,
                    backgroundColor: Colors.green,
                    child: Icon(Icons.done,color: Colors.white,size: 12,),
                  ),
                ],
              ),
              SizedBox(height: 5,),
              Card(
                child:Padding(
                  padding: const EdgeInsets.only(top: 5,bottom: 5,left: 5),
                  child: TextFormField(
                      keyboardType: TextInputType.phone,
                    controller: priceController,
                      style: TextStyle(color: Colors.black),
                      cursorColor: Colors.black,
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          hintText: "Enter Price Here",
                          hintStyle: TextStyle( color: Colors.black54,
                              fontSize: 13),
                          border:InputBorder.none
                      )
                  ),
                ),
              ),
              SizedBox(height: 15,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Offer Price",
                    style: TextStyle(fontSize: 14,
                        fontWeight: FontWeight.bold),),
                  CircleAvatar(
                    radius: 8,
                    backgroundColor: Colors.green,
                    child: Icon(Icons.done,color: Colors.white,size: 12,),
                  ),
                ],
              ),
              SizedBox(height: 5,),
              Card(
                child:Padding(
                  padding: const EdgeInsets.only(top: 5,bottom: 5,left: 5),
                  child: TextFormField(
                      keyboardType: TextInputType.phone,
                      controller: offer_priceController,
                      style: TextStyle(color: Colors.black),
                      cursorColor: Colors.black,
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          hintText: "Enter Price Here",
                          hintStyle: TextStyle( color: Colors.black54,
                              fontSize: 13),
                          border:InputBorder.none
                      )
                  ),
                ),
              ),

              SizedBox(height: 15,),
              Text("Upload Photos",
                style: TextStyle(fontSize: 14,
                    fontWeight: FontWeight.bold),),
              SizedBox(height: 15,),
              // multiple_images == null
              //     ? Image.asset('assets/Group.png',
              //   width:MediaQuery.of(context).size.width*0.9,
              //   height:MediaQuery.of(context).size.height*0.3,
              // )
              //     : Image.file(_image,fit: BoxFit.cover, width:MediaQuery.of(context).size.width*0.9,
              //   height:MediaQuery.of(context).size.height*0.3,),
              // SizedBox(height: 10,),
              Container(
                height: 100,
                child: ListView(
                  physics : NeverScrollableScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  children: [
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: GestureDetector(
                          onTap: (){

                            pickImages();
                            // _showMyDialog(true);
                          },
                          child: Container(
                              color: Colors.black26,
                              width: 70,height: 70,
                              child: Icon(Icons.add)),
                        ),
                      ),
                    ),
                    Container(
                      height: 100,
                      width:  MediaQuery.of(context).size.width,
                      child: ListView.builder(
                          physics : ScrollPhysics(),
                          scrollDirection: Axis.horizontal,
                          itemCount: multiple_images.length,
                          itemBuilder: (BuildContext context,int index){
                            Asset asset = multiple_images[index];
                            return
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  width: 70,height: 70,
                                  child: multiple_images == null
                                      ? Image.asset('assets/Group.png',fit: BoxFit.cover,)
                                      :AssetThumb(
                                        asset: asset,
                                        width: 300,
                                        height: 300,
                                      ),),
                              );
                          }
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 15,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Mileage",style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                  CircleAvatar(
                    radius: 8,
                    backgroundColor: Colors.green,
                    child: Icon(Icons.done,color: Colors.white,size: 12,),
                  ),
                ],
              ),
              SizedBox(height: 5,),
              Card(
                child:Padding(
                  padding: const EdgeInsets.only(top: 5,bottom: 5,left: 5),
                  child: TextField(
                      readOnly: true,
                    controller: mileageController,
                      style: TextStyle(color: Colors.black),
                      cursorColor: Colors.black,
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          hintText: "Options",
                          hintStyle: TextStyle( color: Colors.black54,
                          fontSize: 13),
                          border:InputBorder.none
                      )
                  ),
                ),
              ),
              SizedBox(height: 15,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Description",style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                  CircleAvatar(
                    radius: 8,
                    backgroundColor: Colors.green,
                    child: Icon(Icons.done,color: Colors.white,size: 12,),
                  ),
                ],
              ),
              SizedBox(height: 5,),
              Card(
                child:Padding(
                  padding: const EdgeInsets.only(top: 5,bottom: 50,left: 5),
                  child: TextField(
                    controller: descriptionController,
                      style: TextStyle(color: Colors.black),
                      cursorColor: Colors.black,
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          hintText: "Description",
                          hintStyle: TextStyle( color: Colors.black54,
                          fontSize: 13),
                          border:InputBorder.none
                      )
                  ),
                ),
              ),
              SizedBox(height: 20,),
              FlatButton(
                  color:  Color(0xFF8E2E7B),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 18,bottom: 18),
                    child: Text("PUBLISH",
                      style: TextStyle(color: Colors.white),),
                  ),
                onPressed: () async {
                  await loginAction();
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return Center(
                          child: CircularProgressIndicator(),);
                      });
                  await loginAction();

                  post_add(vehicle_nameController.text,brandController.text,modelController.text,mileageController.text,descriptionController.text,Image);
                },
              ),
              SizedBox(height: 30,),
            ],
          ),
        ),
      ),
    );
  }



}


