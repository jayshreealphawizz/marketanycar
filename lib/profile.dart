import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'comman/_bottomAppBar.dart';
import 'log/allapi.dart';
import 'log/log.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';


import 'log/login.dart';

bool isKeyboardOpen = false;

class Profile extends StatefulWidget {
const  Profile({Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  File _image;

  String Imagepath ;
  String result = "";
  String FinalImage;
  final _picker = ImagePicker();
  String userAuthorization ,useremail,userfirst_name,usermobile;
  String useraddress , userimage;
  TextEditingController dateCtl =new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();
  TextEditingController mobileController = new TextEditingController();
  TextEditingController addressController = new TextEditingController();
  TextEditingController imageController = new TextEditingController();
  String imagePATH;
  Future<Post> post;
  SharedPreferences prefs4444;

  // ignore: non_constant_identifier_names
  Seveid(String name, email,mobile, address) async {
 String Image = FinalImage;

    if(Image==null){
   Image='';
    }
    Map data = {
      "name":name,
      "email": email,
      "mobile":mobile,
      "address": address,
      "image": Image,
    };
    Map<String, String> requestHeaders = {
     'Authorizations': userAuthorization
    };

    var jsondata = null;
    var response = await http.post(Uri.parse(AllApi.profile_updateApi),body: data,
      headers:requestHeaders);
    if(response.statusCode==200){
      jsondata = jsonDecode(response.body);
      int status =  jsondata['status'];
      String message =  jsondata['message'];
      Image =  jsondata['image'];
      if(status==0){
        showToast(message);
      }
      else {
        setState(() {
          // Navigator.of(context).push(
          //   MaterialPageRoute(builder: (BuildContext context) => Deshbord()),);
          showToast(message);
          String id = jsondata['data']['id'];
          String email = jsondata['data']['email'];
          String name = jsondata['data']['first_name'];
          String mobile = jsondata['data']['mobile'];
          String address = jsondata['data']['address'];
          String Image = jsondata['data']['image'];
          String Authorization = jsondata['data']['Authorization'];
          // String status =  jsondata['status'];

          addStringToSF(id, Authorization,email,name,mobile,address,Image);
        });
      }
    }
    else {
      throw Exception('Failed to load post');
    }
  }

  addStringToSF(String id ,Authorization,email,first_name,mobile,address,Image) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(AllApi.userId, id);
    prefs.setString(AllApi.useremail, email);
    prefs.setString(AllApi.userfirst_name, first_name);
    prefs.setString(AllApi.usermobile, mobile);
    prefs.setString(AllApi.useraddress, address);
    prefs.setString(AllApi.userimage, Image);
    prefs.setString(AllApi.userAuthorization , Authorization);
    // return json.decode(prefs.getString("gender"));
  }


  Future getImagefromcamera() async {
     final image = await _picker.getImage(source: ImageSource.camera);
   // final image = await ImagePicker.pickImage(source: ImageSource.camera);
    imagePATH = image.path;
    setState(() {
      File file_IMAGE = new File(imagePATH);
      _image = file_IMAGE;
      FinalImage= base64Encode(file_IMAGE.readAsBytesSync());
      Navigator.of(context).pop(false);
    });
  }

  Future getImagefromGallery() async {
     final image = await _picker.getImage(source: ImageSource.gallery);
    //final image = await ImagePicker.pickImage(source: ImageSource.gallery);
    imagePATH = image.path;
    setState(() {
      File file_IMAGE = new File(imagePATH);
      _image = file_IMAGE;
      FinalImage= base64Encode(file_IMAGE.readAsBytesSync());
      Navigator.of(context).pop(false);
    });
  }
  @override
  void initState() {
    setState(() {
      SharedPreferences.getInstance().then((SharedPreferences sp) {
        prefs4444 = sp;
        userAuthorization = prefs4444.getString(AllApi.userAuthorization);
        useremail = prefs4444.getString(AllApi.useremail);
        userfirst_name = prefs4444.getString(AllApi.userfirst_name);
        usermobile = prefs4444.getString(AllApi.usermobile);
        useraddress = prefs4444.getString(AllApi.useraddress);
        userimage = prefs4444.getString(AllApi.userimage);

        if(userAuthorization==null){
          Navigator.of(context).pop();
          Navigator.push(context,
              MaterialPageRoute(builder:
                  (context) => Loginpage()
              ));
        }
        else{
          mobileController.text = usermobile;
          nameController.text = userfirst_name;
          emailController.text = useremail;
          if(useraddress.isEmpty){
            addressController.text = '';
          }else{
          addressController.text = useraddress;}
          isKeyboardOpen = false;
          hidekeyboard();
        }
      });

    });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    hidekeyboard();
  }

  Future<bool> onBackPress() {
    FocusScope.of(context).requestFocus(new FocusNode());
    Navigator.pop(context);
    isKeyboardOpen = false;
    return Future.value(false);
  }

  void hidekeyboard(){
    if(!isKeyboardOpen) {
      FocusScope.of(context).requestFocus(new FocusNode());
      isKeyboardOpen = true;
    }
    else{
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Edit Profile",
              style: TextStyle(fontSize: 16,color: Color(0xFF8E2E7B))),
        ),
        body:
        Form(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.only(left: 15,right: 15),
              child: ListView(
                children: [
                  SizedBox(height: 20,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(100),
                            child: Container(
                              color: Colors.black12,
                              height: 110,width: 110,
                              child: _image == null
                                  ? Image.network(userimage.toString().trim(),fit: BoxFit.cover,)
                                  : Image.file(_image,fit: BoxFit.cover,),
                            ),
                            // backgroundImage: AssetImage("assets/download.jpg",),
                            // radius: 60,
                          ),
                          Positioned(
                            left: 85,
                            child: GestureDetector(
                              onTap: (){
                                _showMyDialog(true);
                              },
                              child: CircleAvatar(
                                radius: 13,
                                  backgroundColor: Colors.white,
                                  child: Icon(Icons.edit,
                                    color: Color(0xFF8E2E7B),size: 18,)),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),


                  SizedBox(height: 50,),
                  Text("Name",
                    style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                  SizedBox(height: 5,),
                  Material(
                    elevation: 3,
                    shadowColor: Colors.white,
                    child: TextFormField(
                      autofocus: true,
                      controller: nameController,
                        key: Key('name'),
                        style: TextStyle(color: Colors.black), // onFieldSubmitted: (String) {
                        cursorColor: Colors.black,
                      textInputAction: TextInputAction.next,
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          hintStyle: TextStyle( color: Colors.black87,fontSize: 10),
                          border:InputBorder.none
                        ),
                      validator: (val){
                        if(val.isEmpty){
                          return 'enter your name';
                        }
                      } ,
                    ),
                  ),
                  SizedBox(height: 15,),
                  Text("Email",style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                  SizedBox(height: 5,),
                  Material(
                    elevation: 3,
                    shadowColor: Colors.white,
                    child: TextFormField(
                    readOnly: true,
                      autofocus: true,
                      controller: emailController,
                        key:Key('email') ,
                        style: TextStyle(color: Colors.black),
                        cursorColor: Colors.black,
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            fillColor: Colors.white,
                            filled: true,
                            hintStyle: TextStyle( color: Colors.black87,fontSize: 10),
                            border:InputBorder.none
                        ),

                      validator: (val){
                        final RegExp nameExp =
                        new RegExp(r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$');
                        if(val.isEmpty){
                          return'Please enter email address.';
                        }
                        else if(!nameExp.hasMatch(val)){
                          return 'Please enter valid email address.';
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 15,),
                  Text("Mob",style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                  SizedBox(height: 5,),
                  Material(
                    elevation: 3,
                    shadowColor: Colors.white,
                    child: TextFormField(
                      maxLength: 10,
                      autofocus: true,
                      controller: mobileController,
                        key: Key('mobile'),
                        style: TextStyle(color: Colors.black),
                        cursorColor: Colors.black,
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                            counterText: "",
                            fillColor: Colors.white,
                            filled: true,
                            hintStyle: TextStyle( color: Colors.black87,fontSize: 10),
                            border:InputBorder.none
                        ),
                      validator: (val ){
                        RegExp nameExp = new RegExp(r'^([0-9]{10})$');
                        if (val != "") {
                          if (val.length != 10)
                            return " should be 10 digit";
                          else if (!nameExp.hasMatch(val))
                            return " should be in correct format";
                          return null;
                        } else {
                          return "Required";
                        }
                      },
                    ),
                  ),

                  SizedBox(height: 15,),
                  Text("Address",style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                  SizedBox(height: 5,),
                  Material(
                    elevation: 3,
                    shadowColor: Colors.white,
                    child: TextFormField(
                      autofocus: true,
                      controller: addressController,
                      key: Key('address'),
                      style: TextStyle(color: Colors.black),
                      cursorColor: Colors.black,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          hintStyle: TextStyle( color: Colors.black87,fontSize: 10),
                          border:InputBorder.none
                      ),
                      validator: (val){
                        if(val.isEmpty){
                          return 'enter your Addres';
                        }
                      } ,
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(bottom: 30,top: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        RaisedButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.only(top: 12,bottom: 12,left: 40,right: 40),
                              child: Text("Save",style: TextStyle(color: Colors.white ,fontSize: 12),),
                            ),
                            color: Color(0xFF8E2E7B),
                            onPressed: () async {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return Center(child: CircularProgressIndicator(),);
                                  });
                              await loginAction();
                              Navigator.pop(context);
                              // if (_formKey.currentState.validate()) {
                                Seveid(nameController.text,emailController.text,mobileController.text,addressController.text);
                              // }
                              // else{
                              //   print("data is empty");
                              // }
                            },
                            // =>Navigator.push(context,
                            //     MaterialPageRoute(builder:
                            //         (context) => Deshbord()
                            //     )
                            // )
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        bottomNavigationBar: Bbar(),
      ),
      onWillPop: onBackPress,
    );
  }

  Future<void> _showMyDialog(bool enable) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return AlertDialog(
                  title: Text('Select...'),

                  actions: <Widget>[
                    Column(
                      children: [
                        Row(
                          children: <Widget>[
                            FloatingActionButton(
                              onPressed: getImagefromcamera,
                              tooltip: "pickImage",
                              child: Icon(Icons.add_a_photo),

                            ),
                            SizedBox(width: 50,),
                            FloatingActionButton(
                              onPressed: getImagefromGallery,
                              tooltip: "Pick Image",
                              child: Icon(Icons.camera_alt),

                            ),SizedBox(width: 20,),

                          ],
                        ),
                        SizedBox(height: 20,),
                        Row(
                          children: <Widget>[
                            Text("Camera"),
                            SizedBox(width: 50,),
                            Text("Gallery")
                            ,SizedBox(width: 20,),
                          ],
                        ),
                        SizedBox(height: 20,),
                      ],
                    ),
                  ],
                );
              });
        });
  }

}
