import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'log/allapi.dart';

class Comments extends StatefulWidget {
  final String text_PostId;

  const Comments({Key key, this.text_PostId}) : super(key: key);

  @override
  _CommentsState createState() => _CommentsState();
}

class _CommentsState extends State<Comments> {
  String userAuthorization,userimage;
  List get_comment_list;
  TextEditingController commentController = new TextEditingController();
  final ScrollController _scrollController = ScrollController();

  comment_list(String post_id,) async {
    Map data = {
      'post_id':post_id,
    };
    Map<String, String> requestHeaders = {
      'Authorizations': userAuthorization
    };
    var jsondata = null;

    var response = await http.post(Uri.parse(AllApi.get_comment_listApi),body: data,
        headers:requestHeaders);
    if(response.statusCode==200){
      jsondata = jsonDecode(response.body);
      int status =  jsondata['status'];
      String message =  jsondata['message'];
      var data_list =  jsondata['data'];
      if(status==0){
        showToast(message);
      }
      else {
        setState(() {
          get_comment_list = data_list;
        });
      }
    }
    else {
      throw Exception('Failed to load post');
    }
  }

  _nameRetriever() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userimage = prefs.getString(AllApi.userimage);
    userAuthorization = prefs.getString(AllApi.userAuthorization);
    comment_list(widget.text_PostId);

  }

  @override
  void initState() {
    setState(() {
      _nameRetriever();
    });
    super.initState();
  }

  void postcomment(String comment,String postId) async {
    if(comment.isEmpty){
      showToast('Please enter comment before click add button');
    }
    else {
      Map data = {
        "post_id": postId,
        'comment': comment,
      };
      Map<String, String> requestHeaders = {
        'Authorizations': userAuthorization
      };
      var jsondata = null;
      var response = await http.post(
          Uri.parse(AllApi.post_commentApi), body: data, headers: requestHeaders);

      if (response.statusCode == 200) {
        jsondata = jsonDecode(response.body);
        int status = jsondata['status'];
        String message = jsondata['message'];
        if (status == 0) {
          showToast(message);
        }
        else {
          setState(() {
            commentController.text = '';
            showToast(message);
            comment_list(postId);
          });
        }
      }
      else {
        throw Exception('Failed to load post');
      }
    }
  }


  @override
  Widget build(BuildContext context) {
    setState(() {
      // _scrollController.animateTo(
      //     _scrollController.position.maxScrollExtent,
      //     duration: Duration(milliseconds: 500),
      //     curve: Curves.fastOutSlowIn);
    });

    return Scaffold(
      appBar: AppBar(
        title: Text("Comments",
          style: TextStyle(fontSize: 16,color: Color(0xFF8E2E7B)),),
      ),
      body: Stack(
        children: [
          ListView(
            children: [
                 get_comment_list==null?Container(width: 150,height:450,child: Center(child: Text('No Records found')),):Container(
                 // get_comment_list==null?Container(width: MediaQuery.of(context).size.width,height:MediaQuery.of(context).size.height,child: Center(child: Text('No Records found')),):Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.only(top: 10,left:10,right: 10,bottom: 140),
                  child: ListView.builder(
                    controller: _scrollController,
                    itemCount: get_comment_list.length,
                    itemBuilder: (BuildContext context,int index){
                      return _Row_list(get_comment_list[index]);
                    },
                  ),
                ),
              ),
            ],
          ),
            Positioned(
              bottom: 0,
              child:
              Container(color: Colors.white, width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.only(left: 10,right: 10,top: 5,bottom: 3),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          CircleAvatar(
                            backgroundImage: userimage==null?AssetImage("assets/placeholder_user.png",):
                            NetworkImage(userimage),
                            radius: 20,
                          ),
                          SizedBox(width: 8,),
                          Container(
                              width: 200,
                              height: 30,
                              child: TextFormField(
                                key: Key("comment"),
                                controller: commentController,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintStyle: TextStyle(fontSize: 12),
                                  hintText: 'Add Comment...',),
                              )),
                        ],
                      ),
                      IconButton(icon: Icon(Icons.add_circle_outline,size: 18,),
                        onPressed:()async {
                          postcomment(commentController.text,widget.text_PostId);
                        },)
                      // Icon(Icons.add_circle_outline,size: 18,)
                    ],
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
  Widget _Row_list(get_comment_list){
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          CircleAvatar(
            backgroundImage: NetworkImage(AllApi.user_image_url+get_comment_list['userimage']),
            radius: 30,
          ),
          SizedBox(width: 10,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(get_comment_list['username'],
                    style: TextStyle(fontWeight: FontWeight.bold,
                        fontSize: 15),),
                  // Column(
                  //   children: [
                  //     Text(" mention you in a comments:",
                  //       style: TextStyle(fontSize: 13),),
                  //   ],
                  // ),
                ],
              ),
              SizedBox(width: 8,),
              Text(get_comment_list['comment'],
                style: TextStyle(fontSize: 13),),
            ],
          ),
        ],),
    );
  }
}
