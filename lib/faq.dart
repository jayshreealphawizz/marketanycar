import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:getwidget/getwidget.dart';
import 'package:http/http.dart' as http;
import 'package:multi_image_picker/multi_image_picker.dart';

import 'comman/_bottomAppBar.dart';
import 'log/allapi.dart';
import 'log/log.dart';
import 'notifications.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class MyStructure {
  final int id;
  final String text;
  MyStructure({this.id, this.text});
}

class Faq extends StatefulWidget {
  @override
  _FaqState createState() => _FaqState();
}

class MyWidget extends StatelessWidget {
  final MyStructure widgetStructure;
  MyWidget(this.widgetStructure);


  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.green,
        child: Text('${widgetStructure.id} ${widgetStructure.text}')
    );
  }
}

class _FaqState extends State<Faq> {
  SwiperController controller;
  List<bool> autoplayes;
  List<SwiperController> controllers;
  List<MyStructure> widgetList = [];



  List data_whole_list;

  List<String> postImage_strlist;
  List<Asset> multiple_images = List<Asset>();

  Future<List<Post>> getJasonData() async {
    final response = await http.get(Uri.parse(AllApi.faq_listApi));

    final jsonResponse = jsonDecode(response.body);
    int status = jsonResponse['status'];
    String message = jsonResponse['message'];
    var data = jsonResponse['data'];

    if (status == 0) {
      showToast(message);
    } else {
      setState(() {
        data_whole_list = data;
      });
    }
  }

  // @override
  // void initState() {
  //   getJasonData();
  //   // TODO: implement initState
  //   super.initState();
  // }




  @override
  void initState() {
    controller = new SwiperController();
    autoplayes = new List()
      ..length = 10
      ..fillRange(0, 10, false);
    controllers = new List()
      ..length = 10
      ..fillRange(0, 10, new SwiperController());

    for(int i=0;i < 10; i++) {
      widgetList.add(MyStructure(id:i,text: ' this is index ${i}'));
    }

    super.initState();
  }

// ==============================

//   ============================
//   @override
//   Widget build(BuildContext context) {
//     return data_whole_list == null ? Scaffold()
//         : DefaultTabController(
//       length: 3,
//           child: Scaffold(
//               appBar: AppBar(
//                 title: Text(
//                   "FAQ",
//                   style: TextStyle(fontSize: 16, color: Color(0xFF8E2E7B)),
//                 ),
//                 actions: [
//                   GestureDetector(
//                     child: Icon(
//                       Icons.notifications_none,
//                     ),
//                     onTap: () => Navigator.push(context,
//                         MaterialPageRoute(builder: (context) => Notifications())),
//                   ),
//                   SizedBox(
//                     width: 10,
//                   ),
//                 ],
//               ),
//               body: Form(
//                 key: _formKey,
//                 child: Stack(
//                   children: [
//                     Container(
//                       width: MediaQuery.of(context).size.width,
//                       height: MediaQuery.of(context).size.height,
//                       child: ListView(
//                         children: [
//                           Container(
//                             color: Colors.black12,
//                             height: MediaQuery.of(context).size.height * 0.25,
//                             width: MediaQuery.of(context).size.width,
//                             child: Padding(
//                               padding: const EdgeInsets.only(left: 75, right: 75),
//                               child: Image.asset(
//                                 "assets/applogo.png",
//                               ),
//                             ),
//                           ),
//                           SizedBox(
//                             height: 10,
//                           ),
//                           data_whole_list.isEmpty
//                               ? Container()
//                               : Container(
//                                   width: MediaQuery.of(context).size.width,
//                                   height: MediaQuery.of(context).size.height,
//                                   child: Padding(
//                                     padding: const EdgeInsets.only(
//                                         left: 15, right: 15),
//                                     child: ListView.builder(
//                                         physics: ScrollPhysics(),
//                                         itemCount: data_whole_list.length,
//                                         itemBuilder:
//                                             (BuildContext context, int index) {
//                                           return GFAccordion(
//                                             contentBorderRadius: BorderRadius.all(
//                                                 Radius.circular(12.0)),
//                                             expandedIcon: Icon(
//                                               Icons.arrow_drop_up,
//                                               color: Colors.white,
//                                             ),
//                                             collapsedIcon: Icon(
//                                               Icons.arrow_drop_down,
//                                               color: Colors.white,
//                                             ),
//                                             collapsedTitleBackgroundColor:
//                                                 Colors.black,
//                                             contentBackgroundColor: Colors.white,
//                                             expandedTitleBackgroundColor:
//                                                 Color(0xFF8E2E7B),
//                                             title: data_whole_list[index]
//                                                 ['question'],
//                                             textStyle:
//                                                 TextStyle(color: Colors.white),
//                                             content: data_whole_list[index]
//                                                 ['answer'],
//                                           );
//                                         }),
//                                   ),
//                                 ),
//                         ],
//                       ),
//                     ),
//                     Container(
//                       color: Color(0xFF8E2E7B),
//                       height: 2,
//                     )
//                   ],
//                 ),
//               ),
//               bottomNavigationBar: Bbar(),
//             ),
//         );
//   }



  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: new Scaffold(
        body: new Swiper(
          loop: false,
          itemCount: widgetList.length,
          controller: controller,
          pagination: new SwiperPagination(),
          itemBuilder: (BuildContext context, int index) {
            return new Column(
              children: <Widget>[
                new SizedBox(
                  child: new Swiper(
                    controller: controllers[index],
                    pagination: new SwiperPagination(),
                    itemCount: widgetList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return MyWidget(widgetList[index]);
                    },
                    autoplay: autoplayes[index],
                  ),
                  height: 300.0,
                ),
                new RaisedButton(
                  onPressed: () {
                    setState(() {
                      autoplayes[index] = true;
                    });
                  },
                  child: new Text("Start autoplay"),
                ),
                new RaisedButton(
                  onPressed: () {
                    setState(() {
                      autoplayes[index] = false;
                    });
                  },
                  child: new Text("End autoplay"),
                ),
                new Text("is autoplay: ${autoplayes[index]}")
              ],
            );
          },
        ),
      ),
    );
  }
}
