import 'dart:convert';

import 'package:flutter/material.dart';
import 'cardetails.dart';
import 'comman/_bottomAppBar.dart';
import 'comman/_drawer.dart';
import 'filter.dart';
import 'log/allapi.dart';
import 'notifications.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';


class Carlist extends StatefulWidget {

  String text_dashbord_cat;
  String text1_dashbord_subcat;
  String text2_dashbord_childsubcat;

   String text_filter_Postprice;
   String text_filter_postcategory;
   String text_filter_postal_code;
   String text_filter_distance;
   String text_filter_body_type;
   String text_filter_Mileage;
   String text_filter_get_year;
   String text_filter_buying;
   String text_from;

  Carlist({
    Key key,

    @required this.text_dashbord_cat,
    @required this.text1_dashbord_subcat,
    @required this.text2_dashbord_childsubcat,

    @required this.text_filter_Postprice,
    @required this.text_filter_postcategory,
    @required this.text_filter_postal_code,
    @required this.text_filter_distance,
    @required this.text_filter_body_type,
    @required this.text_filter_Mileage,
    @required this.text_filter_get_year,
    @required this.text_filter_buying,
    @required this.text_from,
  }) : super(key: key);

  @override
  _CarlistState createState() => _CarlistState();
}
class _CarlistState extends State<Carlist> {

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
 String final_post_catid;
//  int _selectIndex = 0;
  List get_search_post_list ;
  get_search_post_data() async {

    if(widget.text_filter_Postprice==null){
      widget.text_filter_Postprice = '';
    }
    if(widget.text_filter_postal_code==null){
      widget.text_filter_postal_code = '';
    }
    if(widget.text_filter_distance==null){
      widget.text_filter_distance = '';
    }
    if(widget.text_filter_body_type==null){
      widget.text_filter_body_type = '';
    }
    if(widget.text_filter_Mileage==null){
      widget.text_filter_Mileage = '';
    }
    if(widget.text_filter_get_year==null){
      widget.text_filter_get_year = '';
    }
    if(widget.text_filter_buying==null){
      widget.text_filter_buying = '';
    }
    if(widget.text1_dashbord_subcat==null){
      widget.text1_dashbord_subcat = '';
    }
    if(widget.text2_dashbord_childsubcat==null){
      widget.text2_dashbord_childsubcat = '';
    }
    if(select==null){
      select = '';
    }


    if(widget.text_from=='filter'){
      final_post_catid = widget.text_filter_postcategory;
    }
    else{
      final_post_catid = widget.text_dashbord_cat;
    }

    Map data = {
    'pageNumber':'1',
    'pageSize':'10',
    'category_id':final_post_catid ,
    'subcat_id':widget.text1_dashbord_subcat,
    'childsubcat_id': widget.text2_dashbord_childsubcat,
    'postal_code':widget.text_filter_postal_code,
    'buying_with':widget.text_filter_buying,
    'body_type':widget.text_filter_body_type,
    'mileage':widget.text_filter_Mileage,
    'min_year':widget.text_filter_get_year,
    'price_sort':select,
      'distance':widget.text_filter_distance,
      'latitude':'',//'22.9676',
      'longitude':'',//'75.8577',
      'min_price':widget.text_filter_Postprice,
    };

    var jsondata = null;
    var response = await http.post(Uri.parse(AllApi.get_search_post_listApi),body: data,);
    if(response.statusCode==200){
      jsondata = jsonDecode(response.body);
      int status =  jsondata['status'];
      String message =  jsondata['message'];
      var data_list =  jsondata['data'];
      if(status==0){
        showToast(message);
      }
      else {
        setState(() {
          get_search_post_list = data_list;
          (context as Element).reassemble();
        });
      }
    }
    else {
      throw Exception('Failed to load post');
    }
  }

  @override
  void initState() {
    setState(() {
      get_search_post_data();
    });
    super.initState();
  }
  List price=["1","2","3"];
  String select  ;

  addRadioButton( String title,int btnValue,) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 15),
          child: Text(title,style: TextStyle(fontSize: 14),),
        ),
        Radio(
          activeColor: Theme.of(context).primaryColor,
          value: price[btnValue],
          groupValue: select,
          onChanged: (value){
            setState(() {
              select=value;
            });
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    setState(() {

    });
    return DefaultTabController(
      length:3,
      child:get_search_post_list==0?Scaffold():Scaffold(
        key: _scaffoldKey,
        appBar: _AppBar(),
        body:Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 25),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Padding(
                  padding: const EdgeInsets.only(left: 10,right: 10,top: 20),
                  child: ListView.builder(
                    itemCount: get_search_post_list.length,
                    itemBuilder: (BuildContext context,int index){
                      return  _Card(index);
                    }
                  ),
                ),
              ),
            ),
            Container(
              child: Row(
                children: [
                  Expanded(
                    child: FlatButton(
                      color:  Color(0xFF8E2E7B),
                      child: Padding(
                        padding: const EdgeInsets.only(top:16.0, bottom: 16.0),
                        child: Row(
                          children: [
                            Icon(Icons.search,
                              size: 14,color: Colors.white,),
                            Text('NEW SEARCH',style: TextStyle(color: Colors.white,fontSize: 10),),
                          ],
                        ),
                      ),
                      onPressed:() =>{}
                    ),
                  ),
                  Expanded(
                    child: FlatButton(
                      color:  Color(0xFF8E2E7B),
                      child: Padding(
                        padding: const EdgeInsets.only(top:16.0, bottom: 16.0),
                        child: Row(
                          children: [
                            ImageIcon(
                              AssetImage("assets/icons/filter.png"),
                              color: Colors.white,
                              size: 14,
                            ),
                            Text('FILTER RESULT',style: TextStyle(color: Colors.white,fontSize: 10),),
                          ],
                        ),
                      ),
                      onPressed:() =>Navigator.push(context,
                          MaterialPageRoute(builder:
                              (context) => Filter()
                          )
                      ),
                    ),
                  ),

                  Expanded(
                    child: FlatButton(
                      color:  Color(0xFF8E2E7B),
                      child: Padding(
                        padding: const EdgeInsets.only(top:17.0, bottom: 17.0),
                        child: Text('SHOT BY',style: TextStyle(color: Colors.white,fontSize: 10),),
                      ),
                      onPressed: () => {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return _AlertDialog();
                            }),
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        drawer: MyDrawer(),
        bottomNavigationBar: Bbar(),
      ),
    );
  }
  Widget _AppBar(){
    return AppBar(
      title: Text("Car List",style: TextStyle(
        fontSize: 16,color:  Color(0xFF8E2E7B)
      ),),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 10),
          child: Row(
            children: [
              GestureDetector(
                child: Icon(Icons.notifications_none,),
                onTap: () =>Navigator.push(context,
                    MaterialPageRoute(builder:
                        (context) => Notifications()
                    )
                ),
              ),
              SizedBox(width: 10,),
              GestureDetector(
                child: Icon(Icons.filter_alt_outlined),
                onTap: () =>Navigator.push(context,
                    MaterialPageRoute(builder:
                        (context) => Filter()
                    )
                ),
              )
            ],
          ),
        )
      ],
    );
  }
  Widget _Card(int i){
    return  Padding(
      padding: const EdgeInsets.only(top: 10,),
      child: Container(
        color: Colors.black12,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.network(AllApi.user_image_url+get_search_post_list[i]['filename'],
                        fit: BoxFit.cover,
                        height: 150,
                        width: MediaQuery.of(context).size.width*0.35,),
                      // Image.asset("assets/car.jpg",
                      //   fit: BoxFit.cover,
                      //   height: 150,
                      //   width: MediaQuery.of(context).size.width*0.35,
                      // ),
                    ),
                    Positioned(
                      top: 5,
                      left: 5,
                      child: Container(
                        color: Color(0xFF8E2E7B),
                        width: 50,
                        height: 15,
                        child: Center(
                          child: Text("Featured Car",
                            style: TextStyle(fontSize: 6,color: Colors.white),),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10,),
                Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.network(AllApi.user_image_url+get_search_post_list[i]['filename'],
                        fit: BoxFit.cover,
                        height: 60,
                        width: MediaQuery.of(context).size.width*0.18,
                      ),
                    ),
                    SizedBox(width: 10,),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.network(AllApi.user_image_url+get_search_post_list[i]['filename'],
                        fit: BoxFit.cover,
                        height: 60,
                        width: MediaQuery.of(context).size.width*0.14,
                      ),
                    ),
                  ],
                )
              ],
            ),
            SizedBox(width: 10,),
            Expanded(
              child: GestureDetector(
                onTap: () =>Navigator.push(context,
                    MaterialPageRoute(builder:
                          (context) => Cardetails(text_PostId: get_search_post_list[i]['id'],)
                    )
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 10,),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text(get_search_post_list[i]['name'],
                              style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                          ),
                          // SizedBox(height: 5,),
                          Container(
                            color: Colors.green,
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Text("GOOD PRICE",style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 8
                              ),),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 5,),
                    Text(get_search_post_list[i]['description'],
                      style: TextStyle(fontSize: 12,),),
                    SizedBox(height: 5,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("\$"+get_search_post_list[i]['price'],
                              style: TextStyle(fontSize: 15,
                                  color: Color(0xFF8E2E7B),
                                  fontWeight: FontWeight.bold),),
                            SizedBox(height: 5,),
                            Text("\$"+get_search_post_list[i]['offer_price']+"pm (PCP)",
                              style: TextStyle(fontSize: 12,),),
                          ],
                        ),
                        // Image.asset("assets/Image 3.png",scale: 1.3,)
                      ],
                    ),
                    SizedBox(height: 5,),
                    Column(
                      children: [
                        Wrap(
                          alignment: WrapAlignment.start,
                          children: [
                            Column(children: [
                              CircleAvatar(
                                radius: 15,
                                backgroundColor: Colors.white,
                                child: ImageIcon(
                                  AssetImage("assets/icons/filter.png"),
                                  color:Color(0xFF8E2E7B) ,size: 18,
                                ),
                              ),
                              SizedBox(height: 3,),
                              Text("Manual",
                                style: TextStyle(fontSize: 8), )
                            ],),
                            Column(children: [
                              CircleAvatar(
                                radius: 15,
                                backgroundColor: Colors.white,
                                child:  ImageIcon(
                                  AssetImage("assets/icons/pump.png"),
                                  color:Color(0xFF8E2E7B) ,size: 18,
                                ),
                              ),
                              SizedBox(height: 3,),
                              Text(get_search_post_list[i]['fuel_type'],
                                style: TextStyle(fontSize: 8), )
                            ],),
                            Column(children: [
                              CircleAvatar(
                                radius: 15,
                                backgroundColor: Colors.white,
                                child: ImageIcon(
                                  AssetImage("assets/icons/droplet.png"),
                                  color:Color(0xFF8E2E7B) ,size: 18,
                                ),
                              ),
                              SizedBox(height: 3,),
                              Text(get_search_post_list[i]['mileage'],
                                style: TextStyle(fontSize: 8), )
                            ],),
                            Column(children: [
                              CircleAvatar(
                                radius: 15,
                                backgroundColor: Colors.white,
                                child: ImageIcon(
                                  AssetImage("assets/icons/road.png"),
                                  color:Color(0xFF8E2E7B) ,size: 18,
                                ),
                              ),
                              SizedBox(height: 3,),
                              Text(get_search_post_list[i]['distance'],
                                style: TextStyle(fontSize: 8), )
                            ],),
                            Column(children: [
                              CircleAvatar(
                                radius: 15,
                                backgroundColor: Colors.white,
                                child: ImageIcon(
                                  AssetImage("assets/icons/calendar.png"),
                                  color:Color(0xFF8E2E7B) ,size: 18,
                                ),
                              ),
                              SizedBox(height: 3,),
                              Text(get_search_post_list[i]['registration_date'],
                                style: TextStyle(fontSize: 8), )
                            ],),
                            Column(children: [
                              CircleAvatar(
                                radius: 15,
                                backgroundColor: Colors.white,
                                child: Icon(Icons.attach_money,
                                  color:  Color(0xFF8E2E7B),size: 18,
                                ),
                              ),
                              SizedBox(height: 3,),
                              Text("\$"+get_search_post_list[i]['price'],
                                style: TextStyle(fontSize: 8), )
                            ],),
                          ],),
                      ],
                    ),
                    SizedBox(height: 5,),
                    Row(
                      children: [
                        Icon(Icons.location_on_sharp,color: Color(0xFF8E2E7B) ,size: 18,),
                        Text(get_search_post_list[i]['location'],
                          style: TextStyle(fontSize: 12,
                            color:  Color(0xFF8E2E7B),),),
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
  Widget _AlertDialog(){
    return  AlertDialog(
      content: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left:220),
            child: CircleAvatar(
              radius: 11,backgroundColor:  Color(0xFF8E2E7B),
              child: CircleAvatar(
                backgroundColor: Colors.white,
                radius: 9,

                child: GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                  },
                    child: Icon(Icons.close,color: Color(0xFF8E2E7B),size: 15,)),
              ),
            ),
          ),

          Form(
            key: _formKey,
            child: Container(
              height: 250,
              width: MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Price",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                  Column(
                    children: <Widget>[
                      addRadioButton( 'High to Low',0,),
                      addRadioButton( 'Low to High',1),
                      addRadioButton( 'Recently Added',2),
                    ],
                  ),
                  RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      color: Color(0xFF8E2E7B),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20,right: 20),
                        child: Text("Apply",style: TextStyle(color: Colors.white,fontSize: 12),),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                        get_search_post_data();
                      }
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
