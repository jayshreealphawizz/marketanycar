import 'package:flutter/material.dart';
class Likes extends StatefulWidget {
  @override
  _LikesState createState() => _LikesState();
}

class _LikesState extends State<Likes> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Likes",style: TextStyle(fontSize: 16
        ,  color: Color(0xFF8E2E7B),),),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.only(top: 20,left: 15,right: 15),
          child: ListView(
            children: [
              SizedBox(height: 10,),
              _Row(),
              SizedBox(height: 10,),
              _Row(),
              SizedBox(height: 10,),
              _Row(),
              SizedBox(height: 10,),
              _Row(),
              SizedBox(height: 10,),
            ],
          ),
        ),
      ),
    );
  }
  Widget _Row(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(

          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CircleAvatar(
              backgroundImage: AssetImage("assets/placeholder_user.png",),
              radius: 30,
            ),
            SizedBox(width: 15,),
            Text("al.baseerat",
              style: TextStyle(fontWeight: FontWeight.bold,
                  fontSize: 14),),
            Text(" Liked This Post",
              style: TextStyle(fontSize: 13),),
          ],
        ),
        Icon(Icons.favorite,color: Colors.red,size: 20,)
      ],);
  }
}
