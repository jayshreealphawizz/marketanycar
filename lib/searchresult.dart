import 'package:flutter/material.dart';
class Searchresult extends StatefulWidget {
  @override
  _SearchresultState createState() => _SearchresultState();
}

class _SearchresultState extends State<Searchresult> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Search Results",
          style: TextStyle(fontSize: 16,color: Color(0xFF8E2E7B),),),
      ),
      body: Container(
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20,right: 20,
                  top:20 ),
              child: TextField(
                  cursorColor: Color(0xFF8E2E7B),
                  style: TextStyle(color:  Color(0xFF8E2E7B),),
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    suffixIcon: Icon(Icons.search,color:Color(0xFF8E2E7B)),
                    hintText: "Search your car",
                    hintStyle: TextStyle( color:  Color(0xFF8E2E7B),
                    fontSize: 13),
                  )
              ),
            ),
            Divider(),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Padding(
                padding: const EdgeInsets.only(left: 15,
                right: 15,top: 20,),
                child: ListView(
                  children: [
                    _Card(),
                    SizedBox(height: 30,),
                    _Card()
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
  Widget _Card(){
    return  Container(
      height: 250,
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.center,
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.asset("assets/car.jpg",
                  fit: BoxFit.cover,
                  height: 170,
                  width: 150,
                ),
              ),
              Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.asset("assets/car.jpg",
                      fit: BoxFit.cover,
                      height: 70,
                      width: 80,
                    ),
                  ),
                  SizedBox(width: 10,),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.asset("assets/car.jpg",
                      fit: BoxFit.cover,
                      height: 70,
                      width: 60,
                    ),
                  ),
                ],
              )
            ],
          ),
          SizedBox(width: 10,),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 10,),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("\$2050",
                        style: TextStyle(fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color:  Color(0xFF8E2E7B)
                        ),),
                      Container(
                        color: Colors.green,
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Text("GOOD PRICE",style: TextStyle(
                              color: Colors.white,
                              fontSize: 10
                          ),),
                        ),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("2016 Audi A1 2.0 TFSI S1",
                        style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                      Text("S1 Hatchback3D",
                        style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                    ],
                  ),
                  onTap: (){},
                ),
                Text("\$732 pm (PCP)",
                  style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Column(children: [
                      CircleAvatar(
                        radius: 15,
                        backgroundColor: Colors.white,
                        child: ImageIcon(
                          AssetImage("assets/icons/filter.png"),
                          color:Color(0xFF8E2E7B) ,size: 18,
                        ),
                      ),
                      SizedBox(height: 3,),
                      Text("Manual",
                        style: TextStyle(fontSize: 10), )
                    ],),
                    SizedBox(width: 5,),
                    Column(children: [
                      CircleAvatar(
                        radius: 15,
                        backgroundColor: Colors.white,
                        child:  ImageIcon(
                          AssetImage("assets/icons/pump.png"),
                          color:Color(0xFF8E2E7B) ,size: 18,
                        ),
                      ),
                      SizedBox(height: 3,),
                      Text("Petrol",
                        style: TextStyle(fontSize: 10), )
                    ],),
                    SizedBox(width: 5,),
                    Column(children: [
                      CircleAvatar(
                        radius: 15,
                        backgroundColor: Colors.white,
                        child: ImageIcon(
                          AssetImage("assets/icons/road.png"),
                          color:Color(0xFF8E2E7B) ,size: 18,
                        ),
                      ),
                      SizedBox(height: 3,),
                      Text("9.23 Miles",
                        style: TextStyle(fontSize: 10), )
                    ],),
                  ],),

                Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Text("FRANCHISE APPROVED",
                        style: TextStyle(fontSize: 14,
                            color:  Color(0xFF8E2E7B),
                            fontWeight: FontWeight.bold),),
                    ),
                    CircleAvatar(
                      radius: 18,
                      backgroundColor: Colors.white,
                      child: Icon(Icons.star,
                        color:Color(0xFF8E2E7B) ,size: 20,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
