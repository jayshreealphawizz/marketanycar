import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:http/http.dart' as http;
import 'package:Marketanycar/home.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:getwidget/getwidget.dart';
import 'package:url_launcher/url_launcher.dart';
import 'app_code/chatpage.dart';
import 'comments.dart';
import 'like_list.dart';
import 'log/allapi.dart';
import 'notifications.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';


class MyStructure {
  final int id;
  final String text;
  MyStructure({this.id, this.text});
}

class MyWidget extends StatelessWidget {
  final String widgetStructure; int intdexvalue; int totalimagecount;
  MyWidget(this.widgetStructure, this.intdexvalue, this.totalimagecount);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
        //     child: Text('${widgetStructure}')
            child:Image.network(
                  AllApi.user_image_url+widgetStructure,
                  fit: BoxFit.cover,
                )
        ),
        Positioned(
          right: 10, top: 10,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              color: Colors.white,
                // border: Border.all(color: Colors.blueAccent)
            ),
            width: 40, height: 25,
            child: Row (mainAxisAlignment: MainAxisAlignment.center, children: [Text((intdexvalue+1).toString(),style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),), Text('/', style: TextStyle(color: Colors.black),),Text(totalimagecount.toString(), style: TextStyle(color: Colors.black ,fontWeight: FontWeight.bold),)],),
          ),
        )
      ],
    );
  }
}


class Cardetails extends StatefulWidget {
  final String text_PostId;

  const Cardetails({Key key, this.text_PostId}) : super(key: key);

  @override
  _CardetailsState createState() => _CardetailsState();
}

class _CardetailsState extends State<Cardetails> {
  CameraPosition _kGooglePlex;
  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> markers = Set();

  // ++++++++++++++++++++++++++
  var fileimage_list;
  Map<int, String> postimge_strlist;
  String data;
  String userAuthorization,userID;
  var detaildata;

  post_details_data(String post_id,) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userAuthorization = prefs.getString(AllApi.userAuthorization);
    userID = prefs.getString(AllApi.userId);

    if(userID == null){
      userID = "";
    }

    Map data = {
      'post_id':post_id,
      'user_id' :userID
    };
    // Map<String, String> requestHeaders = {
    //   // 'Authorizations':userAuthorization
    //   'Authorizations':'UzdNeXNsSXlNbFd5QmdBPQ=='
    // };
    var jsondata = null;
    var response = await http.post(Uri.parse(AllApi.post_detailsApi),body: data);
    if(response.statusCode==200){
      jsondata = jsonDecode(response.body);
      int status =  jsondata['status'];
      String message =  jsondata['message'];
      detaildata =  jsondata['data'];
      fileimage_list = jsondata['data']['filename'];
      if(status==0){
        showToast(message);
      }
      else {
        setState(() {
         LatLng latLng =  LatLng(double.tryParse(detaildata['latitude']),double.tryParse(detaildata['longitude']));
          markers.add(Marker(markerId: MarkerId("a"),draggable:true,position: latLng,icon: BitmapDescriptor.defaultMarkerWithHue(
              BitmapDescriptor.hueRed),onDragEnd: (latLng){
          }));
        });
      }
    }
    else {
      throw Exception('Failed to load post');
    }
  }
//++++++++++++++

void postlike(String postId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userAuthorization = prefs.getString(AllApi.userAuthorization);

    if(userAuthorization==null){
      showToast('You can not like post without login');
    }
    else {
      Map data = {
        "post_id": postId,
      };
      Map<String, String> requestHeaders = {
        'Authorizations': userAuthorization
      };
      var jsondata = null;
      var response = await http.post(Uri.parse(AllApi.get_post_likeApi), body: data, headers: requestHeaders);
      if (response.statusCode == 200) {
        jsondata = jsonDecode(response.body);
        int status = jsondata['status'];
        String message = jsondata['message'];
        if (status == 0) {
          showToast(message);
        }
        else {
          setState(() {
            showToast(message);
            post_details_data(widget.text_PostId);
          });
        }
      }
      else {
        throw Exception('Failed to load post');
      }
    }
  }


  @override
  void initState() {
    setState(() {
      post_details_data(widget.text_PostId);
    });
    super.initState();
  }

 //+++++++++++++++++++++++++
String message;
  post_favorite_data(String post_id,) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userAuthorization = prefs.getString(AllApi.userAuthorization);
    userID = prefs.getString(AllApi.userId);

    if (userAuthorization == null) {
      showToast('You can not favourite post without login');
    }
    else {
      Map data = {
        'post_id': post_id,
        // 'user_id':userID,
      };
      Map<String, String> requestHeaders = {
        'Authorizations': userAuthorization
      };
      var jsondata = null;
      var response = await http.post(
          Uri.parse(AllApi.post_favoriteApi), body: data,
          headers: requestHeaders);
      if (response.statusCode == 200) {
        jsondata = jsonDecode(response.body);
        int status = jsondata['status'];
        message = jsondata['message'];
        if (status == 0) {
          showToast(message);
        }
        else {
          setState(() {
            showToast(message);
            post_details_data(widget.text_PostId);
          });
        }
      }
      else {
        throw Exception('Failed to load post');
      }
    }
  }


  Future<bool> _onBackPressed() {
    Navigator.push(context,
        MaterialPageRoute(builder:
        (context) => Home(),));
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      final split = fileimage_list.split(',');
      postimge_strlist = {
        for (int i = 0; i < split.length; i++)
          i: split[i]
      };
    });
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        appBar: _AppBar(),
        body: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: ListView(
                children: [
                  Container(
                    height: 250,
                    child: _Swiper(postimge_strlist)
                  ),
                  _Row(),
                  Padding(
                    padding: const EdgeInsets.only(top: 10,left:20,right: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _row(),
                        SizedBox(height: 5,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              children: [
                                Text("\$"+detaildata['price'],
                                  style: TextStyle(fontSize: 16,
                                color: Color(0xFF8E2E7B),
                                fontWeight: FontWeight.bold),),
                                SizedBox(width: 10,),
                                Text("\$"+detaildata['offer_price']+' Below Market',
                                  style: TextStyle(fontSize: 12,),),
                              ],
                            ),
                            Image.asset("assets/Group 10694.png",
                        height: 50,),
                          ],
                        )
                      ],
                    ),
                  ),
                  Divider( thickness: 1,),
                  Padding(
                    padding: const EdgeInsets.only(left: 20,right: 20),
                    child: Text("Vehicle Details",
                      style: TextStyle(fontSize: 14,
                          fontWeight: FontWeight.bold,
                          ),),
                  ),
                  SizedBox(height: 15,),
                  _Column(),
                  SizedBox(height: 15,),
                  Container(
                    height: 300,
                    width: MediaQuery.of(context).size.width,
                    color: Colors.black12,
                   child:
                   GoogleMap(
                     markers:markers,
                     mapType: MapType.normal,
                     initialCameraPosition: CameraPosition(target: LatLng(double.tryParse(detaildata['latitude']),double.tryParse(detaildata['longitude'])), zoom: 10,),
                     onMapCreated: (GoogleMapController controller) {
                       _controller.complete(controller);
                     },
                   ),
                  ),
                  SizedBox(height: 10,),
                  Padding(
                    padding: const EdgeInsets.only(left: 20,right: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Description",
                          style: TextStyle(fontSize: 14,
                        fontWeight: FontWeight.bold,
                           color:  Color(0xFF8E2E7B) ),),
                        SizedBox(height: 15,),
                        Text(detaildata['description']
                          ,style: TextStyle(fontSize: 12,) ),
                        SizedBox(height: 15,),
                        Text("All Stats",
                          style: TextStyle(fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color:  Color(0xFF8E2E7B) ),),
                        Padding(
                          padding: const EdgeInsets.only(left: 20,right: 20),
                          child: DataTable(columnSpacing: 80,showBottomBorder: true,
                            columns: [
                              DataColumn(label: Text('',)),
                              DataColumn(label: Text('',)),
                              DataColumn(label: Text('',)),
                            ],
                            rows: [
                              DataRow(cells: [
                                DataCell(Text('Dealer,s Price' ,style: TextStyle(fontSize: 12,))),
                                DataCell(Text(':' ,style: TextStyle(fontSize: 12,))),
                                DataCell(Text('\$16,456' ,style: TextStyle(fontSize: 12,))),
                              ]),
                              DataRow(cells: [
                                DataCell(Text('Location' ,style: TextStyle(fontSize: 12,))),
                                DataCell(Text(':' ,style: TextStyle(fontSize: 12,))),
                                DataCell(Text(detaildata['location'] ,style: TextStyle(fontSize: 12,))),
                              ]),
                              DataRow(cells: [
                                DataCell(Text('Mileage' ,style: TextStyle(fontSize: 12,))),
                                DataCell(Text(':' ,style: TextStyle(fontSize: 12,))),
                                DataCell(Text(detaildata['mileage'] ,style: TextStyle(fontSize: 12,))),
                              ]),
                              DataRow(cells: [
                                DataCell(Text('Gearbox' ,style: TextStyle(fontSize: 12,))),
                                DataCell(Text(':' ,style: TextStyle(fontSize: 12,))),
                                DataCell(Text(detaildata['gear_box'] ,style: TextStyle(fontSize: 12,))),
                              ]),
                            ],
                          ),
                        ),
                        SizedBox(height: 15,),
                        Text("More Information",
                          style: TextStyle(fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color:  Color(0xFF8E2E7B) ),),
                    GFAccordion(
                      contentBorderRadius:BorderRadius.all(Radius.circular(12.0)),
                      expandedIcon:Icon(Icons.arrow_drop_up,color: Colors.black,),
                      collapsedIcon: Icon(Icons.arrow_drop_down,color: Colors.black,),
                      collapsedTitleBackgroundColor: Colors.white,
                      contentBackgroundColor: Colors.black12,
                      expandedTitleBackgroundColor: Colors.white,
                      titleChild:Row(
                        children: [
                          Icon(Icons.info_outline,color:Color(0xFF8E2E7B) ,
                            size: 20,),
                          Text('\t\tBasic info')
                        ],
                      ),
                      //title: '\t\tBasic info',
                      textStyle: TextStyle(color: Colors.black),
                      content: detaildata['basic_info'],
                    ),
                        Divider(thickness: 1,),
                        GFAccordion(
                          contentBorderRadius:BorderRadius.all(Radius.circular(12.0)),
                          expandedIcon:Icon(Icons.arrow_drop_up,color: Colors.black,),
                          collapsedIcon: Icon(Icons.arrow_drop_down,color: Colors.black,),
                          collapsedTitleBackgroundColor: Colors.white,
                          contentBackgroundColor: Colors.black12,
                          expandedTitleBackgroundColor: Colors.white,
                          titleChild:Row(
                            children: [
                              Icon(Icons.info_outline,color:Color(0xFF8E2E7B) ,
                                size: 20,),
                              Text('\t\tBasic info')
                            ],
                          ),
                          textStyle: TextStyle(color: Colors.black),
                          content: detaildata['technical_info'],
                        ),
                        Divider(thickness: 1,),
                        GFAccordion(
                          contentBorderRadius:BorderRadius.all(Radius.circular(12.0)),
                          expandedIcon:Icon(Icons.arrow_drop_up,color: Colors.black,),
                          collapsedIcon: Icon(Icons.arrow_drop_down,color: Colors.black,),
                          collapsedTitleBackgroundColor: Colors.white,
                          contentBackgroundColor: Colors.black12,
                          expandedTitleBackgroundColor: Colors.white,
                          titleChild:Row(
                            children: [
                              ImageIcon(
                                AssetImage("assets/icons/Group 10650.png"),
                                color:Color(0xFF8E2E7B),size: 20,),
                              Text('\t\tBasic info')
                            ],
                          ),
                          textStyle: TextStyle(color: Colors.black),
                          content: detaildata['specification'],
                        ),
                        Divider(thickness: 1,),
                        SizedBox(height: 15,),
                        Text("Sell by",style: TextStyle(fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color:  Color(0xFF8E2E7B) ),),
                        SizedBox(height: 15,),
                        Container(
                          height: 200,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.black12,
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Column(

                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(detaildata['username'],
                                  style: TextStyle(fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    color:  Color(0xFF8E2E7B) ),),
                                Text(detaildata['email'],
                                  style: TextStyle(fontSize: 12,
                                    fontWeight: FontWeight.normal,),),
                                Text(detaildata['phone_number'],
                                  style: TextStyle(fontSize: 13,
                                    fontWeight: FontWeight.bold,),),
                                CircleAvatar(
                                  backgroundImage: NetworkImage(AllApi.user_image_url+detaildata['userimage']),
                                  radius: 40,
                                ),
                              //  Image.asset("assets/Image 1.png",height: 100,width: 200,),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 80,),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          detaildata['user_id']==userID?Container(width: 0.0, height: 0.0):Positioned(
              bottom: 0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      add_user_to_chat(detaildata['user_id'], userID);
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width*0.5,
                      height: 60,
                      color: Colors.grey,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ImageIcon(
                            AssetImage("assets/icons/chatboxes.png"),
                            color:Colors.white,size: 20,
                          ),
                          Text("\t\tChat With Seller",
                            style: TextStyle(color: Colors.white,fontSize: 12),)
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => launch("tel:"+detaildata['phone_number']),
                    child: Container(
                      width: MediaQuery.of(context).size.width*0.5,
                      height: 60,
                      color: Color(0xFF8E2E7B),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.call,color: Colors.white,size: 20,),
                          Text("\t\tCall Seller",
                          style: TextStyle(color: Colors.white,fontSize: 12),)
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
  Widget _Swiper(values){
    return Swiper(
      // pagination: SwiperPagination(
      //   // builder: new DotSwiperPaginationBuilder(
      //   //     color: Colors.grey, activeColor: Color(0xff38547C)),
      //   //   builder: SwiperPagination.fraction,
      //   //   alignment: Alignment.topRight
      // ),
      control: SwiperControl(
          iconNext: Icons.east,
          iconPrevious:Icons.west,size: 22),
      itemCount: values.length,
      itemBuilder: (BuildContext context, int index) {
        return MyWidget(values[index],index, values.length);
      },

      // itemBuilder: (context, index) {
      //   return Image.network(
      //     AllApi.user_image_url+values[index],
      //     fit: BoxFit.cover,
      //   );
      // },
    );
  }
  Widget _AppBar(){
    return AppBar(
      title: Text("Car Details",
        style: TextStyle(fontSize: 16,
        color:  Color(0xFF8E2E7B)),),
      actions: [
        GestureDetector(
          child: Icon(Icons.notifications_none,),
          onTap: () =>Navigator.push(context,
              MaterialPageRoute(builder:
                  (context) => Notifications()
              )
          ),
        ),
        SizedBox(width: 10,),
      ],
    );
  }
  Widget _Row(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Material(
          borderRadius: BorderRadius.all(Radius.circular(50)),
          elevation: 2,
          color: Colors.white,
          child: CircleAvatar(
            radius: 18,
            backgroundColor: Colors.white,
            child: IconButton(icon: Icon(Icons.favorite,
                    color: detaildata['is_like']=='0'?Colors.blueGrey:Colors.red,size: 20,),
                    onPressed: (){
                    setState(() {
                    postlike(detaildata['id']);
                   });
                 }),
            ),
        ),
        SizedBox(width: 5,),
       Material(
         borderRadius: BorderRadius.all(Radius.circular(50)),
         elevation: 2,
         color: Colors.white,
         child: CircleAvatar(
            radius: 18,
            backgroundColor: Colors.white,
            child: IconButton(icon: Icon(Icons.star,
                    color: detaildata['is_favorite']=='0'?Colors.blueGrey: Color(0xFF8E2E7B),size: 20,),
                    onPressed: (){
                   setState(() {
                  post_favorite_data(detaildata['id']);
                 // post_favorite_data(recent_view_whole_list[i]['is_favorite']);
                });
                 }),
            ),
       ),
        SizedBox(width: 5,),
        Material(
          borderRadius: BorderRadius.all(Radius.circular(50)),
          elevation: 2,
          color: Colors.white,
          child: CircleAvatar(
            radius: 18,
            backgroundColor: Colors.white,
            child: Icon(Icons.share,
              color: Colors.black,size: 20,),
          ),
        ),
        SizedBox(width: 25,),
      ],
    );
  }
  Widget _row(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 3),
          child: Text(detaildata['name'],
            style: TextStyle(fontSize: 14,
                fontWeight: FontWeight.bold),),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            GestureDetector(
              child: Row(children: [
                Icon(Icons.favorite_sharp,
                  color: Colors.red,size: 15,),
                Text(detaildata['like_count']+" like",
                  style: TextStyle(fontSize: 9,fontWeight: FontWeight.bold),)
              ],),
              onTap: () =>Navigator.push(context,
                    MaterialPageRoute(builder:
                        (context) => LikesList(text_PostId: detaildata['id'],)
                    )
                ),
            ),
            SizedBox(width: 10,),
            GestureDetector(
              child: Row(children: [
                Icon(Icons.send,
                  color: Colors.yellow,size: 15,),
                Text(detaildata['comment_count']+" Comments",
                  style: TextStyle(fontSize: 9,fontWeight: FontWeight.bold),)
              ],),
              onTap:(){
                          if (userAuthorization == null) {
                            showToast('You can not see comment on post without login');
                          } else {
                            Navigator.of(context).push(
                              MaterialPageRoute(builder: (BuildContext context) => Comments(text_PostId: detaildata['id'])),);
                          }
                        },
            ),
            SizedBox(width: 10,),
          ],
        ),
      ],
    );
  }
  Widget _Column(){
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 10,right: 10),
          child: Wrap(alignment: WrapAlignment.start,
            runSpacing: 10,
            spacing: 35,
            children: [
              Column(children: [
                CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.white,
                  child: ImageIcon(
                    AssetImage("assets/icons/filter.png"),
                    color:Color(0xFF8E2E7B) ,size: 20,
                  ),
                ),
                SizedBox(height: 3,),
                Text("Manual",
                  style: TextStyle(fontSize: 10),)
              ],),
              Column(children: [
                CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.white,
                  child:  ImageIcon(
                    AssetImage("assets/icons/pump.png"),
                    color:Color(0xFF8E2E7B) ,size: 20,
                  ),
                ),
                SizedBox(height: 3,),
                Text(detaildata['fuel_type'],
                  style: TextStyle(fontSize: 10), )
              ],),

              Column(children: [
                CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.white,
                  child: ImageIcon(
                    AssetImage("assets/icons/droplet.png"),
                    color:Color(0xFF8E2E7B) ,size: 20,
                  ),
                ),
                SizedBox(height: 3,),
                Text(detaildata['mileage']+" L",
                  style: TextStyle(fontSize: 10), )
              ],),

              Column(children: [
                CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.white,
                  child: ImageIcon(
                    AssetImage("assets/icons/road.png"),
                    color:Color(0xFF8E2E7B) ,size: 20,
                  ),
                ),
                SizedBox(height: 3,),
                Text(detaildata['distance']+" Miles",
                  style: TextStyle(fontSize: 10), )
              ],),

              Column(children: [
                CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.white,
                  child: ImageIcon(
                    AssetImage("assets/icons/calendar.png"),
                    color:Color(0xFF8E2E7B) ,size: 20,
                  ),
                ),
                SizedBox(height: 3,),
                Text(detaildata['registration_date'],
                  style: TextStyle(fontSize: 10), )
              ],),

              Column(children: [
                CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.white,
                  child: Icon(Icons.attach_money,
                    color:  Color(0xFF8E2E7B),size: 20,),
                ),
                SizedBox(height: 3,),
                Text("\$",
                  style: TextStyle(fontSize: 10), )
              ],),

            ],),
        ),
      ],
    );
  }
  Widget _container (){
    return Stack(
      children: [
        Container(
          // color: Colors.red,
          width: 160,
          height: 200,
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20))
              ),
              //  color: Colors.white,
              height: 200,
              width: 160,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.vertical(top: Radius.circular(10),),
                    child: Image.asset("assets/car.jpg",
                      fit: BoxFit.cover,
                      height: 110,
                      width: 160,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10,top: 5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("KL Quonto",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                        Text("\$2000",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                        SizedBox(height: 10,),
                        Text("Closs:40Z Albite 348647",style: TextStyle(fontSize: 10,),),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Positioned(
          bottom: 62,right: 10,
          child: CircleAvatar(
            radius: 15,
            backgroundColor: Colors.white,
            child: Icon(Icons.star,color: Color(0xFF8E2E7B),size: 18,),
          ),
        ),
      ],
    );
  }

  void add_user_to_chat(String receiverID,String senderID) async {
    if(senderID.isEmpty){
      showToast('Before chat with selller, Please login first');
    }
    else {
      Map data = {
        "sender_id": senderID,
        'receiver_id': receiverID,
        'chat_messages_text': 'HII',
      };

      var jsondata = null;
      var response = await http.post(
          Uri.parse(AllApi.chat_add_user_Api), body: data);
      if (response.statusCode == 200) {
        jsondata = jsonDecode(response.body);
        int status = jsondata['status'];
        String message = jsondata['message'];

        if (status == 0) {
          showToast(message);
        }
        else {
          setState(() {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) =>
                    ChatPage(docs: (detaildata['user_id']),
                        receiver_name: detaildata['username'])));
          });
        }
      }
      else {
        throw Exception('Failed to load post');
      }
    }
  }
}
