import 'dart:convert';

import 'package:flutter/material.dart';

// import 'package:Marketanycar/sell_any_car.dart';
import 'package:http/http.dart' as http;
import 'package:Marketanycar/comman/_bottomAppBar.dart';
import 'package:Marketanycar/sell_any_car.dart';
import 'deshbord.dart';
import 'log/allapi.dart';

class Vehicleone extends StatefulWidget {
  @override
  _VehicleoneState createState() => _VehicleoneState();
}

class _VehicleoneState extends State<Vehicleone> {
  final _formKey = GlobalKey<FormState>();
  String vehicle_no;
  String _selectedMileage;
  List mileage_list;
  List<String> mileage_list_strlist;
  Future<String> post;
  TextEditingController vehicle_noController = new TextEditingController();

  void enter_vehicle_no_verify(String vehicle_no) async {
    Map data = {
      "vehicle_no": vehicle_no,
    };
    var jsondata = null;
    var response =
        await http.post(Uri.parse(AllApi.enter_vehicle_no_verifyApi), body: data);
    if (response.statusCode == 200) {
      jsondata = jsonDecode(response.body);
      int status = jsondata['status'];
      String message = jsondata['message'];
      if (status == 0) {
        showToast(message);
      } else {
        showToast(message);
        setState(() {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => Sellanycar(
                      text_model: jsondata['data']['model'],
                      text_color: jsondata['data']['colour'],
                      text_engineno: jsondata['data']['engine_number'],
                      text_enginecapacity: jsondata['data']['engine_capacity'],
                      text_make: jsondata['data']['make'],
                      text_weight: jsondata['data']['weight'],
                      text_vehicleno: vehicle_no, text_mileage :_selectedMileage)));
        });
      }
    } else {
      throw Exception('Failed to load post');
    }
  }

  Future<List<String>> getMileageData() async {
    final response = await http.get(Uri.parse(AllApi.get_mileageApi));
    final jsonResponse = jsonDecode(response.body);
    int status = jsonResponse['status'];
    var data = jsonResponse['data'];
    if (status == 0) {
    } else {
      mileage_list = data;
      mileage_list_strlist = [
        for (int i = 0; i < mileage_list.length; i++)
          mileage_list[i]['name'].toString()
      ];
      (context as Element).reassemble();

      // _selectedMileage = mileage_list_strlist[0];
    }
  }

  @override
  void initState() {
    setState(() {
      getMileageData();
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return mileage_list_strlist.length == 0
        ? Scaffold()
        : Scaffold(
            appBar: AppBar(
              title: Text(
                "Vehicle Your Own",
                style: TextStyle(fontSize: 16, color: Color(0xFF8E2E7B)),
              ),
            ),
            body: Form(
              key: _formKey,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: ListView(
                    children: [
                      SizedBox(
                        height: 40,
                      ),
                      Text(
                        "Make 1,254 more by selling",
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "your car with \"Sell Any Car\" ",
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        "Registration",
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Card(
                        child: Padding(
                          padding:
                              const EdgeInsets.only(top: 5, bottom: 5, left: 5),
                          child: TextFormField(
                              key: Key("vehicle_no"),
                              controller: vehicle_noController,
                              style: TextStyle(color: Colors.black),
                              cursorColor: Colors.black,
                              decoration: InputDecoration(
                                  fillColor: Colors.white,
                                  filled: true,
                                  hintText: "E.g ABCD 123",
                                  hintStyle: TextStyle(
                                      color: Colors.black54, fontSize: 13),
                                  border: InputBorder.none)),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        "Current mileage",
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Card(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 5, bottom: 5, left: 20, right: 20),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: DropdownButton(
                                  hint: Text(
                                    "Mileage",
                                    style: TextStyle(fontSize: 13),
                                  ),
                                  isExpanded: true,
                                  underline: Container(),
                                  value: _selectedMileage,
                                  onChanged: (newValue) {
                                    setState(() {
                                      _selectedMileage = newValue;
                                    });
                                  },
                                  items: mileage_list_strlist
                                      .map((cityTitle) => DropdownMenuItem(
                                          value: cityTitle,
                                          child: Text(
                                            "$cityTitle",
                                            style: TextStyle(fontSize: 13),
                                          )))
                                      .toList(),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          RaisedButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            child: Text(
                              "Create Your Advert",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 13),
                            ),
                            color: Colors.black54,
                            onPressed: () async {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  });
                              await loginAction();
                              Navigator.pop(context);
                              if (vehicle_noController.text.isEmpty) {
                                showToast('Please enter registration number');
                              } else if (_selectedMileage == null) {
                                showToast('Choose current mileage');
                              } else {
                                enter_vehicle_no_verify(vehicle_noController.text);
                              }
                            },
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              child: Text(
                                "Existing Adverts",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 13),
                              ),
                              color: Color(0xFF8E2E7B),
                              onPressed: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Deshbord()))),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            bottomNavigationBar: Bbar(),
          );
  }
}
