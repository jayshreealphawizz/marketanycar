import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'carlist.dart';
import 'log/allapi.dart';

class Filter extends StatefulWidget {
  @override
  _FilterState createState() => _FilterState();
}

class _FilterState extends State<Filter> {
  bool loginAction = false;
  final _formKey = GlobalKey<FormState>();


  TextEditingController postal_codeController = new TextEditingController();
  TextEditingController distanceController = new TextEditingController();

//+++++++++++++++++++++++
  String _selectedMileage;
  String _selectedMileage_id;
  List mileage_list;
  List<String> mileage_list_strlist;

  Future<List<String>> getMileageData() async {
    setState(() => loginAction = true);
    try {
      final response = await http.get(Uri.parse(AllApi.get_mileageApi));
      final jsonResponse = jsonDecode(response.body);
      int status = jsonResponse['status'];
      var data = jsonResponse['data'];

      if (status == 0) {
      }
      else {
        mileage_list = data;
        mileage_list_strlist = [for(int i = 0; i < mileage_list.length; i++)
          mileage_list[i]['name'].toString()+"--"+mileage_list[i]['id'].toString()
        ];

       // _selectedMileage = mileage_list_strlist[0];
      }
    }
    catch (ex) {
      setState(() => mileage_list = null);
    } finally {
      setState(() => loginAction = false);
    }
  }

//+++++++++++++++++++++++

  String _selectedbody_type;
  String _selectedbody_type_id;
  List body_type_list;
  List<String> body_type_strlist;

  Future<List<String>> get_body_typeData() async {
    final response = await http.get(Uri.parse(AllApi.get_body_typeApi));
    final jsonResponse = jsonDecode(response.body);
    int status =  jsonResponse['status'];
    var data =  jsonResponse['data'];

    if(status==0){

    }
    else{
      body_type_list= data;

      body_type_strlist = [for(int i = 0; i < body_type_list.length; i++)
        body_type_list[i]['name'].toString()+"--"+body_type_list[i]['id'].toString()];

     // _selectedbody_type = body_type_strlist[0];
    }
  }

  //+++++++++++++++++++++++++

  String _selectedget_year;
  List get_year_list;
  List<String> get_year_strlist;

  Future<List<String>> get_year_Data() async {
    final response = await http.get(Uri.parse(AllApi.get_yearApi));
    final jsonResponse = jsonDecode(response.body);
    int status =  jsonResponse['status'];
    var data =  jsonResponse['data'];

    if(status==0){

    }
    else{
      get_year_list= data;
      get_year_strlist = [for(int i = 0; i < get_year_list.length; i++)
        get_year_list[i]['year'].toString()];

     // _selectedget_year = get_year_strlist[0];
    }
  }

  //++++++++++++++++++

  String _selectedget_min_price;
  String _selectedget_min_price_id;
  List get_min_price_list;
  List<String> get_min_price_strlist;

  Future<List<String>> get_min_price_Data() async {
    final response = await http.get(Uri.parse(AllApi.get_min_priceApi));
    final jsonResponse = jsonDecode(response.body);

    int status =  jsonResponse['status'];
    var data =  jsonResponse['data'];

    if(status==0){

    }
    else{
      get_min_price_list= data;

      get_min_price_strlist = [for(int i = 0; i <get_min_price_list.length; i++)
        get_min_price_list[i]['min_price'].toString()+" to "+get_min_price_list[i]['max_price'].toString()+"--"+get_min_price_list[i]['id'].toString()];

     // _selectedget_min_price = get_min_price_strlist[0];
    }
  }
//===================================

  // String _selectedget_min_price;
  // List get_min_price_list;
  // List<String> get_min_price_strlist;
  String _selectedgetcategory;
  String _selectedgetcategory_id;
  List getcategory_list;
  List<String> getcategory_strlist;

  Future<List<String>> getcategoryDATA() async {
    final response = await http.get(Uri.parse(AllApi.get_categoriesApi));
    final jsonResponse = jsonDecode(response.body);
    int status =  jsonResponse['status'];
    var data =  jsonResponse['data'];
    if(status==0){
    }
    else{
      getcategory_list= data;
      getcategory_strlist = [for(int i = 0; i <getcategory_list.length; i++)
       // getcategory_list[i]['category_name'].toString()];
    getcategory_list[i]['category_name'].toString()+"--"+getcategory_list[i]['id'].toString(),];

      //_selectedget_min_price = get_min_price_strlist[0];
    }
  }
 //=================================
  //++++++++++++++++++//
  String dropDownValue;
  List<String> buying_List = [
    'Cash',
    'Finance',
  ];
  setFilters() {
    setState(() {
      dropDownValue = buying_List[0];
    });
  }
//+++++++++++++++++++++++++++++
  @override
  void initState() {
      getMileageData();
      get_body_typeData();
      get_year_Data();
      get_min_price_Data();
      getcategoryDATA();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
      return mileage_list_strlist==null? Scaffold():Scaffold(
        appBar: AppBar(
          title: Text("Filters",
              style: TextStyle(fontSize: 16,
                  color: Color(0xFF8E2E7B))),
        ),
        body: Form(
          key: _formKey,
          child: Container(
            width: MediaQuery
                .of(context)
                .size
                .width,
            height: MediaQuery
                .of(context)
                .size
                .height,
            child: Padding(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: ListView(
                children: [
                  SizedBox(height: 20,),
                  Text("PostCode",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
                  SizedBox(height: 5,),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 3, bottom: 3, left: 5),
                      child: TextField(
                        maxLength: 6,
                        controller: postal_codeController,
                          style: TextStyle(fontSize: 13, color: Colors.black),
                          cursorColor: Colors.black,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              fillColor: Colors.white,
                              filled: true,
                              counterText: "",
                              hintText: "456362",
                              hintStyle: TextStyle(
                                  color: Colors.black54, fontSize: 13),
                              border: InputBorder.none
                          )
                      ),
                    ),
                  ),
                  SizedBox(height: 15,),
                  Text("Distance",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
                  SizedBox(height: 5,),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 3, bottom: 3, left: 5),
                      child: TextField(
                        controller: distanceController,
                          style: TextStyle( fontSize: 13,color: Colors.black),
                          cursorColor: Colors.black,
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                              fillColor: Colors.white,
                              filled: true,
                              hintText: "45Km",
                              hintStyle: TextStyle(
                                  color: Colors.black54, fontSize: 13),
                              border: InputBorder.none
                          )
                      ),
                    ),
                  ),
                  SizedBox(height: 15,),
                  Text("Make",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
                  SizedBox(height: 5,),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 5, bottom: 5, left: 20, right: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: DropdownButton(
                              hint: Text("category",style: TextStyle(fontSize: 13),),
                              isExpanded: true,
                              underline: Container(),
                              value: _selectedgetcategory,
                              onChanged: (newValue) {
                                setState(() {
                                  _selectedgetcategory = newValue;
                                  _selectedgetcategory_id  =  _selectedgetcategory.split('--')[1];
                                });
                              },
                              items: getcategory_strlist.map((cityTitle) => DropdownMenuItem(
                                  value: cityTitle, child: Text("$cityTitle".split('--')[0],style: TextStyle(fontSize: 13),)))
                                  .toList(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 15,),
                  Text("Buying with",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
                  SizedBox(height: 5,),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 5, bottom: 5, left: 20, right: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: DropdownButton(
                              isExpanded: true,
                              hint: Text("Buying with",style: TextStyle(fontSize: 13),),
                              underline: Container(),
                              value: dropDownValue,
                              onChanged: (String Value) {
                                setState(() {
                                  dropDownValue = Value;
                                });
                              },
                              items: buying_List
                                  .map((cityTitle) => DropdownMenuItem(
                                  value: cityTitle, child: Text("$cityTitle",style: TextStyle(fontSize: 13),)))
                                  .toList(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  SizedBox(height: 15,),
                  Text("Min Price",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
                  SizedBox(height: 5,),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 5, bottom: 5, left: 20, right: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: DropdownButton(
                              hint: Text("Min Price",style: TextStyle(fontSize: 13),),
                              isExpanded: true,
                              underline: Container(),
                              value: _selectedget_min_price,
                              onChanged: (newValue) {
                                setState(() {
                                  _selectedget_min_price = newValue;
                                  _selectedget_min_price_id  =  _selectedget_min_price.split('--')[1];
                                });
                              },
                              items: get_min_price_strlist.map((cityTitle) => DropdownMenuItem(
                                  value: cityTitle, child: Text("$cityTitle".split('--')[0],style: TextStyle(fontSize: 13),)))
                                  .toList(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 15,),
                  Text("Body type",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
                  SizedBox(height: 5,),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 5, bottom: 5, left: 20, right: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: DropdownButton(
                              hint: Text("Body type",style: TextStyle(fontSize: 13),),
                              isExpanded: true,
                              underline: Container(),
                              value: _selectedbody_type,
                              onChanged: (newValue) {
                                setState(() {
                                  _selectedbody_type = newValue;
                                  _selectedbody_type_id  =  _selectedbody_type.split('--')[1];
                                });
                              },
                              items: body_type_strlist.map((cityTitle) => DropdownMenuItem(
                                  value: cityTitle, child: Text("$cityTitle".split('--')[0],style: TextStyle(fontSize: 13),)))
                                  .toList(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 15,),
                  Text("Mileage",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
                  SizedBox(height: 5,),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 5, bottom: 5, left: 20, right: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: DropdownButton(
                              hint: Text("Mileage",style: TextStyle(fontSize: 13),),
                              isExpanded: true,
                              underline: Container(),
                              value: _selectedMileage,
                              onChanged: (newValue) {
                                setState(() {
                                  _selectedMileage = newValue;
                                  _selectedMileage_id  =  _selectedMileage.split('--')[1];
                                });
                              },
                              items: mileage_list_strlist.map((cityTitle) => DropdownMenuItem(
                                  value: cityTitle, child: Text("$cityTitle".split('--')[0],style: TextStyle(fontSize: 13),)))
                                  .toList(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 15,),
                  Text("Min year",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
                  SizedBox(height: 6,),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 5, bottom: 5, left: 20, right: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: DropdownButton(
                              isExpanded: true,
                              hint: Text("Min year",style: TextStyle(fontSize: 13),),
                              underline: Container(),
                              value: _selectedget_year,
                              onChanged: (newValue) {
                                setState(() {
                                  _selectedget_year = newValue;
                                });
                              },
                              items: get_year_strlist.map((cityTitle) => DropdownMenuItem(
                                  value: cityTitle, child: Text("$cityTitle",style: TextStyle(fontSize: 13),)))
                                  .toList(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 30, top: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        RaisedButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 12, bottom: 12, left: 40, right: 40),
                              child: Text("Apply", style: TextStyle(
                                  color: Colors.white, fontSize: 14),),
                            ),
                            color: Color(0xFF8E2E7B),
                            onPressed: () async {
                              Navigator.of(context).push(
                                MaterialPageRoute(builder:
                                    (BuildContext context) =>
                                    Carlist(
                                      text_filter_postal_code :postal_codeController.text,
                                      text_filter_distance : distanceController.text,
                                      text_filter_Postprice:_selectedget_min_price_id,
                                      text_filter_postcategory :_selectedgetcategory_id ,
                                      text_filter_body_type  :_selectedbody_type_id,
                                      text_filter_Mileage  :_selectedMileage_id,
                                      text_filter_get_year :_selectedget_year,
                                      text_filter_buying :dropDownValue,
                                        text_from:'filter',
                                    )),);
                              },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
  }
}
