import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'cardetails.dart';
import 'carlist.dart';
import 'comman/_bottomAppBar.dart';
import 'comman/_drawer.dart';
import 'filter.dart';
import 'log/allapi.dart';
import 'notifications.dart';

class Deshbord extends StatefulWidget {
  @override
  _DeshbordState createState() => _DeshbordState();
}

class _DeshbordState extends State<Deshbord> {
  String Tabbar_sub_categoryId;
  String Tabbar_categoryId_select = '1';

  TabController _controller;

  bool loginAction = false;
  final _formKey = GlobalKey<FormState>();

  List category_list ;
  Future<List<String>> getcategoryDATA() async {
    final response = await http.get(Uri.parse(AllApi.get_categoriesApi));
    final jsonResponse = jsonDecode(response.body);
    int status =  jsonResponse['status'];
    String message =  jsonResponse['message'];
    var data =  jsonResponse['data'];
    if(status==0){
      showToast(message);
    }
    else{
      setState(() {
        category_list= data;
      });
    }
  }
  //+++++++++++++++++++++++++

String userAuthorization , userID;
  List recent_upload_whole_list ;
  List recent_view_whole_list ;

  get_dashboard_data(String category_id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userAuthorization = prefs.getString(AllApi.userAuthorization);
    userID = prefs.getString(AllApi.userId);

    if(userID==null){userID = '';}
    Map data = {
      'category_id':category_id,
      'user_id':userID,
    };
    var jsondata = null;
    var response = await http.post(Uri.parse(AllApi.get_dashboard_dataApi),body: data);

    if(response.statusCode==200){
      jsondata = jsonDecode(response.body);
      int status =  jsondata['status'];
      String message =  jsondata['message'];
      if(status==0){
        showToast(message);
      }
      else {
        setState(() {
          var data_list =  jsondata['data']['recent_upload'];
          var data_view =  jsondata['data']['recent_view'];
          recent_upload_whole_list = data_list;
          recent_view_whole_list=data_view;
        });
      }
    }
    else {
      throw Exception('Failed to load post');
    }
  }

  //+++++++++++++++++++++++++
String message;
  post_favorite_data(String post_id,) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userAuthorization = prefs.getString(AllApi.userAuthorization);

    if(userAuthorization==null){
      showToast('You can not like post without login');
    }else{
    Map data = {
      'post_id':post_id,
      //'user_id':userID,
    };
    Map<String, String> requestHeaders = {
      'Authorizations': userAuthorization
    };
    var jsondata = null;
    var response = await http.post(Uri.parse(AllApi.post_favoriteApi),body: data,
        headers:requestHeaders);
    if(response.statusCode==200){
      jsondata = jsonDecode(response.body);
      int status =  jsondata['status'];
      message =  jsondata['message'];
      if(status==0){
        showToast(message);
      }
      else {
        setState(() {
          get_dashboard_data(Tabbar_categoryId_select);
          showToast(message);
        });
      }
    }
    else {
      throw Exception('Failed to load post');
    }
  }}
  //+++++++++++++++++++++++++


  String _selectedget_subcategories;
  String _selectedget_subcategories_id;
  List get_subcategories_list;
  List<String> get_subcategories_list_strlist;
  List<String> get_subcategories_list_strlist_Id;

  get_subcategories_Data(String category_id,) async {
    Map data = {
        'category_id':category_id,
      };
    var jsondata = null;
    var response = await http.post(Uri.parse(AllApi.get_subcategoriesApi),body: data,);
    if(response.statusCode==200){
     final jsondata = jsonDecode(response.body);
      int status =  jsondata['status'];
      message =  jsondata['message'];
      if(status==0){
        showToast(message);
        get_subcategories_list_strlist.clear();
        get_childsubcategories_strlist.clear();
      }
      else{
        var data_list =  jsondata['data'];
        get_subcategories_list = data_list;

        if(get_subcategories_list.length==0) {
          get_subcategories_list_strlist.clear();
          get_subcategories_list.clear();
          get_childsubcategories_strlist.clear();
        }
        else{
          get_subcategories_list_strlist =
          [for(int i = 0; i < get_subcategories_list.length; i++)
            get_subcategories_list[i]['subcategory_name'].toString() + "--" +
                get_subcategories_list[i]['id'].toString(),
          ];
        }
      }
    }
    else {
      throw Exception('Failed to load post');
    }
  }
 // ++++++++++++++++++++++++

  String _selectedget_childsubcategories;
  List get_childsubcategories_list;
  List<String> get_childsubcategories_strlist;
  get_childsubcategories_Data(String subcat_id,) async {
    Map data = {
      'subcat_id':subcat_id,
    };
    var jsondata = null;
    var response = await http.post(Uri.parse(AllApi.get_childsubcategoriesApi),body: data,);
    if(response.statusCode==200){
      final jsondata = jsonDecode(response.body);
      int status =  jsondata['status'];
      message =  jsondata['message'];

      if(status==0){
        showToast(message);
      }
      else{
        var data_list =  jsondata['data'];
        get_childsubcategories_list = data_list;
        get_childsubcategories_strlist = [for(int i = 0; i < get_childsubcategories_list.length; i++)
          get_childsubcategories_list[i]['child_category_name'].toString()
        ];
      }
    }
    else {
      throw Exception('Failed to load post');
    }
  }

  @override
  void initState() {
   setState(() {
     get_dashboard_data(Tabbar_categoryId_select);
     get_subcategories_Data(Tabbar_categoryId_select);
     get_childsubcategories_Data(Tabbar_sub_categoryId);
     getcategoryDATA();
   });
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    setErrorLoadingText();

    return category_list==null?Scaffold():
      DefaultTabController(
      length: category_list.length,
      child: Scaffold(
        appBar: _AppBar(),
        body: Form(
          key: _formKey,
          child: Stack(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: ListView(
                 // physics: NeverScrollableScrollPhysics(),
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: TabBarView(
                        children: [
                          for(int j=0;j<category_list.length;j++)
                           _main_container(Tabbar_categoryId_select.toString()),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                constraints: BoxConstraints(maxHeight: 150.0),
                child: Material(
                  color: Colors.white,
                  child: TabBar(
                    controller: _controller,
                    isScrollable:true,
                    physics:ScrollPhysics() ,
                    labelColor: Color(0xFF8E2E7B),
                    indicatorColor: Color(0xFF8E2E7B),
                    onTap: (index) {
                      setState(() {
                        recent_upload_whole_list.clear();
                        recent_view_whole_list.clear();
                        Tabbar_categoryId_select = category_list[index]['id'];
                        initState();
                      });
                    },
                    tabs: [
                      for(int i=0; i<category_list.length;i++)
                        Tab(
                        //   icon: ImageIcon(
                        //   NetworkImage(data_whole_list[i]['']),
                        //     color:Color(0xFF8E2E7B)
                        // ),
                        child: Text(category_list[i]['category_name'])

                        ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        drawer: MyDrawer(),
        bottomNavigationBar:Bbar()
      ),
    );
  }
  Widget _main_container(c){

    return recent_upload_whole_list==null&&recent_view_whole_list==null?Container(): Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Padding(
              padding: const EdgeInsets.only(left: 10,right: 10,),
              child: ListView(
                //physics: NeverScrollableScrollPhysics(),
                children: [
                  SizedBox(height: 50,),
                  _Stack(),
                  SizedBox(height: 20,),
                  
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Text("Recent Uploads",
                      style: TextStyle(fontSize: 14,
                          fontWeight: FontWeight.bold),),
                  ),
                  SizedBox(height: 10,),
                  Container(
                    height: 200,
                    width: MediaQuery.of(context).size.width,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        for(int i=0;i<recent_upload_whole_list.length;i++)
                        _Recent_Uploads_container(i),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 30),
                    child: Text("Recent View",
                      style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                  ),
                  SizedBox(height: 10,),
            Container(
              height: 200,
              width: MediaQuery.of(context).size.width,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  for(int i=0;i<recent_upload_whole_list.length;i++)
                    _Recent_View_container(i),
                 ]
              ),
            ),
                  SizedBox(height: 220,)
                ],
              ),
            ));
  }
  Widget _AppBar(){
      return AppBar(

      title: Text("Search",
        style: TextStyle(fontSize: 16,color: Color(0xFF8E2E7B)),),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 10),
          child: Row(
            children: [
              GestureDetector(
                  child: Icon(Icons.notifications_none,),
              onTap: () =>Navigator.push(context,
                  MaterialPageRoute(builder:
                      (context) => Notifications()
                  )
              ),
              ),
              SizedBox(width: 10,),
              GestureDetector(
                  child: Icon(Icons.filter_alt_outlined,),
                onTap: () =>Navigator.push(context,
                    MaterialPageRoute(builder:
                        (context) => Filter()
                    )
                ),
              )
            ],
          ),
        )
      ],
    );
  }
  Widget _Stack(){
    return get_subcategories_list == null?Container(): Container(
      width: MediaQuery.of(context).size.width,
      height:300,
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.topRight,
            colors: [Color(0xFF410F4E), Color(0xFF8E2E7B)]
        ),
      ),      child: Padding(
        padding: const EdgeInsets.all(25.0),
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10))
                ,color: Colors.white70
            ),
            // height: 280,
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.only(left: 20,right: 20,top: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Find Your Perfect Here!",
                    style: TextStyle(color: Color(0xFF8E2E7B),fontSize: 12),),
                  SizedBox(height: 5,),
                  Card(
                    color: Colors.black12.withOpacity(0.01),
                    child: Padding(
                      padding: const EdgeInsets.only(
                           left: 20, right: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: DropdownButton(
                              hint: Text("Make",style: TextStyle(fontSize: 13),),
                              isExpanded: true,
                              underline: Container(),
                              value: _selectedget_subcategories,
                              onChanged: (newValue) {
                                setState(() {
                                  _selectedget_subcategories = newValue;
                                  _selectedget_subcategories_id  =  _selectedget_subcategories.split('--')[1];
                                  setState(() {
                                    get_childsubcategories_Data(_selectedget_subcategories_id);
                                    (context as Element).reassemble();
                                    initState();
                                  });
                                });
                              },
                              items: get_subcategories_list_strlist.map((cityTitle) => DropdownMenuItem(
                                  value: cityTitle, child: Text("$cityTitle".split('--')[0],style: TextStyle(fontSize: 13),)))
                                  // value: cityTitle, child: Text("$cityTitle",style: TextStyle(fontSize: 13),)))
                                  .toList(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 3,),

                   get_childsubcategories_list == null?Card():
                  Card(
                    color: Colors.black12.withOpacity(0.01),
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: 20, right: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: DropdownButton(
                              hint: Text("Model",style: TextStyle(fontSize: 13),),
                              isExpanded: true,
                              underline: Container(),
                              value: _selectedget_childsubcategories,
                              onChanged: (newValue) {
                                setState(() {
                                  get_childsubcategories_strlist.clear();
                                  _selectedget_childsubcategories = newValue;
                                });
                              },
                              items: get_childsubcategories_strlist.map((cityTitle) => DropdownMenuItem(
                                  value: cityTitle, child: Text("$cityTitle",style: TextStyle(fontSize: 13),)))
                                  .toList(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  // SizedBox(height: 8,),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.center,
                  //   children: [
                  //     GestureDetector(child: Text("More Filters",
                  //       style:TextStyle( color: Color(0xFF8E2E7B),fontSize: 12) ,),
                  //       onTap: ()=>Navigator.push(context,
                  //           MaterialPageRoute(builder:
                  //               (context) => Filter()
                  //           )),
                  //     )
                  //   ],
                  // ),
                  SizedBox(height: 8,),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        _Positioned(),
                      ],
                    ),
                  ),
                  SizedBox(height: 10,),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
  Widget _Positioned(){
    return RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 35,right: 35,top: 10,bottom: 10),
          child: Text("Search",style: TextStyle(color: Colors.white ),),
        ),
        color:  Color(0xFF8E2E7B),
        onPressed: () {
       if(Tabbar_categoryId_select==null){
         setState(() {
           showToast('Select category');
         });
       }else{
            Navigator.push(context,
            MaterialPageRoute(builder:
                (context) => Carlist(text_dashbord_cat:Tabbar_categoryId_select,
                  text1_dashbord_subcat:_selectedget_subcategories_id ,
                  text2_dashbord_childsubcat:_selectedget_childsubcategories,
                  text_from:'deshbord', )
            )
        );}}
    );
  }

  Widget _Recent_Uploads_container (int i){
    return Stack(
      children: [
        GestureDetector(
          onTap: () =>Navigator.push(context,
              MaterialPageRoute(builder:
                  (context) => Cardetails(text_PostId: recent_upload_whole_list[i]['id'],)
              )
          ),
          child: Container(
            // color: Colors.red,
            width: 160,
            height: 200,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20))
                ),
              //  color: Colors.white,
                height: 200,
                width: 160,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(10),),
                      child: Image.network(AllApi.user_image_url+recent_upload_whole_list[i]['filename'], fit: BoxFit.cover,
                          height: 110,
                          width: 160,),
                      // Image.asset("assets/car.jpg",
                      //   fit: BoxFit.cover,
                      //   height: 110,
                      //   width: 160,
                      // ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10,top: 5),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(recent_upload_whole_list[i]['name'],
                            style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                          Text("\$"+recent_upload_whole_list[i]['price'],
                            style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                          SizedBox(height: 10,),
                          Text("class:\t"+recent_upload_whole_list[i]['vehicle_no'],
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 10,),),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          bottom: 62,right: 10,
          child: GestureDetector(
            onTap: (){

                post_favorite_data(recent_upload_whole_list[i]['id']);

            },
            child: CircleAvatar(
              radius: 15,
              backgroundColor: Colors.white,
              child: Icon(Icons.star,

                color:recent_upload_whole_list[i]['is_favorite']=='0'?Colors.grey:Color(0xFF8E2E7B), size: 18,),
                // color:message=="Unfavorite Sucessfully"?Colors.grey:Color(0xFF8E2E7B),
                // size: 18,),
            ),
          ),
        ),
      ],
    );
  }
  Widget _Recent_View_container (int i){
    return Stack(
      children: [
        GestureDetector(
          onTap: () =>Navigator.push(context,
              MaterialPageRoute(builder:
                  (context) => Cardetails(text_PostId: recent_view_whole_list[i]['id'],)
              )
          ),
          child: Container(
            // color: Colors.red,
            width: 160,
            height: 200,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20))
                ),
                //  color: Colors.white,
                height: 200,
                width: 160,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(10),),
                      child: Image.network(AllApi.user_image_url+recent_view_whole_list[i]['filename'], fit: BoxFit.cover,
                        height: 110,
                        width: 160,),
                      // Image.asset("assets/car.jpg",
                      //   fit: BoxFit.cover,
                      //   height: 110,
                      //   width: 160,
                      // ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10,top: 5),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(recent_view_whole_list[i]['name'],
                            style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                          Text("\$"+recent_view_whole_list[i]['price'],
                            style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                          SizedBox(height: 10,),
                          Text("class:\t"+recent_view_whole_list[i]['vehicle_no'],
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 10,),),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          bottom: 62,right: 10,
          child: GestureDetector(
            onTap: (){
              setState(() {
                post_favorite_data(recent_view_whole_list[i]['id']);
               // post_favorite_data(recent_view_whole_list[i]['is_favorite']);
              });
            },
            child: CircleAvatar(
              radius: 15,
              backgroundColor: Colors.white,
              child: Icon(Icons.star,
                color:recent_view_whole_list[i]['is_favorite']=='0'?Colors.grey:Color(0xFF8E2E7B), size: 18,),
            ),
          ),
        ),
      ],
    );
  }
  // Widget _Card(){
  //   return  Card(
  //     elevation: 5,
  //     shape: RoundedRectangleBorder(
  //       borderRadius: BorderRadius.circular(20),
  //     ),
  //     child: Container(
  //       decoration: BoxDecoration(
  //           borderRadius: BorderRadius.all(Radius.circular(20))
  //       ),
  //       // color: Colors.white,
  //       height: 120,
  //       child: Row(
  //         mainAxisAlignment: MainAxisAlignment.center,
  //         crossAxisAlignment: CrossAxisAlignment.start,
  //         children: [
  //           ClipRRect(
  //             borderRadius: BorderRadius.horizontal(left: Radius.circular(10),),
  //             // borderRadius: BorderRadius.circular(10),
  //             child: Image.asset("assets/car.jpg",
  //               fit: BoxFit.cover,
  //               height: 120,
  //               width: 160,
  //             ),
  //           ),
  //           SizedBox(width: 10,),
  //           Expanded(
  //             child: Column(
  //               mainAxisAlignment: MainAxisAlignment.start,
  //               crossAxisAlignment: CrossAxisAlignment.start,
  //               children: [
  //                 Padding(
  //                   padding: const EdgeInsets.only(top: 10,),
  //                   child: Row(
  //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                     children: [
  //                       Text("KL Quonto",
  //                         style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
  //                       Card(
  //                         shape: RoundedRectangleBorder(
  //                           borderRadius: BorderRadius.circular(20),
  //                         ),
  //                         child: CircleAvatar(
  //                           radius: 15,
  //                           backgroundColor: Colors.white,
  //                           child: Icon(Icons.star,color:  Color(0xFF8E2E7B),size: 20,),
  //                         ),
  //                       )
  //                     ],
  //                   ),
  //                 ),
  //                 Text("\$2000",
  //                   style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
  //                 SizedBox(height: 10,),
  //                 Text("Closs:40Z Albite 348647",
  //                   style: TextStyle(fontSize: 10,),),
  //               ],
  //             ),
  //           )
  //         ],
  //       ),
  //     ),
  //   );
  // }
}
