phoneNo(val, fieldName) {
  RegExp nameExp = new RegExp(r'^([0-9]{11})$');
  if (val != "") {
    if (val.length != 11)
      return fieldName + " should be 11 digit";
    else if (!nameExp.hasMatch(val))
      return fieldName + " should be in correct format";
    return null;
  } else {
    return fieldName + "Required";
  }
}

validWalletMoney(val, fieldName) {
  RegExp nameExp = new RegExp(r'^([0-9])');
  if (val != "") {
    if (!nameExp.hasMatch(val))
      return fieldName + " should be in correct format";
    else if (double.parse(val) < 1000)
      return "Minimum Amount is N1000";
  } else {
    return "Enter Valid " + fieldName;
  }
  }

validMoney(val, fieldName) {
  RegExp nameExp = new RegExp(r'^([0-9])');
  if (val != "") {
    if (!nameExp.hasMatch(val))
      return "Enter " + fieldName + " in correct format";
    else if (double.parse(val) < 50.00)
      return "Minimum Recharge Amount is N50";
  } else {
    return "Enter Valid " + fieldName;
  }
}
  otpCode(val, field, [min = '4', max = '4']) {
    final RegExp nameExp = new RegExp("^\\w{$min,$max}\$");
    if (!nameExp.hasMatch(val)) return "OTP must be of 4-digits";
  }

  requiredField(val, field) {
    if (val.isEmpty) {
      return field + 'is required';
    }
  }

  validEmailField(val, field) {
    final RegExp nameExp =
    new RegExp(r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$');
    if (val.isEmpty) {
      return field + ' Required';
    } else
    if (!nameExp.hasMatch(val)) return 'Please enter valid email address.';
  }

  validIfscField(val, fieldName) {
    RegExp nameExp = new RegExp(r'^[A-Z]{4}[0][A-Z0-9]{6}$');
    if (val != "") {
      if (!nameExp.hasMatch(val))
        return fieldName + " should be in correct Indian format";
    } else {
      return fieldName + "";
    }
  }

  validPasswordField(val, field) {
    final RegExp nameExp = new RegExp(
        r'^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,15}$');
    if (val.isEmpty) {
      return field + ' Required';
    } else if (!nameExp.hasMatch(val))
      return "Password is not secure.\nMust contain 1 uppercase, 1 lowercase,1 number";
  }

  validPasswordFormet(val) {
    final RegExp nameExp =
    new RegExp(r'^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[-.@!$£%&#]).{8,15}$');

    if (!nameExp.hasMatch(val))
      return "Password Must contain 1 uppercase,1 lowercase,1 number,1 special character (-.@!\$£%&#)";
  }

  passwordNum(val, field, [min = '8', max = '18']) {
    final RegExp nameExp = new RegExp("^\\w{$min,$max}\$");
    if (val != "") {
      if (!nameExp.hasMatch(val)) return "Please enter 8 digit number";
    }
    return null;
  }

   String validate_String(String value, String message){
    return (value.isEmpty) ? message : null;
    // return (value.isEmpty || (value.contains(**SPECIAL CHARACTERS**))) ? message : null;

}