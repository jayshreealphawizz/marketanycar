import 'package:progress_hud/progress_hud.dart';
import 'package:flutter/material.dart';


const String baseUrl = 'https://eleven11pay.com/web-service/';

ProgressHUD progressHUD = ProgressHUD(
  loading: false,
  color: Colors.purpleAccent,
  borderRadius: 5.0,
  backgroundColor: Colors.transparent,
  text: 'Loading...',
);