import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'comman/_bottomAppBar.dart';
import 'log/allapi.dart';
import 'log/log.dart';

class Valuecar extends StatefulWidget {
  @override
  _ValuecarState createState() => _ValuecarState();
}

class _ValuecarState extends State<Valuecar> {
  final _formKey = GlobalKey<FormState>();
  String vehicle_no;

  Future<Post> post;
  TextEditingController vehicle_noController = new TextEditingController();

  void vehicle_no_verify(String vehicle_no) async {
    if(vehicle_no.isEmpty){
      showToast('Please enter Registration number');
    }else {
      Map data = {
        "vehicle_no": vehicle_no,
      };
      var jsondata = null;
      var response =
      await http.post(Uri.parse(AllApi.enter_vehicle_no_verifyApi), body: data);
      if (response.statusCode == 200) {
        jsondata = jsonDecode(response.body);
        int status = jsondata['status'];
        String message = jsondata['message'];
        if (status == 0) {
          showToast(message);
        } else {
          showToast(message);
          setState(() {
            // showToast();
            // Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context)=> Loginpage()),);
          });
        }
      } else {
        throw Exception('Failed to load post');
      }
    }
  }

  @override
  void initState() {
    // Forgotpass(vehicle_no );
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Value my car",
          style: TextStyle(fontSize: 16, color: Color(0xFF8E2E7B)),
        ),
      ),
      body: Form(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: ListView(
            // physics: NeverScrollableScrollPhysics(),
            children: [
              Stack(
                children: [
                  Image.asset(
                    "assets/car.jpg",
                    fit: BoxFit.cover,
                    height: 230,
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                child: Card(
                  elevation: 5,
                  child: Container(
                    height: 250,
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding:
                          const EdgeInsets.only(left: 15, right: 15, top: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Registration number",
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          TextFormField(
                            key: Key("vehicle_no"),
                            controller: vehicle_noController,
                            cursorColor: Color(0xFF8E2E7B),
                            decoration: InputDecoration(
                              prefixIcon: Padding(
                                padding: const EdgeInsets.only(right: 15),
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.horizontal(
                                      left: Radius.circular(10),
                                    ),
                                    color: Color(0xFF8E2E7B),
                                  ),
                                  width: 55,
                                  height: 57,
                                  child: Column(
                                    children: [
                                      Icon(
                                        Icons.radio_button_off,
                                        color: Colors.white54,
                                        size: 30,
                                      ),
                                      Text(
                                        "GB",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 14),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              fillColor: Colors.white.withOpacity(0.3),
                              filled: true,
                              border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                              hintText: "Ex ABC12CD",
                              hintStyle: TextStyle(
                                  color: Colors.black38, fontSize: 13),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              RaisedButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      top: 13, bottom: 13, left: 30, right: 30),
                                  child: Text(
                                    "Get Your Valuation",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 14),
                                  ),
                                ),
                                color: Color(0xFF8E2E7B),
                                onPressed: () async {
                                    vehicle_no_verify(vehicle_noController.text);
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: Bbar(),
    );
  }
}
