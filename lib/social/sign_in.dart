import 'dart:ffi';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn googleSignIn = GoogleSignIn();
// retrieved from the FirebaseUser
String social_name;
String social_email;
String social_imageUrl;
var databaseReference;


Future<String> signInWithGoogle() async {
  await Firebase.initializeApp();

  final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
  final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;
  databaseReference = FirebaseDatabase.instance.reference();

  final AuthCredential credential = GoogleAuthProvider.credential(accessToken: googleSignInAuthentication.accessToken, idToken: googleSignInAuthentication.idToken,);

  final UserCredential authResult = await _auth.signInWithCredential(credential);
  final User firebaseUser = authResult.user;

  print('user =======');print(firebaseUser);print('authResult =======');print(authResult);

  if (firebaseUser != null) {
    final result = (await FirebaseFirestore.instance.collection('users').where('id', isEqualTo: firebaseUser.uid).get()).docs;
    if (result.length == 0) {
      ///new user
      FirebaseFirestore.instance
          .collection('users')
          .doc(firebaseUser.uid)
          .set({
        "id": firebaseUser.uid,
        "name": firebaseUser.displayName,
        "profile_pic": firebaseUser.photoURL,
        "created_at": DateTime.now().millisecondsSinceEpoch,
      });
    } else {
    }

    assert(!firebaseUser.isAnonymous);
    assert(await firebaseUser.getIdToken() != null);
    assert(firebaseUser.email != null);
    assert(firebaseUser.displayName != null);
    assert(firebaseUser.photoURL != null);

    final User currentUser = _auth.currentUser;
    assert(firebaseUser.uid == currentUser.uid);
    social_name = firebaseUser.displayName;
    social_email = firebaseUser.email;
    social_imageUrl = firebaseUser.photoURL;

    print('signInWithGoogle succeeded: $firebaseUser');

      if (social_name.contains(" ")) {
      social_name = social_name.substring(0, social_name.indexOf(" "));
    }

    return '$firebaseUser';
  }
  return null;
}



Future<void> signOutGoogle() async {
  await googleSignIn.signOut();
  print("User Signed Out");
}


