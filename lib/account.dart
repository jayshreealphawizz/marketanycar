import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'home.dart';
import 'log/allapi.dart';
import 'log/log.dart';
import 'log/login.dart';
class Account extends StatefulWidget {
  Account({Key key}) : super(key: key);

  @override
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<Account> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final prefs =  SharedPreferences.getInstance();
  TextEditingController emailController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();
  TextEditingController mobileController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  Future<Post> post;

  Creatid(String email, password, name, mobile) async {
    Map data = {
      "name":name,
      "mobile":mobile,
      "email": email,
      "password": password,
    };

    var jsondata = null;
    var response = await http.post(Uri.parse(AllApi.registerApi),body: data);
    if(response.statusCode==200){
    jsondata = jsonDecode(response.body);
      int status =  jsondata['status'];
      String message =  jsondata['message'];
      if(status==0){
        showToast(message);
      }
      else {
        setState(() {
          showToast(message);
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => Home()),);
          String id = jsondata['data']['id'];
          String email = jsondata['data']['email'];
          String name = jsondata['data']['first_name'];
          String mobile = jsondata['data']['mobile'];
          String address = jsondata['data']['address'];
          String Authorization = jsondata['data']['Authorization'];
          // String status =  jsondata['status'];

          addStringToSF(id, Authorization,email,name,mobile,address);
        });
      }
    }
    else {
      throw Exception('Failed to load post');
    }
  }
  addStringToSF(String id ,Authorization,email,first_name,mobile,address) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(AllApi.userId, id);
    prefs.setString(AllApi.useremail, email);
    prefs.setString(AllApi.userfirst_name, first_name);
    prefs.setString(AllApi.usermobile, mobile);
    prefs.setString(AllApi.useraddress, address);
    prefs.setString(AllApi.userAuthorization , Authorization);
    // return json.decode(prefs.getString("gender"));
  }
  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: ListView(
          children: [
            Stack(
              children: [
                Image.asset("assets/Mask 180.png",
                   fit: BoxFit.fill,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                ),
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25,right: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height:MediaQuery.of(context).size.height*0.15,),
                        Image.asset("assets/applogo.png",
                          height: 100,width: 200,),
                        Text("Create Account",style: TextStyle(
                          fontSize: 16,fontWeight: FontWeight.bold,
                        ),),

                        TextFormField(
                          controller: nameController,
                            key: Key("name"),
                          textInputAction: TextInputAction.next,
                            style: TextStyle(color: Color(0xFF8E2E7B)),
                            cursorColor: Color(0xFF8E2E7B),
                            decoration: InputDecoration(
                              fillColor: Colors.black12,
                              border: InputBorder.none,
                              filled: true,
                              hintText: "Name",
                              hintStyle: TextStyle( color: Colors.black38,fontSize: 12),
                            ),
                            validator: (val){
                            if(val.isEmpty){
                              return 'enter your name';
                            }
                            } ,
                        ),
                        TextFormField(
                          controller: emailController,
                            key: Key("email"),
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.emailAddress,
                            style: TextStyle(color: Color(0xFF8E2E7B)),
                            cursorColor: Color(0xFF8E2E7B),
                            decoration: InputDecoration(
                              fillColor: Colors.black12,
                              border: InputBorder.none,
                              filled: true,
                              hintText: "Email",
                              hintStyle: TextStyle( color: Colors.black38,fontSize: 12),
                            ),
                          validator: (val){
                            final RegExp nameExp =
                            new RegExp(r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$');
                            if(val.isEmpty){
                              return'Please enter email address.';
                            }
                            else if(!nameExp.hasMatch(val)){
                              return 'Please enter valid email address.';
                            }
                          },
                        ),
                        TextFormField(
                          maxLength: 10,
                          controller: mobileController,
                            key: Key("mobile"),
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.number,
                            style: TextStyle(color: Color(0xFF8E2E7B)),
                            cursorColor: Color(0xFF8E2E7B),
                            decoration: InputDecoration(
                              counterText: "",
                              fillColor: Colors.black12,
                              border: InputBorder.none,
                              filled: true,
                              hintText: "Mobil No.",
                              hintStyle: TextStyle( color: Colors.black38,fontSize: 12),
                            ),
                          validator: (val ){
                            RegExp nameExp = new RegExp(r'^([0-9]{10})$');
                            if (val != "") {
                              if (val.length != 10)
                                return " should be 10 digit";
                              else if (!nameExp.hasMatch(val))
                                return " should be in correct format";
                              return null;
                            } else {
                              return "Required";
                            }
                          },
                        ),

                        TextFormField(
                          controller: passwordController,
                            key: Key("password"),
                          textInputAction: TextInputAction.done,
                            style: TextStyle(color: Color(0xFF8E2E7B)),
                            cursorColor: Color(0xFF8E2E7B),
                            decoration: InputDecoration(
                              fillColor: Colors.black12,
                              border: InputBorder.none,
                              filled: true,
                              hintText: "password",
                              hintStyle: TextStyle( color: Colors.black38,fontSize: 12),
                            ),
                          validator: (val){
                            if(val.isEmpty){
                              return 'enter your password';
                            }
                          } ,
                          // validator: (val){
                          //   final RegExp nameExp = new RegExp(
                          //       r'^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,15}$');
                          //   if (val.isEmpty) {
                          //     return ' Required';
                          //   } else if (!nameExp.hasMatch(val))
                          //     return "Password is not secure.\nMust contain 1 uppercase, 1 lowercase,1 number";
                          // },
                        ),


                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(left: 40,right: 40,top: 15,bottom: 15),
                                child: Text("Create",style: TextStyle(color: Colors.white ),),
                              ),
                              color:  Color(0xFF8E2E7B),
                              onPressed: ()
                              // {
                              //         Navigator.of(context).push(
                              //           MaterialPageRoute(builder: (BuildContext context) => Home()),);
                              //       }
                              async {
                                await showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return Center(child: CircularProgressIndicator(),);
                                    });
                                Navigator.pop(context);
                                if (_formKey.currentState.validate()) {
                                  Creatid(emailController.text,passwordController.text,nameController.text,mobileController.text);
                                }
                                else{
                                }
                              },
                            ),
                          ],),

                        SizedBox(height: 5,),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Already Have An Account",style: TextStyle(
                                color:Colors.black54,fontSize: 14
                            ),),
                            GestureDetector(
                              onTap: () =>
                                  Navigator.push(context,
                                      MaterialPageRoute(builder:
                                          (context) => Loginpage()
                                      )
                                  ),
                              child: Text(" Log in",style: TextStyle(
                                  color: Color(0xFF8E2E7B),fontSize: 14,
                                fontWeight: FontWeight.bold
                              ),),
                            ),
                          ],
                        ),
                        SizedBox(height:MediaQuery.of(context).size.height*0.1,),
                      ],
                    ),
                  ),

                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
