import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:ui' as ui;
import 'cardetails.dart';
import 'comman/_bottomAppBar.dart';
import 'log/allapi.dart';
import 'log/login.dart';
import 'notifications.dart';
class Favoritelist extends StatefulWidget {
  @override
  _FavoritelistState createState() => _FavoritelistState();
}

class _FavoritelistState extends State<Favoritelist> {

  String userAuthorization,res_message;
  List get_favorite_post_list ;

  get_favorite_post_list_data(String pageNumber,pageSize) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userAuthorization = prefs.getString(AllApi.userAuthorization);
    if(userAuthorization==null){
      Navigator.of(context).pop();
      Navigator.push(context,
          MaterialPageRoute(builder:
              (context) => Loginpage()
          ));
    }else {
      Map data = {
        'pageNumber': pageNumber,
        'pageSize': pageSize,
      };
      Map<String, String> requestHeaders = {
        'Authorizations': userAuthorization
      };
      var jsondata = null;
      var response = await http.post(Uri.parse(AllApi.get_favorite_post_listApi), body: data, headers: requestHeaders);
      if (response.statusCode == 200) {
        jsondata = jsonDecode(response.body);
        int status = jsondata['status'];
        res_message = jsondata['message'];
        var data_list = jsondata['data'];
        if (status == 0) {
          // showToast(res_message);
        }
        else {
          setState(() {
            get_favorite_post_list = data_list;
          });
        }
      }
      else {
        throw Exception('Failed to load post');
      }
    }
  }

  @override
  void initState() {
    setState(() {
      get_favorite_post_list_data("1",'20');
    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Favorite List",
            style: TextStyle(fontSize: 16,color: Color(0xFF8E2E7B) )),
        actions: [
          GestureDetector(
              onTap: () =>Navigator.push(context,
                  MaterialPageRoute(builder:
                      (context) => Notifications()
                  )
              ),
              child: Icon(Icons.notifications_none,)),
          SizedBox(width: 10,),
        ],
      ),

      body: get_favorite_post_list==null?Container(child: Center(child: Text('No Records found')),): Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
          child: Padding(
            padding: const EdgeInsets.only(left: 10,right: 10),
            child: ListView.builder(
              itemCount:get_favorite_post_list.length,
              itemBuilder: (BuildContext context,int index){
                return _Card(index);
              }
      ),
          ),
      ),
      bottomNavigationBar: Bbar(),
    );
  }
  Widget _Card(int i){
    return  Padding(
      padding: const EdgeInsets.only(top: 10,bottom: 5),
      child: Container(
        color: Colors.black12,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.network(AllApi.user_image_url+get_favorite_post_list[i]['filename'],
                        fit: BoxFit.cover,
                        height: 150,
                        width: MediaQuery.of(context).size.width*0.35,),
                      // Image.asset("assets/car.jpg",
                      //   fit: BoxFit.cover,
                      //   height: 150,
                      //   width: MediaQuery.of(context).size.width*0.35,
                     // ),
                    ),
                    Positioned(
                      top: 5,
                      left: 5,
                      child: Container(
                        color: Color(0xFF8E2E7B),
                        width: 50,
                        height: 15,
                        child: Center(
                          child: Text("Featured Car",
                            style: TextStyle(fontSize: 6,color: Colors.white),),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10,),
                Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.network(AllApi.user_image_url+get_favorite_post_list[i]['filename'],
                        fit: BoxFit.cover,
                        height: 60,
                        width: MediaQuery.of(context).size.width*0.18,
                      ),
                    ),
                    SizedBox(width: 10,),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.network(AllApi.user_image_url+get_favorite_post_list[i]['filename'],
                        fit: BoxFit.cover,
                        height: 60,
                        width: MediaQuery.of(context).size.width*0.14,
                      ),
                    ),
                  ],
                )
              ],
            ),
            SizedBox(width: 10,),
            Expanded(
              child: GestureDetector(
                onTap: () =>Navigator.push(context,
                    MaterialPageRoute(builder:
                        (context) => Cardetails(text_PostId: get_favorite_post_list[i]['id'],)
                    )
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 10,),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text(get_favorite_post_list[i]['name'],
                              style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
                          ),
                          // SizedBox(height: 5,),
                          Container(
                            color: Colors.green,
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Text("GOOD PRICE",style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 8
                              ),),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 5,),
                    Text(get_favorite_post_list[i]['description'],
                      style: TextStyle(fontSize: 12,),),
                    SizedBox(height: 5,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("\$"+get_favorite_post_list[i]['price'],
                              style: TextStyle(fontSize: 15,
                                  color: Color(0xFF8E2E7B),
                                  fontWeight: FontWeight.bold),),
                            SizedBox(height: 5,),
                            Text("\$"+get_favorite_post_list[i]['offer_price']+"pm (PCP)",
                              style: TextStyle(fontSize: 12,),),
                          ],
                        ),
                       // Image.asset("assets/Image 3.png",scale: 1.3,)
                      ],
                    ),
                    SizedBox(height: 5,),
                    Column(
                      children: [
                        Wrap(
                          alignment: WrapAlignment.start,
                          children: [
                            Column(children: [
                              CircleAvatar(
                                radius: 15,
                                backgroundColor: Colors.white,
                                child: ImageIcon(
                                  AssetImage("assets/icons/filter.png"),
                                  color:Color(0xFF8E2E7B) ,size: 18,
                                ),
                              ),
                              SizedBox(height: 3,),
                              Text("Manual",
                                style: TextStyle(fontSize: 8), )
                            ],),
                            Column(children: [
                              CircleAvatar(
                                radius: 15,
                                backgroundColor: Colors.white,
                                child:  ImageIcon(
                                  AssetImage("assets/icons/pump.png"),
                                  color:Color(0xFF8E2E7B) ,size: 18,
                                ),
                              ),
                              SizedBox(height: 3,),
                              Text(get_favorite_post_list[i]['fuel_type'],
                                style: TextStyle(fontSize: 8), )
                            ],),
                            Column(children: [
                              CircleAvatar(
                                radius: 15,
                                backgroundColor: Colors.white,
                                child: ImageIcon(
                                  AssetImage("assets/icons/droplet.png"),
                                  color:Color(0xFF8E2E7B) ,size: 18,
                                ),
                              ),
                              SizedBox(height: 3,),
                              Text(get_favorite_post_list[i]['mileage'],
                                style: TextStyle(fontSize: 8), )
                            ],),
                            Column(children: [
                              CircleAvatar(
                                radius: 15,
                                backgroundColor: Colors.white,
                                child: ImageIcon(
                                  AssetImage("assets/icons/road.png"),
                                  color:Color(0xFF8E2E7B) ,size: 18,
                                ),
                              ),
                              SizedBox(height: 3,),
                              Text(get_favorite_post_list[i]['distance'],
                                style: TextStyle(fontSize: 8), )
                            ],),
                            Column(children: [
                              CircleAvatar(
                                radius: 15,
                                backgroundColor: Colors.white,
                                child: ImageIcon(
                                  AssetImage("assets/icons/calendar.png"),
                                  color:Color(0xFF8E2E7B) ,size: 18,
                                ),
                              ),
                              SizedBox(height: 3,),
                              Text(get_favorite_post_list[i]['registration_date'],
                                style: TextStyle(fontSize: 8), )
                            ],),
                            Column(children: [
                              CircleAvatar(
                                radius: 15,
                                backgroundColor: Colors.white,
                                child: Icon(Icons.attach_money,
                                  color:  Color(0xFF8E2E7B),size: 18,
                                ),
                              ),
                              SizedBox(height: 3,),
                              Text("\$"+get_favorite_post_list[i]['price'],
                                style: TextStyle(fontSize: 8), )
                            ],),
                          ],),
                      ],
                    ),
                    SizedBox(height: 5,),
                    Row(
                      children: [
                        Icon(Icons.location_on_sharp,color: Color(0xFF8E2E7B) ,size: 18,),
                        Text(get_favorite_post_list[i]['location'],
                          style: TextStyle(fontSize: 12,
                            color:  Color(0xFF8E2E7B),),),
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
