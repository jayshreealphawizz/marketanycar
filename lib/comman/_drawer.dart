import 'dart:async';
import 'dart:convert';

import 'package:Marketanycar/social/sign_in.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:Marketanycar/log/allapi.dart';
import 'package:Marketanycar/log/log.dart';
import 'package:Marketanycar/log/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../change_pass.dart';
import '../chetlist.dart';
import '../faq.dart';
import '../favoritelist.dart';
import '../filter.dart';
import '../mypostlist.dart';
import '../privacy.dart';
import '../profile.dart';
import '../termuse.dart';
import '../value.dart';

class MyDrawer extends StatefulWidget {
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String Authorization;
  String userfirst_name,userimage;

  _logout() {

    signOutGoogle();
    facebooksignOut();
    clearToSF();
    // Navigator.of(context).push(
    //   MaterialPageRoute(builder: (BuildContext context) => Loginpage()),
    // );

    Navigator.of(context).pop();
    Navigator.push(context, MaterialPageRoute(builder: (context) => Loginpage()));
  }

  Future<List<Post>> logoutApiCall() async {
    if(Authorization==null){
      Navigator.of(context).pop();
      Navigator.push(context, MaterialPageRoute(builder: (context) => Loginpage()));
    }
    else {
      Map<String, String> headers = {'Authorizations': Authorization};
      final response = await http.get(Uri.parse(AllApi.logoutApi), headers: headers);
      final jsonResponse = jsonDecode(response.body);
      int status = jsonResponse['status'];
      String message = jsonResponse['message'];
      if (status == 0) {
        showToast(message);
      } else {
        setState(() {
          showToast(message);
          Navigator.of(context).pop();
          _logout();
        });
      }
    }
  }

  clearToSF() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
  }

  addStringToSF(String id ,String Authorization, String name, String email, String mobile, String address) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(AllApi.userId, id);
    prefs.setString(AllApi.userAuthorization , Authorization);
    prefs.setString(AllApi.userfirst_name , name);
    prefs.setString(AllApi.useremail, email);
    prefs.setString(AllApi.usermobile, mobile);
    prefs.setString(AllApi.useraddress, address);
  }

  _nameRetriever() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userfirst_name = prefs.getString(AllApi.userfirst_name);
    Authorization = prefs.getString(AllApi.userAuthorization);
    userimage = prefs.getString(AllApi.userimage);
    (context as Element).reassemble();
  }
  @override
  void initState() {
    setState(() {
      _nameRetriever();
    });
    super.initState();
    return;
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 5,
      child: ListView(
        children: [
          DrawerHeader(
            curve: Curves.fastOutSlowIn,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                CircleAvatar(
                  backgroundImage: userimage==null?AssetImage("assets/placeholder_user.png",):

                  NetworkImage(AllApi.user_image_url+userimage),
                  // AssetImage(
                  //   "assets/placeholder_user.png",
                  // ),
                  radius: 30,
                ),
                // Container(
                //   height: 0,
                //   width: 0,
                //   child: TextFormField(
                //     autofocus: true,
                //     key: Key('name'),
                //     style: TextStyle(color: Colors.black),
                //     // onFieldSubmitted: (String) {
                //     cursorColor: Colors.black,
                //     textInputAction: TextInputAction.next,
                //     decoration: InputDecoration(
                //         fillColor: Colors.white,
                //         filled: true,
                //         hintText: (userfirst_name),
                //         hintStyle:
                //             TextStyle(color: Colors.black87, fontSize: 12),
                //         border: InputBorder.none),
                //   ),
                // ),
                Expanded(
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                        margin: const EdgeInsets.only(left: 15.0),
                        child: new Text(
                          userfirst_name != null ? userfirst_name : '',
                        ),
                      ),
                    ],
                  ),
                ),
                IconButton(
                    icon: ImageIcon(
                      AssetImage("assets/icons/edit.png"),
                      color: Color(0xFF8E2E7B),
                      size: 16,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Profile()));
                    })
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: ListTile(
                title: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: Icon(
                        Icons.filter_alt,
                        size: 20,
                        color: Color(0xFF8E2E7B),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: Text("Filter Search"),
                    )
                  ],
                ),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Filter()));
                }

                // onTap: (){
                //   Navigator.of(context).pop();
                //   Navigator.push(context,
                //     MaterialPageRoute(builder:
                //         (BuildContext context) => Filter()),)
                //       .then((value) => setState(() {}));
                //
                // }
                ),
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: ListTile(
                title: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: Icon(
                        Icons.star,
                        size: 20,
                        color: Color(0xFF8E2E7B),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: Text("Favorites"),
                    )
                  ],
                ),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Favoritelist()));
                }),
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: ListTile(
                title: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: Icon(
                        Icons.person,
                        size: 20,
                        color: Color(0xFF8E2E7B),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: Text("My Account"),
                    )
                  ],
                ),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Profile()));
                }),
          ),
          // Divider(),
          // Padding(
          //   padding: const EdgeInsets.only(left: 10),
          //   child: ListTile(
          //       title: Row(
          //         children: <Widget>[
          //           Padding(
          //             padding: const EdgeInsets.all(3.0),
          //             child: Icon(
          //               Icons.history,
          //               size: 20,
          //               color: Color(0xFF8E2E7B),
          //             ),
          //           ),
          //           SizedBox(
          //             width: 10,
          //           ),
          //           Padding(
          //             padding: EdgeInsets.only(left: 8.0),
          //             child: Text("History"),
          //           )
          //         ],
          //       ),
          //       onTap: () {
          //         Navigator.of(context).pop();
          //         Navigator.push(context,
          //             MaterialPageRoute(builder: (context) => Historys()));
          //       }),
          // ),
          Divider(),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: ListTile(
                title: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: ImageIcon(
                        AssetImage("assets/icons/upload.png"),
                        color: Color(0xFF8E2E7B),
                        size: 20,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: Text("My Post"),
                    )
                  ],
                ),

                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Mypost_list(),
                      ));
                }),
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: ListTile(
                title: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: ImageIcon(
                        AssetImage("assets/icons/chatboxes.png"),
                        color: Color(0xFF8E2E7B),
                        size: 20,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: Text("Chat List"),
                    )
                  ],
                ),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Chatlist()));
                }),
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: ListTile(
                title: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: Icon(
                        Icons.attach_money,
                        size: 22,
                        color: Color(0xFF8E2E7B),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: Text("Value My Care"),
                    )
                  ],
                ),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Valuecar()));
                }),
          ),

          Divider(),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: ListTile(
                title: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: ImageIcon(
                        AssetImage("assets/icons/question.png"),
                        color: Color(0xFF8E2E7B),
                        size: 20,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: Text("FAQ"),
                    )
                  ],
                ),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Faq()));
                }),
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: ListTile(
                title: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: Icon(
                        Icons.insert_drive_file,
                        size: 20,
                        color: Color(0xFF8E2E7B),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: Text("Privacy Policy"),
                    )
                  ],
                ),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Privacy()));
                }),
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: ListTile(
                title: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: Icon(
                        Icons.insert_drive_file,
                        size: 20,
                        color: Color(0xFF8E2E7B),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: Text("Term of Use"),
                    )
                  ],
                ),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Termuse()));
                }),
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: ListTile(
                title: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: ImageIcon(
                        AssetImage("assets/change_Password.png"),
                        color: Color(0xFF8E2E7B),
                        size: 20,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: Text("Change Password"),
                    )
                  ],
                ),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Change_pass()));
                }),
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: ListTile(
                title: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: ImageIcon(
                        AssetImage("assets/icons/exit.png"),
                        color: Color(0xFF8E2E7B),
                        size: 20,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: Text("Logout"),
                    )
                  ],
                ),
                onTap: () {
                  //Navigator.of(context).pop();
                  _showMyDialog(true);
                  // _exitApp(context);
                }),
          ),
          Divider(),
        ],
      ),
    );
  }

  Future<void> _showMyDialog(bool enable) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return AlertDialog(
              // title: Text('Do you want to exit this application?',),
              content: Text('Are you sure, you want to exit this application ?'),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                  child: Text('No'),
                ),
                TextButton(
                  onPressed: () {
                    logoutApiCall();
                    Navigator.of(context).pop(false);
                  },
                  child: Text('Yes'),
                ),
              ],
            );
          });
        });
  }

// Future<bool> _exitApp(BuildContext context) {
//   return showDialog(
//     context: context,
//     child: AlertDialog(
//       title: Text('Do you want to exit this application?'),
//       content: Text('We hate to see you leave...'),
//       actions: <Widget>[
//         FlatButton(
//           onPressed: () {
//             print("you choose no");
//             Navigator.of(context).pop(false);
//           },
//           child: Text('No'),
//         ),
//         FlatButton(
//           onPressed: () {
//             getJasonData();
//             Navigator.of(context).pop(false);
//           },
//           child: Text('Yes'),
//         ),
//       ],
//     ),
//   ) ;
// }
}
