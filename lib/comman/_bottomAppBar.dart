import 'package:flutter/material.dart';
import 'package:Marketanycar/log/allapi.dart';
import 'package:Marketanycar/log/login.dart';
import '../chetlist.dart';

import '../deshbord.dart';
import '../home.dart';
import '../profile.dart';
import '../sell_any_car.dart';
import '../vehicleone.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Bbar extends StatefulWidget {

  @override
  _BbarState createState() => _BbarState();
}

class _BbarState extends State<Bbar> {
String useremail, userAuthorization;

  getSharedPrefrenceData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    useremail = prefs.getString(AllApi.useremail);
    userAuthorization = prefs.getString(AllApi.userAuthorization);
  }

  @override
  void initState() {
    getSharedPrefrenceData();
  }

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
        elevation: 10.0,
        color: Colors.white,
        //  shape: CircularNotchedRectangle(),
        child: Container(
          height: 75,
          child: Padding(
            padding: const EdgeInsets.only(left: 15,right: 15),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  iconSize: 30.0,
                  icon: Icon(Icons.home,color: Colors.black,),
                  onPressed: () =>
                      Navigator.pushReplacement(context,
                      MaterialPageRoute(builder:
                          (context) => Home())

                  ),
                ),

                IconButton(
                    iconSize: 30.0,
                    icon: Icon(Icons.search,color: Colors.black,),
                    onPressed: () =>Navigator.push(context,
                        MaterialPageRoute(builder:
                            (context) =>Deshbord()
                        )
                    )
                ),

                IconButton(
                    padding: const EdgeInsets.only(
                      bottom: 15),
                  iconSize: 25.0,
                  icon: Icon(Icons.add_circle_outline,
                    color: Colors.black87,
                    size: 42,),
                    onPressed:(){
                      if (userAuthorization == null) {
                        showToast('Before Add any post, Please login first');
                      } else {
                        Navigator.of(context).push(
                          MaterialPageRoute(builder: (BuildContext context) => Vehicleone()),);
                      }
                    }
                ),

                IconButton(
                  iconSize: 25.0,
                  icon: Icon(Icons.chat_bubble_outline,color: Colors.black,),
                  onPressed: () =>Navigator.push(context,
                      MaterialPageRoute(builder:
                          (context) => Chatlist()
                      )
                  ),
                ),
                IconButton(
                  iconSize: 3.0,
                  icon: CircleAvatar(
                    backgroundImage: AssetImage("assets/placeholder_user.png"),
                  ),
                    onPressed:(){
                      if (userAuthorization == null) {
                        Navigator.of(context).push(
                          MaterialPageRoute(builder: (BuildContext context) => Loginpage()),);
                      } else {
                        Navigator.of(context).push(
                          MaterialPageRoute(builder: (BuildContext context) => Profile()),);
                      }
                    }
                ),
              ],
            ),
          ),
        )
    );
  }
}
