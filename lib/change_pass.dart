import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'home.dart';
import 'log/allapi.dart';
class Change_pass extends StatefulWidget {
  @override
  _Change_passState createState() => _Change_passState();
}

class _Change_passState extends State<Change_pass> {

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController old_passwordController = new TextEditingController();
  TextEditingController new_passwordController = new TextEditingController();
  TextEditingController confirm_passwordController = new TextEditingController();
  String userAuthorization;
  changepassword_data(String old_password, new_password) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userAuthorization = prefs.getString(AllApi.userAuthorization);
    Map data = {
      "old_password": old_password,
      "new_password": new_password,
      'social_login': '0',
    };
    Map<String, String> requestHeaders = {
       'Authorizations':userAuthorization
     // 'Authorizations':'UzdNeXNsSXlNbFd5QmdBPQ=='
    };
    var jsondata = null;
    var response = await http.post(Uri.parse(AllApi.changepasswordApi),
        body: data,headers: requestHeaders);
    if(response.statusCode==200){
      jsondata = jsonDecode(response.body);
      int status =  jsondata['status'];
      String message =  jsondata['message'];
      if(status==0){
        showToast(message);
      }
      else {
        setState(() {
          showToast(message);
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => Home()),);
        });
      }
    }
    else {
      throw Exception('Failed to load post');
    }
  }
  @override
  void initState() {
    setState(() {
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("change Password",
          style: TextStyle(color: Color(0xFF8E2E7B),fontSize: 16,),),
      ),
     body: Form(
       key: _formKey,
       child: Container(
         width: MediaQuery.of(context).size.width,
         height: MediaQuery.of(context).size.height,
         child: Padding(
           padding: const EdgeInsets.only(top: 20,left: 20,right: 20),
           child: Column(
             crossAxisAlignment: CrossAxisAlignment.start,
             children: [
               Text("Old Password",
                 style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
               SizedBox(height: 10,),
               TextFormField(
                 controller: old_passwordController,
                 textInputAction: TextInputAction.next,
                 style: TextStyle(color: Color(0xFF8E2E7B)),
                 cursorColor: Color(0xFF8E2E7B),
                 decoration: InputDecoration(
                   fillColor: Colors.black12,
                   border: InputBorder.none,
                   filled: true,
                   hintText: "Old Password",
                   hintStyle: TextStyle( color: Colors.black38,fontSize: 12),
                 ),
                 validator: (val){
                   if(val.isEmpty){
                     return 'enter your Old Password';
                   }
                 } ,
               ),
               SizedBox(height: 10,),
               Text("New Password",
                 style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
               SizedBox(height: 10,),
               TextFormField(
                 controller: new_passwordController,
                 textInputAction: TextInputAction.next,
                 style: TextStyle(color: Color(0xFF8E2E7B)),
                 cursorColor: Color(0xFF8E2E7B),
                 decoration: InputDecoration(
                   fillColor: Colors.black12,
                   border: InputBorder.none,
                   filled: true,
                   hintText: "New Password",
                   hintStyle: TextStyle( color: Colors.black38,fontSize: 12),
                 ),
                 validator: (val){
                   if(val.isEmpty){
                     return 'enter your New Password';
                   }
                 } ,
               ),
               SizedBox(height: 10,),
               Text("Confirm Password",
                 style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
               SizedBox(height: 10,),
               TextFormField(
                 controller: confirm_passwordController,
                 textInputAction: TextInputAction.next,
                 style: TextStyle(color: Color(0xFF8E2E7B)),
                 cursorColor: Color(0xFF8E2E7B),
                 decoration: InputDecoration(
                   fillColor: Colors.black12,
                   border: InputBorder.none,
                   filled: true,
                   hintText: "Confirm Password",
                   hintStyle: TextStyle( color: Colors.black38,fontSize: 12),
                 ),
                 validator: (val){
                   if(val.isEmpty){
                     return 'enter your name';
                   }
                 } ,
               ),
               SizedBox(height: 20,),
               Row(
                 crossAxisAlignment: CrossAxisAlignment.start,
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 children: [
                   RaisedButton(
                       shape: RoundedRectangleBorder(
                         borderRadius: BorderRadius.circular(5.0),
                       ),
                       child: Padding(
                         padding: const EdgeInsets.only(left: 30,right: 30,top: 10,bottom: 10),
                         child: Text("Cancel",style: TextStyle(color: Colors.white ),),
                       ),
                       color:  Color(0xFF8E2E7B),
                       onPressed: ()
                       {
                         Navigator.of(context).pop();
                       }
                   ),
                   RaisedButton(
                     shape: RoundedRectangleBorder(
                       borderRadius: BorderRadius.circular(5.0),
                     ),
                     child: Padding(
                       padding: const EdgeInsets.only(left: 30,right: 30,top: 10,bottom: 10),
                       child: Text("Save",style: TextStyle(color: Colors.white ),),
                     ),
                     color:  Color(0xFF8E2E7B),
                     onPressed: ()
                     // {
                     //         Navigator.of(context).push(
                     //           MaterialPageRoute(builder: (BuildContext context) => Home()),);
                     //       }
                     async {
                       if (_formKey.currentState.validate()){
                       if(new_passwordController.text==confirm_passwordController.text) {
                         changepassword_data(old_passwordController.text,
                           new_passwordController.text,);
                       }
                       else{
                       }}
                       else{
                       }
                     },
                   ),
                 ],),
             ],
           ),
         ),
       ),
     ),
    );
  }
}
