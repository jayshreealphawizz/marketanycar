import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'log/allapi.dart';
import 'log/login.dart';
class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {


  String userAuthorization;
  List notification_list;

  Seveid(String pageNumber,pageSize) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userAuthorization = prefs.getString(AllApi.userAuthorization);

    if(userAuthorization==null){
      Navigator.of(context).pop();
      Navigator.push(context,
          MaterialPageRoute(builder:
              (context) => Loginpage()
          ));
    }else {
      Map data = {
        'pageNumber': pageNumber,
        'pageSize': pageSize,
      };
      Map<String, String> requestHeaders = {
        'Authorizations': userAuthorization
      };
      var jsondata = null;
      var response = await http.post(
          Uri.parse(AllApi.get_notification_listApi), body: data,
          headers: requestHeaders);
      if (response.statusCode == 200) {
        jsondata = jsonDecode(response.body);
        int status = jsondata['status'];
        String message = jsondata['message'];
        var data_list = jsondata['data'];
        if (status == 0) {
          showToast(message);
        }
        else {
          setState(() {
            notification_list = data_list;
          });
        }
      }
      else {
        throw Exception('Failed to load post');
      }
    }
  }

  @override
  void initState() {
    setState(() {
      Seveid("1",'10');
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Notifications",
            style: TextStyle(fontSize: 16,
              color: Color(0xFF8E2E7B))),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Padding(
          padding: const EdgeInsets.only(left: 15,right: 15,top: 30),
          child: ListView.builder(
            itemCount: notification_list.length,
            itemBuilder: (BuildContext context,int index){
              return _Row(notification_list[index]);
            },
          ),
        ),
      ),
    );
  }
  Widget _Row(notification_data){
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Row(children: [
        CircleAvatar(
          backgroundColor: Color(0xFF8E2E7B),
          radius: 30,
          child: CircleAvatar(
            radius: 29,
            backgroundColor: Colors.white,
            child: IconButton(
              padding: EdgeInsets.zero,
              icon: Icon(Icons.notifications_none,
                size: 30,
                color:  Color(0xFF8E2E7B),),
              onPressed: () {},
            ),
          ),
        ),
        SizedBox(width: 10,),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(notification_data['username']+" "+notification_data['last_name'],style: TextStyle(
                  fontSize: 15
              ),),
              SizedBox(height: 5,),
              Text(notification_data['message']
                ,style: TextStyle(fontSize: 12,
              ),),
            ],),
        )
      ],),
    );
  }
}
