import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'log/allapi.dart';
class LikesList extends StatefulWidget {
  final String text_PostId;

  const LikesList({Key key, this.text_PostId}) : super(key: key);

  @override
  _LikesListState createState() => _LikesListState();
}

class _LikesListState extends State<LikesList> {

  String userAuthorization;
  List get_like_list;


  get_like_list_data(String post_id,) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userAuthorization = prefs.getString(AllApi.userAuthorization);

    Map data = {
      'post_id':post_id,
    };
    Map<String, String> requestHeaders = {
      'Authorizations':userAuthorization
    };
    var jsondata = null;
    var response = await http.post(Uri.parse(AllApi.get_like_listApi),body: data,
        headers:requestHeaders);
    if(response.statusCode==200){
      jsondata = jsonDecode(response.body);
      int status =  jsondata['status'];
      String message =  jsondata['message'];
     var likedata =  jsondata['data'];
      if(status==0){

      }
      else {
        setState(() {
          get_like_list = likedata;
        });
      }
    }
    else {
      throw Exception('Failed to load post');
    }
  }

  @override
  void initState() {
    setState(() {
      get_like_list_data(widget.text_PostId);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Likes",style: TextStyle(fontSize: 16
        ,  color: Color(0xFF8E2E7B),),),
      ),
      body: get_like_list==null?Container(child: Center(child: Text('No Records found')),):Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.only(top: 10,left: 10,right: 10),
          child: ListView.builder(
            itemCount: get_like_list.length,
            itemBuilder: (BuildContext context,int index){
              return _Row(index);
            }
          ),
        ),
      ),
    );
  }
  Widget _Row(int i){
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CircleAvatar(
                backgroundImage:NetworkImage(AllApi.user_image_url+get_like_list[i]['userimage']),
                //AssetImage("assets/download.jpg",),
                radius: 30,
              ),
              SizedBox(width: 15,),
              Text(get_like_list[i]['username'],
                style: TextStyle(fontWeight: FontWeight.bold,
                    fontSize: 14),),
              Text(" Liked This Post",
                style: TextStyle(fontSize: 13),),
            ],
          ),
          Icon(Icons.favorite,color: Colors.red,size: 20,)
        ],),
    );
  }
}
