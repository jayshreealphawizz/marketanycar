import 'package:flutter/material.dart';
import 'package:Marketanycar/searchresult.dart';
import 'comman/_bottomAppBar.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body:Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: ListView(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 100,left: 20,right: 20),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white,
                    ),
                    height: 300,
                    width: MediaQuery.of(context).size.width,
                    child: Expanded(
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 25,bottom: 25),
                            child: Text("Lets Find your next car",style: TextStyle(
                              fontSize: 16,fontWeight: FontWeight.bold
                            ),),
                          ),
                          Text("Lorem Ipsum is simply dummy text "
                              ,style: TextStyle(
                              fontSize: 14,
                          ),),
                          Text("of the printing and typesetting industry.",style: TextStyle(
                              fontSize: 14,
                          ),),
                          Text("Lorem ipsum has been",style: TextStyle(
                              fontSize: 14,
                          ),),
                          Padding(
                            padding: const EdgeInsets.only(left: 20,right: 20,
                            top:20 ),
                            child: TextField(
                              cursorColor: Color(0xFF8E2E7B),
                                style: TextStyle(color:  Color(0xFF8E2E7B),),
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                suffixIcon: Icon(Icons.search,),
                                    hintText: "Search your car",
                                    hintStyle: TextStyle( color:  Color(0xFF8E2E7B)),

                                )
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 35,right: 35,top: 12,bottom: 12),
                                  child: Text("Search",style: TextStyle(color: Colors.white ),),
                                ),
                                color:  Color(0xFF8E2E7B),
                                onPressed: () =>Navigator.push(context,
                                    MaterialPageRoute(builder:
                                        (context) => Searchresult()
                                    )
                                ),),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 30),
                  child: Image.asset("assets/Group.png",
                    height: 200,width: 200,),
                ),
                Text("Already registered with us?",
                  style: TextStyle(color: Colors.black38 ),),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: GestureDetector(
                      child: Text("Sign In",style: TextStyle(color:  Color(0xFF8E2E7B) ),),
                  onTap: (){},
                  ),
                )
              ],
            ),
          ],
        ),
      ) ,
      bottomNavigationBar: Bbar(),
    );
  }
}
