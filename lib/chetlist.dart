
import 'dart:convert';

import 'package:flutter/material.dart';
import 'app_code/chatpage.dart';
import 'comman/_bottomAppBar.dart';
import 'comman/_drawer.dart';
import 'log/login.dart';
import 'notifications.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'log/allapi.dart';
import 'package:intl/intl.dart';


class Chatlist extends StatefulWidget {
  @override
  _ChatlistState createState() => _ChatlistState();
}

class _ChatlistState extends State<Chatlist> {
  SharedPreferences prefs;
  List userchat_list;
  String userAuthorization,userID;

  getchatData(String pageNumber,pageSize) async {
    prefs = await SharedPreferences.getInstance();
    userAuthorization = prefs.getString(AllApi.userAuthorization);
    userID = prefs.getString(AllApi.userId);

    if(userAuthorization==null){
      Navigator.of(context).pop();
      Navigator.push(context,
          MaterialPageRoute(builder:
              (context) => Loginpage()
          ));
    }
    else {
      Map data = {
        'pageNumber': pageNumber,
        'pageSize': pageSize,
        'user_id': userID,
      };

      Map<String, String> requestHeaders = {'Authorizations': userAuthorization};
      var jsondata = null;

      var response = await http.post(Uri.parse(AllApi.get_chat_list), body: data, headers: requestHeaders);
      if (response.statusCode == 200) {
        jsondata = jsonDecode(response.body);

        int status = jsondata['status'];
        String message = jsondata['message'];
        var data_list = jsondata['data'];

        if (status == 0) {
          showToast(message);
        }
        else {
          setState(() {
            userchat_list = data_list;
          });
        }
      }
      else {
        throw Exception('Failed to load post');
      }
    }
  }

  String readTimestamp(int timestamp) {
    var now = new DateTime.now();
    var format = new DateFormat('HH:mm a');
    var date = new DateTime.fromMicrosecondsSinceEpoch(timestamp * 1000);
    var diff = date.difference(now);
    var time = '';
    if (diff.inSeconds <= 0 || diff.inSeconds > 0 && diff.inMinutes == 0 || diff.inMinutes > 0 && diff.inHours == 0 || diff.inHours > 0 && diff.inDays == 0) {
      time = format.format(date);
    } else {
      if (diff.inDays == 1) {
        time = diff.inDays.toString() + 'DAY AGO';
      } else {
        time = diff.inDays.toString() + 'DAYS AGO';
      }
    }
    return time;
  }

  @override
  void initState() {
    setState(() {
      getchatData('1','20');
    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return  userchat_list==null?Scaffold():Scaffold(
      drawer: MyDrawer(),
      appBar: AppBar(
        title: Text("Chat List",
            style: TextStyle(fontSize: 16,color: Color(0xFF8E2E7B)),),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: GestureDetector(
                onTap: () => Navigator.push(context,
                      MaterialPageRoute(builder:
                          (context) => Notifications()
                      )),
                child: Icon(Icons.notifications_none)),
          )
        ],
      ),
      body: ListView.builder(
          itemCount: userchat_list.length,
          itemBuilder: (BuildContext context,int index){
            return Padding(
              padding: const EdgeInsets.only(left: 10,right: 10,bottom: 5),
              child: GestureDetector(
                child: Container(
                  //color: Colors.green,
                 // height: 80,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10,left: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CircleAvatar(
                              backgroundImage: NetworkImage(AllApi.user_image_url+userchat_list[index]['image']),
                            minRadius: 30,
                            ),
                            SizedBox(width: 8,),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 8,),
                                Text(userchat_list[index]['first_name'],style: TextStyle(fontSize: 15,),),
                                SizedBox(height: 5,),
                                Column(
                                  children: [
                                    Text(userchat_list[index]['email'],style: TextStyle(fontSize: 12,),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10,bottom: 10),
                          child: Text(readTimestamp(int.parse(userchat_list[index]['update_dt'])),style: TextStyle(fontSize: 11,),),
                        ),
                      ],
                    ),
                  ),
                ),
                onTap: () =>Navigator.push(context,
                    MaterialPageRoute(builder:
                        (context) =>  ChatPage(docs: userchat_list[index]['id'], receiver_name:userchat_list[index]['first_name'])
                    )
                ),
              ),
            );
          }
      ),
      bottomNavigationBar:Bbar(),
    );
  }
}
