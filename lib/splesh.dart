import 'dart:async';

import 'package:flutter/material.dart';

import 'home.dart';
class Splesh extends StatefulWidget {
  @override
  _SpleshState createState() => _SpleshState();
}

class _SpleshState extends State<Splesh> {

  void initState() {
    super.initState();
    Timer(Duration(seconds: 3),
            ()=>Navigator.pushReplacement(context,
            MaterialPageRoute(builder:
                 (context) => Home()
            )
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Positioned(
            bottom: 2,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              // child: Image.asset("assets/splesh.png",
              //   fit: BoxFit.cover,
              //     height: MediaQuery.of(context).size.height,
              //   width: MediaQuery.of(context).size.width,),
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 100,right: 100,bottom: 50),
                  child: Image.asset("assets/applogo.png"),
                ),
                SizedBox(height: 10,),
                Text("Powerd By",
                style: TextStyle(fontSize: 18,
                color: Colors.black54,
                fontWeight: FontWeight.bold),),
                Padding(
                  padding: const EdgeInsets.only(left: 15,right: 15,top: 20),
                  child: Row(
                    children: [
                      Expanded(
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Container(
                            child: Image.asset("assets/Ferrari.png",),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Container(
                            child: Image.asset("assets/BMW.png",),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Container(
                            child: Image.asset("assets/Mars.png",),
                          ),
                        ),
                      ),

                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15,right: 15,top: 5),
                  child: Row(
                    children: [
                      Expanded(
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Container(
                            child: Image.asset("assets/Audi.png",),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Container(
                            child: Image.asset("assets/Porsche.png",),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Container(
                            child: Image.asset("assets/Landrover.png",),
                          ),
                        ),
                      ),

                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );;
  }
}
