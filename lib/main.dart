import 'package:Marketanycar/emojitest.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:Marketanycar/splesh.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SharedPreferences.setMockInitialValues({});
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Color(0xFF8E2E7B),
      ),
      child:
      // ScreenUtilInit(
      //   designSize: Size(360, 690),
      //   allowFontScaling: false,
      //   child:
        MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Flutter Demo',
          theme: ThemeData(
            fontFamily: 'Quicksand',
            appBarTheme: AppBarTheme(
              elevation: 0.0,
              color: Colors.black12,
              iconTheme: IconThemeData(
                color:Color(0xFF8E2E7B),
              ),
            textTheme: TextTheme(
              headline1: TextStyle(
                color: Color(0xFF8E2E7B),
                fontSize: 16
              ),
                bodyText1: TextStyle(color: Color(0xFF8E2E7B)),
                bodyText2: TextStyle(color: Color(0xFF8E2E7B))
            ),
            ),
            primaryColor:  Color(0xFF8E2E7B),
            accentColor: Color(0xFF8E2E7B),
            cursorColor:  Color(0xFF8E2E7B),
            backgroundColor: Colors.white,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          // home:Mypost_list(),
          // home:pageloadingnew(),
          home:Splesh(),
        ),
     // ),
    );
  }
}

