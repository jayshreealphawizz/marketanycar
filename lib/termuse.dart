import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'comman/_bottomAppBar.dart';
import 'log/allapi.dart';
import 'log/log.dart';
import 'notifications.dart';

class Termuse extends StatefulWidget {
  Termuse({Key key}) : super(key: key);
  @override
  _TermuseState createState() => _TermuseState();
}

class _TermuseState extends State<Termuse> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

String description;
  Future<Post> post;
  String Condition = "terms_condition";
  Future<Post> createAlbum(String title) async {
    Map data = {
      "page_title":Condition,
    };
    var jsondata = null;
    var response = await http.post(Uri.parse(AllApi.getcontentApi),body: data,);
    if (response.statusCode == 200) {
      jsondata = jsonDecode(response.body);
      bool status =  jsondata['status'];
      description =  jsondata['data']['description'];
      String message =  jsondata['message'];
      if(status){
        setState(() {
          description;
        });
      }
     else{}
    }
    else {}
  }
  void initState() {
    createAlbum(Condition);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Term of Use",
          style: TextStyle(fontSize: 16,color:  Color(0xFF8E2E7B)),),
        actions: [
          GestureDetector(
              child: Icon(Icons.notifications_none),
          onTap: () =>Navigator.push(context,
              MaterialPageRoute(builder:
                  (context) => Notifications()
              )
          ),
          ),
          SizedBox(width: 10,)
        ],
      ),
      body: Form(
        key: _formKey,
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: ListView(
            children: [
              Container(color: Color(0xFF8E2E7B),height: 2,),
              Stack(
                children: [
                  Container(
                    color:Colors.black12,
                    height: MediaQuery.of(context).size.height*0.25,
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 75,right: 75),
                      child: Image.asset("assets/applogo.png",

                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20,left: 20,right: 20),
                child: Column(
                  children: [
                  description==null?
                  Text('')
                      :Text(description)
                  ],
                )
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: Bbar(),
    );
  }
}
