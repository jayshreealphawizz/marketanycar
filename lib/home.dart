import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:Marketanycar/social/sign_in.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'cardetails.dart';
import 'comman/_bottomAppBar.dart';
import 'comman/_drawer.dart';
import 'comments.dart';
import 'filter.dart';
import 'like_list.dart';
import 'log/allapi.dart';
import 'notifications.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:loadmore/loadmore.dart';
import 'package:share/share.dart';
import 'package:path_provider/path_provider.dart';

class Home extends StatefulWidget {

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController commentController = new TextEditingController();
  String userAuthorization, userID;
  var recent_upload_whole_list ;
  List filename_whole_list ;
  List<String> postimge_strlist;
  int pageno_value =0; int isFinish = 0;
  int get count => recent_upload_whole_list.length;
  List recent_upload_whole_list_newdata ;

  getAllPost_Api(category_id,pageNumber,pageSize) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userAuthorization = prefs.getString(AllApi.userAuthorization);
    userID = prefs.getString(AllApi.userId);
    if(userID == null){
      userID = '';
    }
    Map data = {
      'pageNumber':pageNumber.toString(),
      'pageSize': pageSize,
      // 'category_id':category_id,
      'user_id':userID,
    };
    var jsondata = null;
    var response = await http.post(Uri.parse(AllApi.get_post_listApi),body: data,);
    if(response.statusCode==200){
      jsondata = jsonDecode(response.body);
      int status = jsondata['status'];
      String message = jsondata['message'];
      var data_list = jsondata['data'];
      if(status==0){
        isFinish = -1;
      }
      else {
        setState(() {
          recent_upload_whole_list_newdata = data_list;
          if(recent_upload_whole_list!=null) {
            recent_upload_whole_list = new List.from(recent_upload_whole_list_newdata)..addAll(recent_upload_whole_list);
          }
          else{
            recent_upload_whole_list = recent_upload_whole_list_newdata;
          }
        });
      }
    }
    else {
      throw Exception('Failed to load post');
    }
  }
  void postlike(String postId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userAuthorization = prefs.getString(AllApi.userAuthorization);

    if(userAuthorization==null){
      showToast('You can not like post without login');
    }
    else {
      Map data = {
        "post_id": postId,
      };
      Map<String, String> requestHeaders = {
        'Authorizations': userAuthorization
      };
      var jsondata = null;
      var response = await http.post(Uri.parse(AllApi.get_post_likeApi), body: data, headers: requestHeaders);

      if (response.statusCode == 200) {
        jsondata = jsonDecode(response.body);
        int status = jsondata['status'];
        String message = jsondata['message'];
        if (status == 0) {
          showToast(message);
        }
        else {
          setState(() {
            showToast(message);
            getAllPost_Api("1", '1', '10');
          });
        }
      }
      else {
        throw Exception('Failed to load post');
      }
    }
  }
  void postcomment(String comment,String postId) async {

    if(userAuthorization==null){
      showToast('You can not comment post without login');
    }
    else {
      Map data = {
        "post_id": postId,
        'comment': comment,
      };
      Map<String, String> requestHeaders = {
        'Authorizations': userAuthorization
      };
      var jsondata = null;
      var response = await http.post(
         Uri.parse (AllApi.post_commentApi), body: data, headers: requestHeaders);
      if (response.statusCode == 200) {
        jsondata = jsonDecode(response.body);
        int status = jsondata['status'];
        String message = jsondata['message'];
        if (status == 0) {
          showToast(message);
        }
        else {
          setState(() {
            showToast(message);
            getAllPost_Api("1", '1', '10');
          });
        }
      }
      else {
        throw Exception('Failed to load post');
      }
    }
  }


  @override
  void initState() {
    setState(()  {
      loadHomeScreenApi();
    });
    super.initState();
  }


// ==============================
  void loadHomeScreenApi() {
    print("load");
    setState(() {
      if(isFinish != -1) {
        getAllPost_Api('1', pageno_value, '5');
        pageno_value = pageno_value + 1;
      }
    });
  }

  Future<bool> _loadMore() async {
    print("onLoadMore");
    await Future.delayed(Duration(seconds: 0, milliseconds: 2000));
    loadHomeScreenApi();
    return true;
  }

  Future<void> _refresh() async {
    print("_refresh");
    // await Future.delayed(Duration(seconds: 0, milliseconds: 2000));
    // recent_upload_whole_list.clear();
    // load();
  }
//   ============================

  @override
  Widget build(BuildContext context) {
    setErrorLoadingText();
    return recent_upload_whole_list.length==null?Scaffold(backgroundColor: Colors.grey,):
    Scaffold(
      appBar: _AppBar(),
      body: Container(
        child: RefreshIndicator(
          child: LoadMore(
            isFinish: isFinish == -1,
            onLoadMore: _loadMore,
            child: ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                return _Contaner_list(index);
              },
              itemCount: count,
            ),
            whenEmptyLoad: false,
            delegate: DefaultLoadMoreDelegate(),
            textBuilder: DefaultLoadMoreTextBuilder.english,
          ),
          onRefresh: _refresh,
        ),
      ),
      bottomNavigationBar: Bbar(),
      drawer: MyDrawer(),
    );
  }

  Widget _Contaner_list(int index){
      return recent_upload_whole_list.length==null?Container():
     Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    CircleAvatar(
                      backgroundImage:recent_upload_whole_list[index]['userimage']==null?NetworkImage(''):
                      NetworkImage(AllApi.user_image_url+recent_upload_whole_list[index]['userimage']),
                      minRadius: 25,
                    ),
                    SizedBox(width: 8,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(recent_upload_whole_list[index]['name'],style: TextStyle(fontSize: 14,),),
                        Text(recent_upload_whole_list[index]['useraddress'],style: TextStyle(fontSize: 12,)),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),

          recent_upload_whole_list[index]['filename'].length == 0?Container():
          GestureDetector(
            onTap: () =>Navigator.push(context,
                MaterialPageRoute(builder:
                    (context) => Cardetails(text_PostId: recent_upload_whole_list[index]['id'],)
                )
            ),
            child: Container(
              height: 230,
              width: MediaQuery.of(context).size.width,
              child:recent_upload_whole_list[index]['filename'][0]['image']==null?Image.network(''):
              Image.network(AllApi.user_image_url+recent_upload_whole_list[index]['filename'][0]['image'],
                fit: BoxFit.cover,
                height: 250, width: MediaQuery.of(context).size.width,
              ) ,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 5,right: 10,top: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    IconButton(icon: Icon(Icons.favorite,
                        color: recent_upload_whole_list[index]['is_like']=='0'?Colors.blueGrey:Colors.red),
                        onPressed: (){
                      setState(() {
                        postlike(recent_upload_whole_list[index]['id']);
                      });
                    }),

                    IconButton(icon: ImageIcon(
                      AssetImage("assets/icons/comment.png")),
                        onPressed:(){
                          if (userAuthorization == null) {
                            showToast('You can not see comment on post without login');
                          } else {
                            Navigator.of(context).push(
                              MaterialPageRoute(builder: (BuildContext context) => Comments(text_PostId: recent_upload_whole_list[index]['id'])),);
                          }
                        }
                     )
                  ],
                ),
                GestureDetector(
                  onTap: () => ShareIntent(context, index),
                  child: CircleAvatar(
                    child:  ImageIcon(
                      AssetImage("assets/icons/share.png"),
                      color: Colors.white,size: 18,
                    ),
                    minRadius: 13,
                    backgroundColor: Color(0xFF8E2E7B),
                  ),
                ),
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector( onTap: () =>Navigator.push(context,
                    MaterialPageRoute(builder:
                        (context) => LikesList(text_PostId: recent_upload_whole_list[index]['id'],)
                    )
                ),
                child: Text(recent_upload_whole_list[index]['like_count']+"\t like", style: TextStyle(fontSize: 11,fontWeight: FontWeight.bold),)),
                SizedBox(height: 5,),
                GestureDetector(
                      onTap:(){
                          if (userAuthorization == null) {
                            showToast('You can not see comment on post without login');
                          } else {
                            Navigator.of(context).push(
                              MaterialPageRoute(builder: (BuildContext context) => Comments(text_PostId: recent_upload_whole_list[index]['id'])),);
                          }
                        },
                                  child: Text(recent_upload_whole_list[index]['comment_count']+"\t comment",
                    style: TextStyle(fontSize: 11),),
                ),
              ],
            ),
          ),
          // Padding(
          //   padding: const EdgeInsets.only(left: 10,right: 10,top: 5,bottom: 3),
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //     children: [
          //       Row(
          //         children: [
          //           CircleAvatar(
          //             backgroundImage: NetworkImage(AllApi.user_image_url+recent_upload_whole_list[index]['login_user_image']),
          //             minRadius: 15,
          //           ),
          //           SizedBox(width: 8,),
          //           Container(
          //               width: 200,
          //               height: 30,
          //               child: TextFormField(
          //                 key: Key("comment"),
          //                 controller: commentController,
          //                 decoration: InputDecoration(
          //                   border: InputBorder.none,
          //                   hintStyle: TextStyle(fontSize: 12),
          //                   hintText: 'Add Comment...',),
          //               )),
          //         ],
          //       ),
          //       IconButton(icon: Icon(Icons.add_circle_outline,size: 18,),
          //           onPressed:()async {
          //             postcomment(commentController.text,recent_upload_whole_list[index]['id']);
          //           },)
          //      // Icon(Icons.add_circle_outline,size: 18,)
          //     ],
          //   ),
          // ),
          Divider()
        ],
      ),
    );

  }

  Widget _AppBar(){
    return AppBar(
      title: Text("Home",style: TextStyle(fontSize: 16,
        color:  Color(0xFF8E2E7B) ),),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 10),
          child: Row(
            children: [
              GestureDetector(
                child: Icon(Icons.notifications_none,),
                onTap: () =>Navigator.push(context,
                    MaterialPageRoute(builder:
                        (context) => Notifications()
                    )
                ),
              ),
              SizedBox(width: 10,),
              GestureDetector(
                child: Icon(Icons.filter_alt_outlined,),
                onTap: () =>Navigator.push(context,
                    MaterialPageRoute(builder:
                        (context) => Filter()
                    )
                ).then((value) => setState(() {})),
              )
            ],
          ),
        )
      ],
    );
  }

  ShareIntent(BuildContext context,int index) async {

    final RenderBox box = context.findRenderObject();

    postimge_strlist = [for(int i = 0; i < recent_upload_whole_list[index]['filename'].length; i++)
      AllApi.user_image_url+recent_upload_whole_list[index]['filename'][i]['image'].toString()];

    await Share.share(recent_upload_whole_list[index]['name'],
        subject: 'SellAnyCar',
        sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);


    // if (postimge_strlist.length != 0) {
    //   print('share intent');
    //   print('imagePaths'+postimge_strlist.toString());
    //
    //   // getFilePath(postimge_strlist,box);
    //
    // } else {
    //   await Share.share('hiii',
    //       subject: 'subject',
    //       sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
    // }
  }


  // getFilePath(List<String> postimge_strlist, RenderBox box) async {
    // Directory appDocumentsDirectory = await getApplicationDocumentsDirectory(); // 1
    // String appDocumentsPath = appDocumentsDirectory.path; // 2
    //
    // List<String> localFile;
    // localFile = [for(int i = 0; i <postimge_strlist.length; i++) '$appDocumentsPath/amanImage/'+postimge_strlist[0]];
    // await Share.shareFiles(localFile,
    //     text: 'text',
    //     subject: 'subject',
    //     sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  // }

}


