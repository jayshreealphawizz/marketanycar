import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'log/allapi.dart';
import 'log/log.dart';
import 'log/login.dart';
class Forgotp extends StatefulWidget {
  Forgotp({Key key}) : super(key: key);

  @override
  _ForgotpState createState() => _ForgotpState();
}

class _ForgotpState extends State<Forgotp> {
  final _formKey = GlobalKey<FormState>();
  Future<Post> post;
  TextEditingController emailController = new TextEditingController();

  Forgotpass(String email ) async {
    Map data = {
      "email": email,
    };
    var jsondata = null;
    var response = await http.post(Uri.parse(AllApi.forgot_passwordApi),body: data);

    if(response.statusCode==200){
      jsondata =jsonDecode(response.body);
      int status =  jsondata['status'];
      String message =  jsondata['message'];
      if(status==0){
        showToast(message);
      }
      else {
        showToast(message);
        setState(() {
          // showToast();
          Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context)=> Loginpage()),);
        });
      }
    }
    else {
      throw Exception('Failed to load post');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
          key: _formKey,
          child: ListView(
            children: [
              Stack(
                children: [
                  Image.asset("assets/Mask 180.png",
                    fit: BoxFit.fill,
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 25,right: 25),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(height:MediaQuery.of(context).size.height*0.15,),
                          Image.asset("assets/applogo.png",
                            height: 100,width: 200,),
                          Text("Forgot Password",style: TextStyle(
                            fontSize: 16,fontWeight: FontWeight.bold,
                          ),),

                          TextFormField(
                            controller: emailController,
                              key: Key("email"),
                            keyboardType: TextInputType.emailAddress,
                              style: TextStyle(color: Color(0xFF8E2E7B)),
                              cursorColor: Color(0xFF8E2E7B),
                              decoration: InputDecoration(
                                fillColor: Colors.black12,
                                border: InputBorder.none,
                                filled: true,
                                hintText: "Email",
                                hintStyle: TextStyle( color: Colors.black38,fontSize: 12),
                              ),
                            validator: (val){
                              final RegExp nameExp =
                              new RegExp(r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$');
                              if(val.isEmpty){
                                return'Please enter email address.';
                              }
                              else if(!nameExp.hasMatch(val)){
                                return 'Please enter valid email address.';
                              }
                            },
                          ),

                          Column(
                            children: [
                              Text("New Password will share to your mail. kindly check Your Mail",style: TextStyle(
                                  fontSize: 14,color: Colors.black
                              ),)
                            ],
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              RaisedButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 40,right: 40,top: 15,bottom: 15),
                                  child: Text("Send",style: TextStyle(color: Colors.white ),),
                                ),
                                color:  Color(0xFF8E2E7B),
                                  // onPressed: () {
                                  //   Navigator.of(context).push(
                                  //     MaterialPageRoute(builder: (BuildContext context) => Home()),);
                                  // }
                                onPressed: () async {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return Center(child: CircularProgressIndicator(),);
                                      });
                                  await loginAction();
                                  Navigator.pop(context);
                                  if (_formKey.currentState.validate()) {
                                    Forgotpass(emailController.text,);
                                  }
                                  else{
                                  }
                                },
                              ),
                            ],),
                          SizedBox(height:MediaQuery.of(context).size.height*0.3,),
                        ],
                      ),
                    ),

                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: IconButton(icon: Icon(Icons.arrow_back,color: Colors.white70,),
                         onPressed: (){
                           Navigator.of(context).pop();
                        }),
                  ),
                ],
              ),
            ],
          ),
        ),
    );
  }
}
