import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'comman/_bottomAppBar.dart';
import 'log/allapi.dart';
import 'log/log.dart';
import 'notifications.dart';
class Privacy extends StatefulWidget {
  Privacy({Key key}) : super(key: key);
  @override
  _PrivacyState createState() => _PrivacyState();
}

class _PrivacyState extends State<Privacy> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  String description;
  Future<Post> post;
  String Condition = "privacy_policy";
  Future<Post> createAlbum(String title) async {
    Map data = {
      "page_title":Condition,
    };
    var jsondata = null;
    var response = await http.post(Uri.parse(AllApi.getcontentApi),body: data,);
    if (response.statusCode == 200) {

      jsondata = jsonDecode(response.body);
      bool status =  jsondata['status'];
      description =  jsondata['data']['description'];
      String message =  jsondata['message'];

      if(status){
        setState(() {
          description;
        });
      }
      else{}
    }
    else {}
  }
  void initState() {
    createAlbum(Condition);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Privacy Policy",
          style: TextStyle(fontSize: 16,color:  Color(0xFF8E2E7B)),),
        actions: [
          GestureDetector(
              child: Icon(Icons.notifications_none),
          onTap: ()=>Navigator.push(context,
              MaterialPageRoute(builder:
                  (context) => Notifications(),
              ),
          ),
          ),
          SizedBox(width: 10,)
        ],
      ),
      body: Form(
        key: _formKey,
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: ListView(
            children: [
             Container(color: Color(0xFF8E2E7B),height: 2,),
              Stack(
                children: [
                  Container(
                    color:Colors.black12,
                    height: MediaQuery.of(context).size.height*0.25,
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 75,right: 75),
                      child: Image.asset("assets/applogo.png",

                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                  padding: const EdgeInsets.only(top: 20,left: 20,right: 20),
                  child: Column(
                    children: [
                      description==null?
                      Text('')
                          :Text(description)

                      // Text("Creating a Terms of Use for your"
                      //     " application or website can take a "
                      //     "lot of time. You could either spend"
                      //     " tons of money on hiring a lawyer, or"
                      //     " you could simply use our service "
                      //     "and get a unique Terms of "
                      //     "Use fully custumized to your website."
                      //     " If your website or app has the option"
                      //     " to recieve payments then including a"
                      //     " Terms of Use is required by law. We "
                      //     "will make sure that your Terms of Use "
                      //     "ensures that you stay in"
                      //     " line with your legal obligations."
                      //     "Creating a Terms of Use for your"
                      //     " application or website can take a "
                      //     "lot of time. You could either spend"
                      //     " tons of money on hiring a lawyer, or"
                      //     " you could simply use our service "
                      //     "and get a unique Terms of "
                      //     "Use fully custumized to your website."
                      //     "If your website or app has the option"
                      //     " to recieve payments then including a"
                      //     " Terms of Use is required by law. We "
                      //     "will make sure that your Terms of Use "
                      //     "ensures that you stay in"
                      //     " line with your legal obligations."
                      //   ,style: TextStyle(fontSize: 14,),
                      // )
                    ],
                  )
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: Bbar(),
    );
  }
}
